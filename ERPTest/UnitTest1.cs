using Entities.ERP;
using ERPDataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ERPTest
{
    [TestClass]
    public class UnitTest1
    {
            
        [TestMethod]
        public void TestMethod1()
        {
            //Prueba conexi�n database
            var options = new DbContextOptionsBuilder<ToolsERP_DESARROLLOContext>().UseSqlServer("ERPDatabase").Options;

            var context = new ToolsERP_DESARROLLOContext(options);

            var concepto = context.AfipConceptos.Select(c => c).ToList()[0];

            concepto.Descripcion = "Cambiada";

            context.SaveChanges();
        }
    }
}
