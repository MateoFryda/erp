﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Temp1
    {
        public string SaArt { get; set; }
        public string PrecioOrigen { get; set; }
        public string CoefDescuento1 { get; set; }
        public decimal? Valor1 { get; set; }
        public decimal? PrecioConDto1 { get; set; }
        public string CoefDescuento2 { get; set; }
        public decimal? Valor2 { get; set; }
        public decimal? PrecioConDto2 { get; set; }
        public string CoefDescuento3 { get; set; }
        public decimal? Valor3 { get; set; }
        public decimal? PrecioConDto3 { get; set; }
        public string CoefDescuento4 { get; set; }
        public decimal? Valor4 { get; set; }
        public decimal? CostoNeto { get; set; }
        public string CodigoCoefLanding { get; set; }
        public decimal? Valor5 { get; set; }
        public decimal? CostoLanded { get; set; }
        public string CodigoCoefADolar { get; set; }
        public decimal? Valor6 { get; set; }
        public decimal? CostoEnDolares { get; set; }
        public string CoefFinal { get; set; }
        public decimal? Valor7 { get; set; }
        public decimal? CostoStd { get; set; }
        public string Utilidad { get; set; }
        public decimal? Valor8 { get; set; }
        public decimal? PrecioDeLista { get; set; }
    }
}
