﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AnticiposPreciosDet
    {
        public int Id { get; set; }
        public short IdLista { get; set; }
        public string SaArt { get; set; }
        public decimal Precio { get; set; }
        public decimal? Moneda { get; set; }

        public virtual AnticiposPreciosCab IdNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
