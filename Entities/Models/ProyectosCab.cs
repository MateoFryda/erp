﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProyectosCab
    {
        public ProyectosCab()
        {
            ProyectosDets = new HashSet<ProyectosDet>();
        }

        public int PrNro { get; set; }
        public string PrDesc { get; set; }
        public string PrClnro { get; set; }
        public DateTime PrFfech { get; set; }
        public string PrVen { get; set; }
        public string PrRespSeg { get; set; }
        public byte? PrCpag { get; set; }
        public int PrLista { get; set; }
        public string PrObs { get; set; }
        public string PrDeposito { get; set; }
        public DateTime PrFecIniEst { get; set; }
        public DateTime? PrFecFinEst { get; set; }
        public DateTime? PrFecCierre { get; set; }
        public bool PrFacturable { get; set; }
        public byte PrMon { get; set; }
        public decimal PrCotizmon { get; set; }
        public decimal PrCotizdolar { get; set; }
        public string PrCc { get; set; }
        public string PrIc { get; set; }
        public bool PrAnul { get; set; }
        public string PrUsuario { get; set; }
        public string PrHcarga { get; set; }
        public DateTime? PrFultModif { get; set; }
        public string PrHultModif { get; set; }
        public DateTime? Fregistro { get; set; }

        public virtual ICollection<ProyectosDet> ProyectosDets { get; set; }
    }
}
