﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Facturacionjob
    {
        public string VaTipo { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public string FIdjob { get; set; }
        public string FMesimputado { get; set; }
        public decimal FImporte { get; set; }
        public int Id { get; set; }
    }
}
