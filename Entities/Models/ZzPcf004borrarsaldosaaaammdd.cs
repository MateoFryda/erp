﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzPcf004borrarsaldosaaaammdd
    {
        public string CdFcomp { get; set; }
        public short CdSuc { get; set; }
        public int? CdFnro { get; set; }
        public string CdCnro { get; set; }
        public DateTime? CdFfech { get; set; }
        public decimal? CdTotal { get; set; }
        public DateTime? CdFvto { get; set; }
        public DateTime? CdFcanc { get; set; }
        public decimal? CdSaldo { get; set; }
        public string CdCcanc { get; set; }
        public short? CdCsuc { get; set; }
        public int? CdNcanc { get; set; }
        public decimal? CdPordesc { get; set; }
        public decimal? CdIva { get; set; }
        public decimal? CdComi { get; set; }
        public string CdComp { get; set; }
        public string CdFcprov { get; set; }
        public int? CdClasificacion { get; set; }
        public bool CdSeleccion { get; set; }
        public byte CdMon { get; set; }
        public decimal CdCotizmon { get; set; }
        public decimal CdCotiz { get; set; }
        public DateTime? CdFpago { get; set; }
        public string CdCta { get; set; }
    }
}
