﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProformasAfacturar2
    {
        public string IdEmpresaStock { get; set; }
        public string EmpresaStock { get; set; }
        public string IdOrganizacion { get; set; }
        public string EmpresaTr { get; set; }
        public int? NroProf { get; set; }
        public decimal? Total { get; set; }
        public string Responsable { get; set; }
        public string Departamento { get; set; }
        public string Periodo { get; set; }
        public byte Moneda { get; set; }
    }
}
