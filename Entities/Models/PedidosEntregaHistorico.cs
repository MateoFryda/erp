﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosEntregaHistorico
    {
        public string VcTipo { get; set; }
        public byte VcSuc { get; set; }
        public int VcNro { get; set; }
        public string VcClnro { get; set; }
        public string DepCodigo { get; set; }
        public string DepDescripcion { get; set; }
        public short? TaCodigo { get; set; }
        public string TaDescripcion { get; set; }
        public string Observacion { get; set; }
        public string Plazo { get; set; }
        public string Direccion { get; set; }
        public short? PaisCodigo { get; set; }
        public string PaisDescripcion { get; set; }
        public string Codpostal { get; set; }
        public string ProvCodigo { get; set; }
        public string ProvDescripcion { get; set; }
        public string Localidad { get; set; }
        public string Tel { get; set; }
        public string TipoEntrega { get; set; }
        public short CaHorario { get; set; }
    }
}
