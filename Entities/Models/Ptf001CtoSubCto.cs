﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001CtoSubCto
    {
        public string PaCnro { get; set; }
        public string PaCcosto { get; set; }
        public string PaSubcosto { get; set; }
        public decimal Porcentaje { get; set; }

        public virtual Cgf010 PaCcostoNavigation { get; set; }
        public virtual Cgf011 PaSubcostoNavigation { get; set; }
    }
}
