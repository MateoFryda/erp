﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CodigosDeFacturacion
    {
        public string CfCodigo { get; set; }
        public string CfDescripcion { get; set; }
    }
}
