﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001C
    {
        public string SaArt { get; set; }
        public string PaCnro { get; set; }
        public string ScCodProveedor { get; set; }
        public string ScCodBarra { get; set; }
        public decimal? ScCompraMin { get; set; }
        public decimal? ScDemora { get; set; }
        public string ScUnidadCom { get; set; }
        public int? ScUniVtaXunidad { get; set; }
        public string ScDescArtProv { get; set; }
        public decimal? ScCantxenv { get; set; }
        public bool? ScActualizaPrecio { get; set; }

        public virtual Ptf001 PaCnroNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
