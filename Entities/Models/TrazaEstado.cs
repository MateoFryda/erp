﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaEstado
    {
        public string STabla { get; set; }
        public int NCodEstado { get; set; }
        public string SDescEstado { get; set; }
        public int? NHab { get; set; }
    }
}
