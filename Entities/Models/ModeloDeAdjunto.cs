﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ModeloDeAdjunto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string NombreReporte { get; set; }
        public string NombreAdjunto { get; set; }
        public string TipoxEmailsConfiguracion { get; set; }
    }
}
