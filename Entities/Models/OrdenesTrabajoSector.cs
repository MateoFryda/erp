﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class OrdenesTrabajoSector
    {
        public int OtNroot { get; set; }
        public short? OtSector { get; set; }
        public string OtPnro { get; set; }
        public DateTime OtFechaEnvio { get; set; }
        public short? OtPasoPlanProduccion { get; set; }
        public string OtTarea { get; set; }
        public int OtId { get; set; }

        public virtual OrdenesTrabajoCab OtNrootNavigation { get; set; }
    }
}
