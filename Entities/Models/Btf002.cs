﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Btf002
    {
        public DateTime? BbFec { get; set; }
        public DateTime? BbFvto { get; set; }
        public short BbBanco { get; set; }
        public int BbNro { get; set; }
        public decimal BbImporte { get; set; }
        public string BbEntrega { get; set; }
        public string BbTipo { get; set; }
        public int? BbRecibo { get; set; }
        public string BbCcomp { get; set; }
        public string BbFcprov { get; set; }
        public int? BbCnro { get; set; }
        public DateTime? BbFecsal { get; set; }
        public string BbDiferido { get; set; }
        public int BbIdent { get; set; }
        public bool BbCtrlimpresion { get; set; }
    }
}
