﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempretenciongananciasX
    {
        public string Cuit { get; set; }
        public string Certificado { get; set; }
        public string DiaRet { get; set; }
        public string MesRet { get; set; }
        public string AnioaRet { get; set; }
        public string Regimen { get; set; }
        public string Importe { get; set; }
    }
}
