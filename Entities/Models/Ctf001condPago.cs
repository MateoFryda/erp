﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001condPago
    {
        public Ctf001condPago()
        {
            CondPagoVencs = new HashSet<CondPagoVenc>();
            Ctf001s = new HashSet<Ctf001>();
        }

        public short Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool Escontado { get; set; }
        public int? IdDto { get; set; }
        public bool Esfinanciacion { get; set; }

        public virtual ICollection<CondPagoVenc> CondPagoVencs { get; set; }
        public virtual ICollection<Ctf001> Ctf001s { get; set; }
    }
}
