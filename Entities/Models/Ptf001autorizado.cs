﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001autorizado
    {
        public string PaCnro { get; set; }
        public string PaTipodocbnl { get; set; }
        public string PaNrodoc { get; set; }
        public string PaApel1 { get; set; }
        public string PaApel2 { get; set; }
        public string PaNombre { get; set; }
        public DateTime? PaFultmodif { get; set; }
        public DateTime? PaFulttransf { get; set; }
        public DateTime? PaFfinalizacion { get; set; }
    }
}
