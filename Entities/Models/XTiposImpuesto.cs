﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XTiposImpuesto
    {
        public XTiposImpuesto()
        {
            ImpuestosTipos = new HashSet<ImpuestosTipo>();
        }

        public int IdTipoImpuesto { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<ImpuestosTipo> ImpuestosTipos { get; set; }
    }
}
