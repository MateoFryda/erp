﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf003
    {
        public Stf003()
        {
            Stf001s = new HashSet<Stf001>();
        }

        public string ScFlia { get; set; }
        public string ScDesc { get; set; }

        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
