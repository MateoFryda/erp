﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tabla7
    {
        public string TipoComp { get; set; }
        public string Cfiscal { get; set; }
        public string FechComp { get; set; }
        public string PtoVta { get; set; }
        public string Comprob { get; set; }
        public string ComprobReg { get; set; }
        public decimal? Cantidad { get; set; }
        public string UnidMed { get; set; }
        public decimal VbPciolocal { get; set; }
        public decimal? Bonif { get; set; }
        public int ImpAjuste { get; set; }
        public decimal? Subtotal { get; set; }
        public decimal Alicuota { get; set; }
        public string Campo14 { get; set; }
        public string Anulado { get; set; }
        public string DisenoLib { get; set; }
    }
}
