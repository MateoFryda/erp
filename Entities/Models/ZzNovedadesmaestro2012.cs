﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzNovedadesmaestro2012
    {
        public int IdNovedad { get; set; }
        public string CodCliente { get; set; }
        public int IdConcepto { get; set; }
        public decimal Importe { get; set; }
        public short Moneda { get; set; }
        public int Cantidad { get; set; }
        public DateTime? Finicio { get; set; }
        public DateTime? Ffinal { get; set; }
        public string Descripcion { get; set; }
        public short CantCuotas { get; set; }
        public bool MuestraCuotas { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public short GrupoProforma { get; set; }
        public bool Anulado { get; set; }
    }
}
