﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class MovCajaTipo
    {
        public MovCajaTipo()
        {
            MovCajaCodigos = new HashSet<MovCajaCodigo>();
        }

        public byte Código { get; set; }
        public string Descripción { get; set; }
        public string SpValidacionInicial { get; set; }
        public string SpValidacionFinal { get; set; }

        public virtual ICollection<MovCajaCodigo> MovCajaCodigos { get; set; }
    }
}
