﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsImpuestosGrale
    {
        public int IdOperacion { get; set; }
        public string IdImpuesto { get; set; }
        public string Art { get; set; }
        public int CodTransac { get; set; }
        public string Descripcion { get; set; }
        public string ImputacionVentas { get; set; }
        public string ImputacionCompras { get; set; }
        public decimal? Impuesto { get; set; }
        public decimal? Alicuota { get; set; }
        public decimal? Base { get; set; }
        public decimal? Exento { get; set; }
        public decimal? ImpuestoExt { get; set; }
        public decimal? Baseext { get; set; }
        public decimal? ExentoExt { get; set; }
    }
}
