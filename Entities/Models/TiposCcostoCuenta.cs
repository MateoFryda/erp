﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposCcostoCuenta
    {
        public byte IdTipoCcosto { get; set; }
        public string Cuenta { get; set; }
    }
}
