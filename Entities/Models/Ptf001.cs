﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001
    {
        public Ptf001()
        {
            IngStkCabs = new HashSet<IngStkCab>();
            Otf001s = new HashSet<Otf001>();
            PreciosProvCabs = new HashSet<PreciosProvCab>();
            Stf001Cs = new HashSet<Stf001C>();
        }

        public string PaCnro { get; set; }
        public string PaCnom { get; set; }
        public string PaDom { get; set; }
        public int? PaCp { get; set; }
        public string PaResp { get; set; }
        public string PaTele { get; set; }
        public string PaFax { get; set; }
        public byte? PaCpag { get; set; }
        public string PaObs { get; set; }
        public byte? PaCact { get; set; }
        public byte? PaCcpto { get; set; }
        public byte? PaIva { get; set; }
        public string PaCuit { get; set; }
        public byte? PaGan { get; set; }
        public double? PaCoefadi { get; set; }
        public double? PaOrigen { get; set; }
        public string PaImpcom { get; set; }
        public byte? PaTipdoc { get; set; }
        public byte? PaRetiv { get; set; }
        public string PaImppro { get; set; }
        public string PaBenef { get; set; }
        public string PaUser { get; set; }
        public string PaEmail { get; set; }
        public string PaProvincia { get; set; }
        public string PaImpprodolar { get; set; }
        public string PaImpproom { get; set; }
        public byte PaRetingbr { get; set; }
        public string PaCbu { get; set; }
        public decimal PaPorex { get; set; }
        public string PaNroIngbr { get; set; }
        public short? PaPais { get; set; }
        public string PaIdAgrupImp { get; set; }
        public string PaTitularCuenta { get; set; }
        public string PaFormapagodef { get; set; }
        public string PaNumeroCuentaBan { get; set; }
        public string PaTipoCuentaBan { get; set; }
        public string PaCodigoBanco { get; set; }
        public byte PaRetsuss { get; set; }
        public byte? PaActec { get; set; }
        public int? PaTae { get; set; }
        public string PaIngbtos { get; set; }
        public bool? PaRetirapago { get; set; }
        public string PaEmailPagos { get; set; }
        public string PaCcosto { get; set; }
        public string PaSubcosto { get; set; }
        public string PaSucbnl { get; set; }
        public bool PaChequecruzado { get; set; }
        public DateTime? PaFulttransf { get; set; }
        public DateTime? PaFultmodif { get; set; }
        public string PaEmailOc { get; set; }
        public string PaNombreFantasia { get; set; }
        public int PaTipopago { get; set; }
        public short PaTransporte { get; set; }
        public string PaAnticipo { get; set; }
        public string PaAnticipodolar { get; set; }
        public DateTime? PaInhabilitacion { get; set; }
        public string PaCuitCbu { get; set; }
        public bool? PaCargafact { get; set; }
        public DateTime? PaFalta { get; set; }
        public string PaUserAlta { get; set; }
        public string PaGln { get; set; }
        public short? PaSituacionIb { get; set; }
        public DateTime? PaVigenciaGln { get; set; }

        public virtual ICollection<IngStkCab> IngStkCabs { get; set; }
        public virtual ICollection<Otf001> Otf001s { get; set; }
        public virtual ICollection<PreciosProvCab> PreciosProvCabs { get; set; }
        public virtual ICollection<Stf001C> Stf001Cs { get; set; }
    }
}
