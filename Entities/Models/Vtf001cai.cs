﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf001cai
    {
        public string Fcomp { get; set; }
        public int Suc { get; set; }
        public int Fdesde { get; set; }
        public int Fhasta { get; set; }
        public DateTime VtoCai { get; set; }
        public string Cai { get; set; }
    }
}
