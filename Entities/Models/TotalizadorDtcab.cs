﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TotalizadorDtcab
    {
        public TotalizadorDtcab()
        {
            TotalizadorDtdets = new HashSet<TotalizadorDtdet>();
        }

        public int IdTotalizadorDt { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public bool Anulado { get; set; }
        public int? NroAnulacion { get; set; }
        public DateTime? Fecharegistro { get; set; }
        public short? Transporte { get; set; }

        public virtual ICollection<TotalizadorDtdet> TotalizadorDtdets { get; set; }
    }
}
