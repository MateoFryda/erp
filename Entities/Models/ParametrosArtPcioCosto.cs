﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParametrosArtPcioCosto
    {
        public string IdConcepto { get; set; }
        public string Campo { get; set; }
        public string Tabla { get; set; }
        public byte Dato { get; set; }

        public virtual ParametrosPcioCosto IdConceptoNavigation { get; set; }
    }
}
