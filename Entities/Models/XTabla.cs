﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XTabla
    {
        public int IdTabla { get; set; }
        public string Descripcion { get; set; }
        public string Tabla { get; set; }
        public int CantCampos { get; set; }
        public string Obs { get; set; }
        public string TipoAbm { get; set; }
    }
}
