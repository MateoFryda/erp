﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CondPagoVencProv
    {
        public short CCod { get; set; }
        public short CDias { get; set; }
        public decimal CPorc { get; set; }
    }
}
