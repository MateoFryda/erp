﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Empresa
    {
        public string Variable { get; set; }
        public string Valor { get; set; }
        public byte[] Logo { get; set; }
    }
}
