﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzRubros20210202
    {
        public string CódigoRubro { get; set; }
        public string Descripción { get; set; }
        public string F3 { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public double? Versión { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
