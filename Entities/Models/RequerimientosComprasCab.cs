﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RequerimientosComprasCab
    {
        public RequerimientosComprasCab()
        {
            RequerimientosComprasDets = new HashSet<RequerimientosComprasDet>();
        }

        public string RcTipo { get; set; }
        public int RcNumero { get; set; }
        public DateTime RcFecha { get; set; }
        public string RcUsuario { get; set; }
        public int RcSector { get; set; }
        public string RcProveedor { get; set; }
        public string RcObservacion { get; set; }
        public short RcEstado { get; set; }
        public bool? RcProveedorexigido { get; set; }
        public byte RcMoneda { get; set; }
        public byte? RcPrioridad { get; set; }
        public DateTime? RcFechacritica { get; set; }
        public string RcCodcomprador { get; set; }
        public decimal RcCotizacion { get; set; }
        public decimal RcCotizmoneda { get; set; }
        public bool RcAnul { get; set; }
        public DateTime? RcFechaRechazo { get; set; }
        public string RcMotivo { get; set; }
        public string RcUsuarioAviso { get; set; }
        public string RcUsuariorechazo { get; set; }
        public string RcUsuarioaprob { get; set; }
        public DateTime? RcFechaaprob { get; set; }
        public string RcUsuarioanulaprob { get; set; }
        public DateTime? RcFechaanulaprob { get; set; }
        public string RcUsuarioModif { get; set; }
        public DateTime? RcFechaModif { get; set; }
        public string RcClasificacion { get; set; }

        public virtual ICollection<RequerimientosComprasDet> RequerimientosComprasDets { get; set; }
    }
}
