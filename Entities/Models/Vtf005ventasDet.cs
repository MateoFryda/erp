﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf005ventasDet
    {
        public string VeComp { get; set; }
        public byte VeSuc { get; set; }
        public int VeNro { get; set; }
        public string VaFcomprec { get; set; }
        public short VaSucrec { get; set; }
        public int VaFnrorec { get; set; }
        public DateTime VaFfech { get; set; }
        public DateTime VaFvto { get; set; }
        public decimal Importefactura { get; set; }
        public short Diasatraso { get; set; }
        public decimal Tasageneral { get; set; }
        public decimal Tasaaplicada { get; set; }
        public decimal Impcalculado { get; set; }

        public virtual Vtf005ventasCab Ve { get; set; }
    }
}
