﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class EnvioMail
    {
        public DateTime FechaAlta { get; set; }
        public string IdOrganizacion { get; set; }
        public DateTime FechaEnvio { get; set; }
        public string De { get; set; }
        public string Para { get; set; }
        public string Cc { get; set; }
        public string Co { get; set; }
        public string Asunto { get; set; }
        public string MsjTexto { get; set; }
        public string MsjAdj { get; set; }
        public string MsjTipo { get; set; }
        public string Adjunto { get; set; }
        public int? Prioridad { get; set; }
        public DateTime? FechaEnviado { get; set; }
        public int IdMail { get; set; }
        public string User { get; set; }
        public string Resp { get; set; }
    }
}
