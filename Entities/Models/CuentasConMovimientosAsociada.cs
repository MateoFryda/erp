﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CuentasConMovimientosAsociada
    {
        public string CuentaAsociada { get; set; }
        public string CgIc { get; set; }
        public string CgCc { get; set; }
        public string CgIt { get; set; }
        public double? CiTipo { get; set; }
        public string CiDesc { get; set; }
        public DateTime CgFecha { get; set; }
    }
}
