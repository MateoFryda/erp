﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XemailsConfiguracion
    {
        public string Tipo { get; set; }
        public string De { get; set; }
        public string Para { get; set; }
        public string Cc { get; set; }
        public string Co { get; set; }
        public string Resp { get; set; }
        public string Asunto { get; set; }
        public string Mesaje { get; set; }
        public string PathMensajeAdjunto { get; set; }
        public string TipoMenAdjunto { get; set; }
        public string PathArchAdjunto { get; set; }
        public byte? Prioridad { get; set; }
        public DateTime? NoEntregarAntesde { get; set; }
        public string TablaOrigen { get; set; }
        public string CampoBusqueda { get; set; }
        public string CampoEmail { get; set; }
        public string Spasociado { get; set; }
    }
}
