﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposRetencionSalidum
    {
        public int Codigo { get; set; }
        public short? CodOpAfip { get; set; }
        public short? CodImpAfip { get; set; }
        public byte CodSalida { get; set; }
        public string DescSalida { get; set; }
        public string TablaTemp1 { get; set; }
        public string SpInicio { get; set; }
        public string SpCheck { get; set; }
        public string TablaTempSalida { get; set; }
        public string SpInicioExcel { get; set; }
        public string TablaTempExcel { get; set; }
        public string TablaTempExcel2 { get; set; }
        public string SpInicioExcel2 { get; set; }
        public bool? Quincena { get; set; }
        public string TablaOrden { get; set; }
        public string LibroIvadigital { get; set; }
    }
}
