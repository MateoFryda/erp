﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PresupuestosComprasCab
    {
        public int PcNro { get; set; }
        public DateTime PcFfech { get; set; }
        public byte PcMon { get; set; }
        public decimal PcCotizmon { get; set; }
        public decimal PcCotizdolar { get; set; }
    }
}
