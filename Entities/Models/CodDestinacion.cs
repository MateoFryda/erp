﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CodDestinacion
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FDesde { get; set; }
        public DateTime? FHasta { get; set; }
    }
}
