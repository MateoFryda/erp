﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001cobranza
    {
        public Ctf001cobranza()
        {
            Ctf001s = new HashSet<Ctf001>();
        }

        public byte Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool? Dia1 { get; set; }
        public bool? Dia2 { get; set; }
        public bool? Dia3 { get; set; }
        public bool? Dia4 { get; set; }
        public bool? Dia5 { get; set; }
        public bool? Dia6 { get; set; }
        public bool? Dia7 { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
    }
}
