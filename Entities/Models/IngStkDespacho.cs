﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class IngStkDespacho
    {
        public string IhTipo { get; set; }
        public int IhSuc { get; set; }
        public int IhNro { get; set; }
        public string SaArt { get; set; }
        public string IdLote { get; set; }
        public string IdDespacho { get; set; }
        public float IdCant { get; set; }
        public float IdCantFact { get; set; }
        public decimal IdCosto1 { get; set; }
        public decimal IdCosto2 { get; set; }
        public DateTime? IdFvto { get; set; }
        public decimal? IdRegistro { get; set; }
    }
}
