﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosPlanCab
    {
        public string IdPlanImp { get; set; }
        public byte CondicionImpositiva { get; set; }
        public string Descripcion { get; set; }
        public byte Clientes { get; set; }
        public byte GeneraCompDc { get; set; }

        public virtual ImpuestosCondicion CondicionImpositivaNavigation { get; set; }
    }
}
