﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ComprasClasificacion
    {
        public int? ClCodigo { get; set; }
        public string ClDescripcion { get; set; }
        public string ClImputacion { get; set; }
    }
}
