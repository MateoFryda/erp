﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001TipoPago
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
