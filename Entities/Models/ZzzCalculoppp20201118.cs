﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzCalculoppp20201118
    {
        public string SaArt { get; set; }
        public decimal? StockAnterior { get; set; }
        public decimal? CostoLocalAnt { get; set; }
        public decimal? CostoExtranjeraAnt { get; set; }
        public decimal? Cantidad { get; set; }
        public decimal? CostoLocal { get; set; }
        public decimal? CostoExtranjera { get; set; }
        public decimal? Cotizdolar { get; set; }
        public int SgId { get; set; }
        public decimal? CostoPpplocal { get; set; }
        public decimal? CostoPppextranjera { get; set; }
    }
}
