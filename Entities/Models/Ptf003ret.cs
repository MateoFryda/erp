﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf003ret
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public string PcTiporet { get; set; }
        public int PcCpto { get; set; }

        public virtual RetencionTipo PcTiporetNavigation { get; set; }
    }
}
