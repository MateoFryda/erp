﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DepositosUbicacion
    {
        public string UbicacionFisica { get; set; }
        public string Deposito { get; set; }
        public bool? Acumula { get; set; }

        public virtual DePosito DepositoNavigation { get; set; }
    }
}
