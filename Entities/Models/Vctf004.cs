﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vctf004
    {
        public string CdFcomp { get; set; }
        public short CdSuc { get; set; }
        public decimal CdFnro { get; set; }
        public string CdCnro { get; set; }
        public string CdCn { get; set; }
        public DateTime? CdFfech { get; set; }
        public decimal CdTotal { get; set; }
        public DateTime? CdFvto { get; set; }
        public DateTime? CdFcanc { get; set; }
        public decimal CdSaldo { get; set; }
        public string CdCcanc { get; set; }
        public short? CdCsuc { get; set; }
        public decimal? CdNcanc { get; set; }
        public double? CdPordesc { get; set; }
        public decimal? CdIva { get; set; }
        public decimal? CdComi { get; set; }
        public string CdComp { get; set; }
        public string CdCta { get; set; }
        public int? CdVen { get; set; }
        public int? CdZona { get; set; }
        public string CdCodigo { get; set; }
        public int CdIdentif { get; set; }
        public bool CdSeleccion { get; set; }
        public byte CdMon { get; set; }
        public decimal CdCotiz { get; set; }
        public decimal CdCotizmon { get; set; }
        public DateTime? CdFpago { get; set; }
        public DateTime? CdFpre { get; set; }
        public byte? CDCond { get; set; }
        public bool CdDifcam { get; set; }
        public string CgCc { get; set; }
        public string CgIt { get; set; }
    }
}
