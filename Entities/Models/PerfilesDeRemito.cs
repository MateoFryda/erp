﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PerfilesDeRemito
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int? PideVendedor { get; set; }
        public string OriVendedor { get; set; }
        public int? ComVendedor { get; set; }
        public int? ComCp { get; set; }
        public int? ClaveCp { get; set; }
        public int? PideTransporte { get; set; }
        public string OriTransporte { get; set; }
        public int? ComTransporte { get; set; }
        public int? ComLista { get; set; }
        public int? ModificaAlFacturar { get; set; }
        public int? PrecioManual { get; set; }
        public int? ClaveLp { get; set; }
        public int? ComMovStk { get; set; }
        public string DepOri1 { get; set; }
        public string DepOri2 { get; set; }
        public string DepOri3 { get; set; }
        public string DepOri4 { get; set; }
        public string DepDes { get; set; }
        public int? ManejaReserva { get; set; }
        public int? ManejaPedPer { get; set; }
        public int? ListaPerfil { get; set; }
        public string OriLista { get; set; }
        public int? CampoUser1Pide { get; set; }
        public int? CampoUser1Tipo { get; set; }
        public int? CampoUser1Valida { get; set; }
        public string CampoUser1Titulo { get; set; }
        public int? CampoUser2Pide { get; set; }
        public int? CampoUser2Tipo { get; set; }
        public int? CampoUser2Valida { get; set; }
        public string CampoUser2Titulo { get; set; }
        public int? CampoUser3Pide { get; set; }
        public int? CampoUser3Tipo { get; set; }
        public int? CampoUser3Valida { get; set; }
        public string CampoUser3Titulo { get; set; }
        public int? CampoUser4Pide { get; set; }
        public int? CampoUser4Tipo { get; set; }
        public int? CampoUser4Valida { get; set; }
        public string CampoUser4Titulo { get; set; }
        public int? PideVencimiento { get; set; }
        public int? AplicaBonificacion { get; set; }
        public string OriBonificacion { get; set; }
        public int? ComBonificacion { get; set; }
        public decimal? BonifPerfil { get; set; }
        public string PerfilFacturacion { get; set; }
        public string OriCodFac { get; set; }
        public string CodFacPerfil { get; set; }
        public int? ComCodFac { get; set; }
        public int? PideCodigoDeFacturacion { get; set; }
        public int? PideLugarDeEntrega { get; set; }
        public int? RemitoDePedidos { get; set; }
        public int? ModificaCantidad { get; set; }
        public int? AgregaArticulos { get; set; }
        public string Clasificacion0 { get; set; }
        public string Clasificacion1 { get; set; }
        public string Clasificacion2 { get; set; }
        public string Clasificacion3 { get; set; }
        public string Clasificacion4 { get; set; }
        public string Clasificacion5 { get; set; }
        public string Clasificacion6 { get; set; }
        public string TipoDeComprobante { get; set; }
        public int? Sucursal { get; set; }
        public int? PideFecha { get; set; }
        public int? NoFacturable { get; set; }
        public int? P { get; set; }
        public int? RemitirAuto { get; set; }
        public byte CantCopias { get; set; }
    }
}
