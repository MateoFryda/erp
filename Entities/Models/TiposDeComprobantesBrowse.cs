﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposDeComprobantesBrowse
    {
        public string Tipo { get; set; }
        public string Descricpion { get; set; }
    }
}
