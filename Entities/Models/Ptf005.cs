﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf005
    {
        public Ptf005()
        {
            Ptf005retTablas = new HashSet<Ptf005retTabla>();
        }

        public string PeCop { get; set; }
        public int PeNro { get; set; }
        public string PePnro { get; set; }
        public DateTime PeFech { get; set; }
        public decimal PeTotal { get; set; }
        public decimal? PePordesc { get; set; }
        public decimal? PeDto { get; set; }
        public string PeObs { get; set; }
        public bool PeAnul { get; set; }
        public decimal PeReten { get; set; }
        public decimal PeInterno { get; set; }
        public decimal PeComis { get; set; }
        public decimal PeIvacomi { get; set; }
        public bool PeConfirm { get; set; }
        public string PeBenef { get; set; }
        public decimal? PeTotgrav { get; set; }
        public decimal? PeDcamb { get; set; }
        public decimal? PePordes1 { get; set; }
        public string PeUser { get; set; }
        public byte PeMon { get; set; }
        public decimal PeCotiz { get; set; }
        public decimal PeCotizmon { get; set; }
        public string PeImporteenletras { get; set; }
        public byte PeEstadotransf { get; set; }
        public byte? PeEstado { get; set; }
        public bool? PeEnviamail { get; set; }

        public virtual ICollection<Ptf005retTabla> Ptf005retTablas { get; set; }
    }
}
