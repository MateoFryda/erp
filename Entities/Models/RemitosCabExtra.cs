﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RemitosCabExtra
    {
        public string V7Tipo { get; set; }
        public short V7Suc { get; set; }
        public int V7Nro { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }
        public string Campo10 { get; set; }
        public string Campo11 { get; set; }
        public string Campo13 { get; set; }
        public string Campo14 { get; set; }
        public string Campo15 { get; set; }

        public virtual RemitosCab V7 { get; set; }
    }
}
