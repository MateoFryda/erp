﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001
    {
        public Ctf001()
        {
            Btf001s = new HashSet<Btf001>();
            Ctf001planesOs = new HashSet<Ctf001planesO>();
            InverseCaCtacteNavigation = new HashSet<Ctf001>();
            InverseCaFacturarANavigation = new HashSet<Ctf001>();
            NovedadesMaestros = new HashSet<NovedadesMaestro>();
            PedidosCabs = new HashSet<PedidosCab>();
            PresupuestosCabs = new HashSet<PresupuestosCab>();
            ProformasCabs = new HashSet<ProformasCab>();
            RemitosCabs = new HashSet<RemitosCab>();
            Stf001Clientes = new HashSet<Stf001Cliente>();
            VentasCabProformas = new HashSet<VentasCabProforma>();
            VentasCabs = new HashSet<VentasCab>();
            Vtf005s = new HashSet<Vtf005>();
        }

        public string CaCnro { get; set; }
        public string CaCnom { get; set; }
        public string CaDir { get; set; }
        public int? CaCp { get; set; }
        public string CaResp { get; set; }
        public string CaZona { get; set; }
        public string CaVen { get; set; }
        public byte CaIva { get; set; }
        public decimal CaPorex { get; set; }
        public string CaTel { get; set; }
        public string CaFax { get; set; }
        public string CaCuit { get; set; }
        public string CaIngb { get; set; }
        public short? CaCnc { get; set; }
        public short? CaCact { get; set; }
        public short? CaCpag { get; set; }
        public DateTime? CaFalta { get; set; }
        public string CaPcia { get; set; }
        public short CaTransporte { get; set; }
        public short CaLista { get; set; }
        public string CaLent { get; set; }
        public bool CaExterio { get; set; }
        public byte? CaIbcf { get; set; }
        public byte? CaTasaingb { get; set; }
        public string CaEntrega { get; set; }
        public byte? CaTipdoc { get; set; }
        public string CaLocalidad { get; set; }
        public string CaDeudor { get; set; }
        public string CaDeudordolar { get; set; }
        public string CaIcvtas { get; set; }
        public string CaProvincia { get; set; }
        public string CaContacto { get; set; }
        public string CaCodigoDeFacturacion { get; set; }
        public string CaEmail { get; set; }
        public byte? CaNc1 { get; set; }
        public byte? CaNc2 { get; set; }
        public byte? CaNc3 { get; set; }
        public string CaCtacte { get; set; }
        public string CaFacturarA { get; set; }
        public byte? CaCodDto { get; set; }
        public string CaObservaciones { get; set; }
        public int? CaAgrup { get; set; }
        public string CaCodigo { get; set; }
        public DateTime? CaInhabilitacion { get; set; }
        public byte CaClasif { get; set; }
        public string CaIdAgrupImp { get; set; }
        public short? CaPais { get; set; }
        public string CaSubZona { get; set; }
        public string CaNombreFantasia { get; set; }
        public int CaTipocliente { get; set; }
        public short CaHorario { get; set; }
        public short CaCiclo { get; set; }
        public string CaDealer { get; set; }
        public bool CaEventualCtaCte { get; set; }
        public string CaEmailCobranza { get; set; }
        public string CaCodproveedor { get; set; }
        public byte? CaCobranzas { get; set; }
        public string CaObsCobranza { get; set; }
        public string CaEmailComprobantes { get; set; }
        public bool CaCalculainteres { get; set; }
        public string CaMovil { get; set; }
        public bool CaEsclienteInterno { get; set; }
        public string CaAnticipo { get; set; }
        public string CaAnticipodolar { get; set; }
        public string CaPasswordFactura { get; set; }
        public bool? CaEsAdminFactura { get; set; }
        public string CaContactoCobranzas { get; set; }
        public string CaContactoFacturas { get; set; }
        public bool CaSinDescuento { get; set; }
        public short? CaDiasPago { get; set; }
        public string CaCobrador { get; set; }
        public string CaGln { get; set; }
        public bool CaNovalidaNivelCred { get; set; }
        public string CaUser { get; set; }
        public string CaEmail2 { get; set; }
        public short? CaSituacionIb { get; set; }
        public DateTime? CaVigenciaGln { get; set; }
        public bool? CaGranEmpresa { get; set; }

        public virtual Ctf001ciclo CaCicloNavigation { get; set; }
        public virtual Ctf001Clasif CaClasifNavigation { get; set; }
        public virtual Ctf001cobranza CaCobranzasNavigation { get; set; }
        public virtual Ctf001condPago CaCpagNavigation { get; set; }
        public virtual Ctf001 CaCtacteNavigation { get; set; }
        public virtual Ctf009 CaDeudorNavigation { get; set; }
        public virtual Ctf009 CaDeudordolarNavigation { get; set; }
        public virtual Ctf001 CaFacturarANavigation { get; set; }
        public virtual Ctf001Horario CaHorarioNavigation { get; set; }
        public virtual Ctf009 CaIcvtasNavigation { get; set; }
        public virtual ImpuestosCondicion CaIvaNavigation { get; set; }
        public virtual PreciosCab CaListaNavigation { get; set; }
        public virtual XnivelCredito CaNc1Navigation { get; set; }
        public virtual XnivelCredito CaNc2Navigation { get; set; }
        public virtual Ctf001Tipo CaTipoclienteNavigation { get; set; }
        public virtual Transporte CaTransporteNavigation { get; set; }
        public virtual Vendedore CaVenNavigation { get; set; }
        public virtual Zona CaZonaNavigation { get; set; }
        public virtual Ctf001Extra Ctf001Extra { get; set; }
        public virtual ICollection<Btf001> Btf001s { get; set; }
        public virtual ICollection<Ctf001planesO> Ctf001planesOs { get; set; }
        public virtual ICollection<Ctf001> InverseCaCtacteNavigation { get; set; }
        public virtual ICollection<Ctf001> InverseCaFacturarANavigation { get; set; }
        public virtual ICollection<NovedadesMaestro> NovedadesMaestros { get; set; }
        public virtual ICollection<PedidosCab> PedidosCabs { get; set; }
        public virtual ICollection<PresupuestosCab> PresupuestosCabs { get; set; }
        public virtual ICollection<ProformasCab> ProformasCabs { get; set; }
        public virtual ICollection<RemitosCab> RemitosCabs { get; set; }
        public virtual ICollection<Stf001Cliente> Stf001Clientes { get; set; }
        public virtual ICollection<VentasCabProforma> VentasCabProformas { get; set; }
        public virtual ICollection<VentasCab> VentasCabs { get; set; }
        public virtual ICollection<Vtf005> Vtf005s { get; set; }
    }
}
