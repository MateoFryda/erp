﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzStf007H20201118
    {
        public int SgId { get; set; }
        public DateTime SgFecha { get; set; }
        public string SgComp { get; set; }
        public int SgSuc { get; set; }
        public int SgNro { get; set; }
        public string SgOri { get; set; }
        public string SgDest { get; set; }
        public string SgCta { get; set; }
        public string SgObs { get; set; }
        public string TipoMovimientoStock { get; set; }
        public int? NroMovStock { get; set; }
        public string SgUser { get; set; }
        public string SgDeporiginal { get; set; }
        public byte? SgIdMovitoDevol { get; set; }
        public byte? SgIdMotivoDevol { get; set; }
    }
}
