﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempSubdiario1
    {
        public string Tipo { get; set; }
        public int Suc { get; set; }
        public int Nro { get; set; }
        public string Comp { get; set; }
        public DateTime Fecha { get; set; }
        public string Comprobante { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Cuit { get; set; }
        public string CondImp { get; set; }
        public byte Anulado { get; set; }
        public int? Codprov { get; set; }
        public string Provincia { get; set; }
        public decimal? Ivani { get; set; }
        public decimal? Grav1050 { get; set; }
        public decimal? Iva1050 { get; set; }
        public decimal? Grav1900 { get; set; }
        public decimal? Iva1900 { get; set; }
        public decimal? Grav2100 { get; set; }
        public decimal? Iva2100 { get; set; }
        public decimal? Grav2700 { get; set; }
        public decimal? Iva2700 { get; set; }
        public decimal? Exento { get; set; }
        public decimal? Total { get; set; }
        public decimal? Totaliibb { get; set; }
    }
}
