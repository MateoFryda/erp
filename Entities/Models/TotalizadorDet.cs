﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TotalizadorDet
    {
        public int IdTotalizador { get; set; }
        public string Tcomp { get; set; }
        public short SucComp { get; set; }
        public int Ncomp { get; set; }

        public virtual TotalizadorCab IdTotalizadorNavigation { get; set; }
    }
}
