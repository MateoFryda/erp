﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AutorizacionesEstadosH
    {
        public short? TipoOperacion { get; set; }
        public int CodAutorizacion { get; set; }
        public string Descripcion { get; set; }
    }
}
