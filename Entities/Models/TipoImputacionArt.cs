﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TipoImputacionArt
    {
        public TipoImputacionArt()
        {
            AgrupContabDets = new HashSet<AgrupContabDet>();
        }

        public byte Codigo { get; set; }
        public string Descripcion { get; set; }
        public string TipoCuenta { get; set; }
        public byte EsObligatorio { get; set; }

        public virtual ICollection<AgrupContabDet> AgrupContabDets { get; set; }
    }
}
