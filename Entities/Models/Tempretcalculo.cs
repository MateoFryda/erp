﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempretcalculo
    {
        public int CodTransac { get; set; }
        public decimal Importeret { get; set; }
        public string Tiporet { get; set; }
        public int? Cptoret { get; set; }
        public string Descripcion { get; set; }
        public string Parametro { get; set; }
        public int? Nrobanco { get; set; }
        public decimal? ImporteSujeto { get; set; }
        public decimal? Coef { get; set; }
        public decimal? Coef1 { get; set; }
        public decimal? Coef2 { get; set; }
    }
}
