﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf007cHistorica
    {
        public int SgId { get; set; }
        public int SgItem { get; set; }
        public string SaArt { get; set; }
        public decimal SgCant { get; set; }
        public decimal SgPcio { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public int? SgOrigen { get; set; }
        public int? Item { get; set; }
        public string NroPack { get; set; }
        public decimal SgPcioExt { get; set; }
        public decimal? SgCant2 { get; set; }
    }
}
