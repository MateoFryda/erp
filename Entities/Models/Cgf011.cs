﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf011
    {
        public Cgf011()
        {
            NovedadesMaestros = new HashSet<NovedadesMaestro>();
        }

        public string IcCod { get; set; }
        public string IcDesc { get; set; }
        public DateTime? Finhab { get; set; }
        public int? IcSector { get; set; }
        public int? IcTipo { get; set; }

        public virtual ICollection<NovedadesMaestro> NovedadesMaestros { get; set; }
    }
}
