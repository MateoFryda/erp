﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalCalculo
    {
        public string Cliente { get; set; }
        public string Art { get; set; }
        public decimal? PcioBruto { get; set; }
        public decimal? Cant { get; set; }
        public decimal? PcioNeto { get; set; }
        public string CodImp { get; set; }
        public byte? CodAgrupContable { get; set; }
        public int? CodTransac { get; set; }
        public string RemitoTipo { get; set; }
        public int? Remitosuc { get; set; }
        public int? RemitoNro { get; set; }
        public string CodImpuestoCli { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public byte Moneda { get; set; }
        public decimal Cotizdolar { get; set; }
        public int Orden { get; set; }
        public string Cuit { get; set; }
        public byte? ImpuestoPorCobrado { get; set; }
        public decimal? PcioNetoConIva { get; set; }
    }
}
