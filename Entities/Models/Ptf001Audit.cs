﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001Audit
    {
        public string PaCnro { get; set; }
        public string Campo { get; set; }
        public string Valor { get; set; }
        public string ValorAnt { get; set; }
        public string PaUser { get; set; }
        public DateTime? PaFalta { get; set; }
    }
}
