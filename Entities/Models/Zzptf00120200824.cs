﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzptf00120200824
    {
        public string Prov { get; set; }
        public string PaIdagrupimp { get; set; }
        public string PaCbu { get; set; }
        public byte? PaTipdoc { get; set; }
        public string PaDom { get; set; }
        public string PaEmailpagos { get; set; }
        public string PaCuit { get; set; }
    }
}
