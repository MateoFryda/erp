﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf006Audit
    {
        public string PfCop { get; set; }
        public int PfNro { get; set; }
        public string PfCcompAnt { get; set; }
        public string PfCcompNvo { get; set; }
        public int? PfCnroAnt { get; set; }
        public int? PfCnroNvo { get; set; }
        public DateTime PfFecha { get; set; }
        public TimeSpan? PfHora { get; set; }
        public string PfUser { get; set; }
    }
}
