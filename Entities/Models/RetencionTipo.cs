﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RetencionTipo
    {
        public RetencionTipo()
        {
            Ptf003rets = new HashSet<Ptf003ret>();
        }

        public string IdRetencion { get; set; }
        public string Descripcion { get; set; }
        public string CbIdent { get; set; }
        public string Parametro { get; set; }
        public int? Nrobanco { get; set; }
        public bool PorFactura { get; set; }
        public string Sbrowse { get; set; }
        public string Validacion { get; set; }
        public string TituloReporte { get; set; }
        public string NombreReporte { get; set; }

        public virtual ICollection<Ptf003ret> Ptf003rets { get; set; }
    }
}
