﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PuntosDeVentum
    {
        public short Punto { get; set; }
        public string Tipo { get; set; }
        public int? Desde { get; set; }
        public int UltimoNumero { get; set; }
        public int? Copias { get; set; }
        public string SpAvance { get; set; }
        public string Reporte { get; set; }
        public bool ConImpFiscal { get; set; }
        public string ModeloControlador { get; set; }
        public string Impresora { get; set; }
        public short CantidadLineas { get; set; }
        public string Ccosto { get; set; }
        public bool Manual { get; set; }
        public byte ValidacionFecha { get; set; }
        public bool Electronico { get; set; }
        public string Reporte1 { get; set; }
        public string Reporte2 { get; set; }
        public bool? Ncdautomaticas { get; set; }
        public int? AfipWebService { get; set; }
        public short? PuntoRx { get; set; }
        public string LeyendaVta { get; set; }
        public bool? InformeDoc { get; set; }
        public string Reporte3 { get; set; }
        public string Reporte4 { get; set; }
        public bool? ExtraerSap { get; set; }
        public int? Moneda { get; set; }
        public string Descripcion { get; set; }
        public bool? EsInterno { get; set; }
        public bool? ImprimeSerieLote { get; set; }
        public bool? EsFce { get; set; }
        public string NombreFx { get; set; }

        public virtual Cgf010 CcostoNavigation { get; set; }
        public virtual PuntosDeVentaLock PuntosDeVentaLock { get; set; }
    }
}
