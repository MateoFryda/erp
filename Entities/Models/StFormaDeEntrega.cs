﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StFormaDeEntrega
    {
        public StFormaDeEntrega()
        {
            StRecepcions = new HashSet<StRecepcion>();
        }

        public string SCodFormaDeEntrega { get; set; }
        public string SDescFormaDeEntrega { get; set; }
        public int? NHab { get; set; }
        public int? NPideDireccionEntrega { get; set; }
        public short? TaCodigo { get; set; }

        public virtual ICollection<StRecepcion> StRecepcions { get; set; }
    }
}
