﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgfcompensh
    {
        public int GpNro { get; set; }
        public DateTime GpFecha { get; set; }
        public string GpUser { get; set; }
        public DateTime GpFechatran { get; set; }
        public bool? GpAnul { get; set; }
        public int CgNro { get; set; }
        public string CgIc { get; set; }

        public virtual Ctf009 CgIcNavigation { get; set; }
    }
}
