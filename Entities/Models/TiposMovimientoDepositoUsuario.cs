﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposMovimientoDepositoUsuario
    {
        public string Codigo { get; set; }
        public string Usuario { get; set; }
    }
}
