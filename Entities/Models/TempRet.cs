﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempRet
    {
        public int CodTransac { get; set; }
        public string Proveedor { get; set; }
        public DateTime? Fecha { get; set; }
        public byte? Moneda { get; set; }
        public decimal? CotizMon { get; set; }
        public decimal? Cotizdolar { get; set; }
        public decimal? Aliciva { get; set; }
        public string Fcomp { get; set; }
        public int? Fnro { get; set; }
        public decimal? Importe { get; set; }
        public DateTime? Fvto { get; set; }
    }
}
