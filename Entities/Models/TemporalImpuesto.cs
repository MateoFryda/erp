﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalImpuesto
    {
        public string Art { get; set; }
        public string Remitotipo { get; set; }
        public int? Remitosuc { get; set; }
        public int? RemitoNro { get; set; }
        public string IdImpuesto { get; set; }
        public string Idcalculo { get; set; }
        public int Codtransac { get; set; }
        public string Descripcion { get; set; }
        public string ImputacionVentas { get; set; }
        public string ImputacionCompras { get; set; }
        public decimal? Impuesto { get; set; }
        public decimal Alicuota { get; set; }
        public decimal? Base { get; set; }
        public decimal? Exento { get; set; }
        public decimal? ImpuestoExt { get; set; }
        public decimal? BaseExt { get; set; }
        public decimal? ExentoExt { get; set; }
        public int? Orden { get; set; }
        public decimal? TotalConIva { get; set; }
    }
}
