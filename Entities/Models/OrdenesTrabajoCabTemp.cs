﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class OrdenesTrabajoCabTemp
    {
        public OrdenesTrabajoCabTemp()
        {
            OrdenesTrabajoDetTemps = new HashSet<OrdenesTrabajoDetTemp>();
        }

        public int OtNroOt { get; set; }
        public string OtArt { get; set; }
        public string OtDetalle { get; set; }
        public decimal? OtCantProd { get; set; }
        public DateTime? OtFechaEstimada { get; set; }
        public DateTime OtFfech { get; set; }
        public string OtObservaciones { get; set; }
        public short OtEstado { get; set; }
        public DateTime? OtFechaCierre { get; set; }
        public int? OtNroOtgen { get; set; }
        public short? OtLineaOtgen { get; set; }
        public bool OtAnul { get; set; }
        public string NroLote { get; set; }
        public string TipoLote { get; set; }

        public virtual ICollection<OrdenesTrabajoDetTemp> OrdenesTrabajoDetTemps { get; set; }
    }
}
