﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UpdateDinamicoTabla
    {
        public string Tabla { get; set; }
        public string SpUpdate { get; set; }
        public string Descripcion { get; set; }
        public string Info { get; set; }
        public string SpBackUp { get; set; }
        public bool HabilitarAlta { get; set; }
    }
}
