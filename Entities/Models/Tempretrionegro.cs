﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempretrionegro
    {
        public string NumeroDeRenglon { get; set; }
        public string OrigenDelComprobante { get; set; }
        public string TipoDeComprobante { get; set; }
        public string NumeroDeComprobante { get; set; }
        public string Cuit { get; set; }
        public string FechaDeRetencion { get; set; }
        public string MontoSujetoARetencion { get; set; }
        public string Alicuota { get; set; }
        public string MontoRetenido { get; set; }
        public string TipoDeRegimenDeRetencion { get; set; }
        public string Jurisdiccion { get; set; }
    }
}
