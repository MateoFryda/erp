﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Btf001
    {
        public DateTime? BaFec { get; set; }
        public string BaTipo { get; set; }
        public DateTime? BaFecch { get; set; }
        public string BaClnro { get; set; }
        public string BaCheque { get; set; }
        public string BaBanco { get; set; }
        public string BaPlaza { get; set; }
        public decimal BaImporte { get; set; }
        public int BaNro { get; set; }
        public DateTime? BaFcan { get; set; }
        public string BaCcan { get; set; }
        public int? BaNrocan { get; set; }
        public double? BaImput { get; set; }
        public string BaEntrega { get; set; }
        public int? BaRecibo { get; set; }
        public string BaComp { get; set; }
        public short? BaSuc { get; set; }
        public int? BaClearing { get; set; }
        public bool BaRechazo { get; set; }
        public string BaDepComo { get; set; }
        public string BaMotivoRe { get; set; }
        public string BaEstadoNeg { get; set; }
        public string BaCuit { get; set; }
        public string BaFirmante { get; set; }
        public short? BaSuccan { get; set; }

        public virtual Ctf001 BaClnroNavigation { get; set; }
    }
}
