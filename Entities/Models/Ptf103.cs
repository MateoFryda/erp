﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf103
    {
        public string PaFnro { get; set; }
        public string PaCnom { get; set; }
        public string PaCuit { get; set; }
        public double? PaIva { get; set; }
        public double? PaNro { get; set; }
    }
}
