﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposDeComprobante
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Modulo { get; set; }
        public string CodAfip { get; set; }
        public string Tcid { get; set; }
        public string CodAfipComplementario { get; set; }
    }
}
