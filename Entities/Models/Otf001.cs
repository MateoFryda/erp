﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Otf001
    {
        public Otf001()
        {
            IngStkDets = new HashSet<IngStkDet>();
            Otf002s = new HashSet<Otf002>();
        }

        public int OaNro { get; set; }
        public string OaPnro { get; set; }
        public DateTime? OaFfech { get; set; }
        public int? OaTiva { get; set; }
        public decimal? OaPorex { get; set; }
        public byte? OaCpag { get; set; }
        public DateTime? OaFvto { get; set; }
        public string OaObs { get; set; }
        public decimal? OaPordesc { get; set; }
        public string OaEnt { get; set; }
        public byte? OaMon { get; set; }
        public double? OaCotizmon { get; set; }
        public double? OaCotizdolar { get; set; }
        public int? OaCumplida { get; set; }
        public short OaTransporte { get; set; }
        public bool? OaAnul { get; set; }
        public string OaUsuario { get; set; }
        public int? OaSector { get; set; }
        public short? OaEstado { get; set; }
        public byte? OaPrioridad { get; set; }
        public DateTime? OaFechaCritica { get; set; }
        public DateTime? OaFechaRechazo { get; set; }
        public string OaMotivo { get; set; }
        public string OaUsuarioAviso { get; set; }
        public string OaUsuarioRechazo { get; set; }
        public string OaUsuarioAprob { get; set; }
        public DateTime? OaFechaAprob { get; set; }
        public string OaUsuarioAnulaAprob { get; set; }
        public DateTime? OaFechaAnulaAprob { get; set; }
        public string OaDeposito { get; set; }
        public int? OaModeloReporte { get; set; }
        public string OaOcproveedor { get; set; }

        public virtual Ptf001 OaPnroNavigation { get; set; }
        public virtual Otf001CabExtra Otf001CabExtra { get; set; }
        public virtual Otf001Entrega Otf001Entrega { get; set; }
        public virtual ICollection<IngStkDet> IngStkDets { get; set; }
        public virtual ICollection<Otf002> Otf002s { get; set; }
    }
}
