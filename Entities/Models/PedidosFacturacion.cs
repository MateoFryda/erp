﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosFacturacion
    {
        public string Tipo { get; set; }
        public byte Suc { get; set; }
        public int Nro { get; set; }
        public DateTime Fvtocuota { get; set; }
        public decimal Pcio { get; set; }
        public double Porcentaje { get; set; }
        public short? Diasvto { get; set; }
        public byte Estado { get; set; }
        public string VaFcomp { get; set; }
        public short? VaSuc { get; set; }
        public int? VaFnro { get; set; }
    }
}
