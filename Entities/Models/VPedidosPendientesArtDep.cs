﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VPedidosPendientesArtDep
    {
        public int VcNro { get; set; }
        public string SaArt { get; set; }
        public string SaFamilia { get; set; }
        public string SaRubro { get; set; }
        public string SaCodFabricante { get; set; }
        public short SaClasif { get; set; }
        public string SaProvVig { get; set; }
        public string DepCodigo { get; set; }
    }
}
