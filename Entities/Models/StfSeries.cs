﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StfSeries
    {
        public string SaArt { get; set; }
        public string SeSerie { get; set; }
        public DateTime SeFechaDespacho { get; set; }
        public int SeAduana { get; set; }
        public int SePartida { get; set; }
        public string SeDespacho { get; set; }
        public string SeFcomp { get; set; }
        public int? SeFsuc { get; set; }
        public int? SeFnro { get; set; }
        public string SeTipoDeComprobante { get; set; }
        public string SeDepositoEnQueEsta { get; set; }
    }
}
