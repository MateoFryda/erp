﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsTemporal
    {
        public int IdOperacion { get; set; }
        public int NroOrdenGrilla { get; set; }
        public string Deposito { get; set; }
        public string Articulo { get; set; }
        public string DescArt { get; set; }
        public decimal Cantidad { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public decimal? Precio { get; set; }
        public decimal? PrecioLista { get; set; }
        public decimal? Dto1 { get; set; }
        public decimal? Dto2 { get; set; }
        public decimal? Dto3 { get; set; }
        public decimal? Dto4 { get; set; }
        public bool? LlevaStock { get; set; }
        public bool? EsIngreso { get; set; }
        public decimal? PcioNeto { get; set; }
        public string TxtDto1 { get; set; }
        public string TxtDto2 { get; set; }
        public string TxtDto3 { get; set; }
        public string TxtDto4 { get; set; }
        public string NroPack { get; set; }
        public int? CantPack { get; set; }
        public string CodImpuestos { get; set; }
        public string RemitoTipo { get; set; }
        public int? RemitoSuc { get; set; }
        public int? RemitoNro { get; set; }
        public string PedidoTipo { get; set; }
        public int? PedidoSuc { get; set; }
        public string PedidoNro { get; set; }
        public string CodImpuesto { get; set; }
        public string CodCalculo { get; set; }
        public decimal? Alicuota { get; set; }
        public decimal? Impuesto { get; set; }
        public decimal? Base { get; set; }
        public decimal? Exento { get; set; }
        public decimal? Precioland { get; set; }
        public decimal? CantidadReal { get; set; }
        public string Tipodev { get; set; }
        public int? Sucdev { get; set; }
        public int? Nrodev { get; set; }
        public int? Item { get; set; }
        public decimal? CostoStdlocal { get; set; }
        public decimal? CostoStdext { get; set; }
        public decimal? CostoUclocal { get; set; }
        public decimal? CostoUcext { get; set; }
        public decimal? CostoPpplocal { get; set; }
        public decimal? CostoPppext { get; set; }
        public decimal? CostoFifolocal { get; set; }
        public decimal? CostoFifoext { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public decimal? PppartLote { get; set; }
        public string TipodeRem { get; set; }
        public decimal? PppartLoteLand { get; set; }
        public decimal? ImpuestoExt { get; set; }
        public decimal? BaseExt { get; set; }
        public decimal? ExentoExt { get; set; }
        public decimal? PrecioCiva { get; set; }
        public decimal? PrecioNetociva { get; set; }
    }
}
