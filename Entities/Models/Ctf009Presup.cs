﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf009Presup
    {
        public string CiNro { get; set; }
        public string CiDesc { get; set; }
        public byte CiNivel { get; set; }
        public string CiAcumul { get; set; }
        public string CiClasif { get; set; }
        public byte? CiTipo { get; set; }
        public string CiAgrup { get; set; }
        public byte? CiMon { get; set; }
        public DateTime? Finhab { get; set; }
        public string CiCtaasoc { get; set; }
        public string CiCtaasocconsolidacion { get; set; }
        public bool? CiAsientoman { get; set; }
        public string CiAjuste { get; set; }
    }
}
