﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001exencion
    {
        public string CaCnro { get; set; }
        public string ImpuestoCodigo { get; set; }
        public decimal Porcentaje { get; set; }
        public DateTime? FechaHasta { get; set; }

        public virtual Ctf001 CaCnroNavigation { get; set; }
        public virtual ImpuestosCodigo ImpuestoCodigoNavigation { get; set; }
    }
}
