﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf003cai
    {
        public string PcCuit { get; set; }
        public int PcPtoVenta { get; set; }
        public string PcCai { get; set; }
        public DateTime PcVtoCai { get; set; }
    }
}
