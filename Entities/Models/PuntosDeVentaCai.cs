﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PuntosDeVentaCai
    {
        public short? Punto { get; set; }
        public string Tipo { get; set; }
        public int? Desde { get; set; }
        public int? Ultimonumero { get; set; }
        public string CaiNro { get; set; }
        public DateTime? CaiVto { get; set; }
    }
}
