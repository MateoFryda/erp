﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposdeComprobanteIva
    {
        public byte CondicionImpositiva { get; set; }
        public string TipoComprobante { get; set; }
        public short Punto { get; set; }
        public string TipoFactura { get; set; }
        public int Id { get; set; }
    }
}
