﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TmpVentasCabExtra
    {
        public string TipoAsoc { get; set; }
        public int? SucAsoc { get; set; }
        public int? NroAsoc { get; set; }
        public string CodigoAnulacion { get; set; }
        public string Tipo { get; set; }
        public int Suc { get; set; }
        public int Nro { get; set; }
    }
}
