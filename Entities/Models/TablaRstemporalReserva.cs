﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRstemporalReserva
    {
        public int IdOperacion { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
    }
}
