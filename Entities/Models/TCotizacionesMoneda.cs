﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TCotizacionesMoneda
    {
        public int CMoneda { get; set; }
        public int CCotizacion { get; set; }
        public DateTime FhCotizacion { get; set; }
        public decimal Cotizacion { get; set; }
    }
}
