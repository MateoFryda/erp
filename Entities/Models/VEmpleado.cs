﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VEmpleado
    {
        public string Codigo { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
    }
}
