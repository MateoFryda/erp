﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposSubcostoCuenta
    {
        public byte IdTipoSubcosto { get; set; }
        public string Cuenta { get; set; }
    }
}
