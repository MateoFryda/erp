﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposMovimientoStockTrazaEvento
    {
        public string Codigo { get; set; }
        public int IdEvento { get; set; }
    }
}
