﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParameTrosCashFlow
    {
        public string Imputacion { get; set; }
        public string ImputacionDesc { get; set; }
    }
}
