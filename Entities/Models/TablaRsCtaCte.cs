﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsCtaCte
    {
        public int IdOperacion { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Importe { get; set; }
    }
}
