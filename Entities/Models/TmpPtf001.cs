﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TmpPtf001
    {
        public string PaCnro { get; set; }
        public string PaCnom { get; set; }
        public string PaDom { get; set; }
        public string PaCp { get; set; }
        public string PaResp { get; set; }
        public string PaTele { get; set; }
        public string PaFax { get; set; }
        public byte? PaCpag { get; set; }
        public string PaObs { get; set; }
        public byte? PaCact { get; set; }
        public byte? PaCcpto { get; set; }
        public byte? PaIva { get; set; }
        public string PaCuit { get; set; }
        public byte? PaGan { get; set; }
        public double? PaCoefadi { get; set; }
        public double? PaOrigen { get; set; }
        public string PaImpcom { get; set; }
        public byte? PaTipdoc { get; set; }
        public byte? PaRetiv { get; set; }
        public string PaImppro { get; set; }
        public string PaBenef { get; set; }
        public string PaUser { get; set; }
        public string PaEmail { get; set; }
        public byte? PaActec { get; set; }
        public int? PaTae { get; set; }
        public string PaIngbtos { get; set; }
        public double? PaSaldoan { get; set; }
        public double? PaSaldol { get; set; }
        public double? PaSaldol2 { get; set; }
        public double? PaSaldoan2 { get; set; }
        public double? PaSaldoa2 { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string PaProvincia { get; set; }
        public string PaImpprodolar { get; set; }
        public string PaImpproom { get; set; }
        public string PaCbu { get; set; }
        public byte PaRetingbr { get; set; }
        public byte PaRetsuss { get; set; }
        public string PaCcosto { get; set; }
        public string PaSubcosto { get; set; }
    }
}
