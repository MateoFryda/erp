﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CondFacturacion
    {
        public CondFacturacion()
        {
            CondFacturacionVencs = new HashSet<CondFacturacionVenc>();
        }

        public short Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool Escontado { get; set; }
        public int? IdDto { get; set; }

        public virtual ICollection<CondFacturacionVenc> CondFacturacionVencs { get; set; }
    }
}
