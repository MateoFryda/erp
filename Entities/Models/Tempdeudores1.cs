﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempdeudores1
    {
        public string Empresa { get; set; }
        public DateTime? FechaEmisión { get; set; }
        public string Comprobante { get; set; }
        public decimal SaldoPesos { get; set; }
        public string MonedaOrigen { get; set; }
        public string Cotización { get; set; }
    }
}
