﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasImpg1
    {
        public string Fcomp { get; set; }
        public short Fsuc { get; set; }
        public int Fnro { get; set; }
        public decimal Iibb { get; set; }
        public decimal IibbExt { get; set; }
        public decimal Iibbper { get; set; }
        public decimal IibbperExt { get; set; }
        public decimal Ivaper { get; set; }
        public decimal IvaperExt { get; set; }
    }
}
