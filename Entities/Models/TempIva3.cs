﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempIva3
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public int? Cantidad { get; set; }
    }
}
