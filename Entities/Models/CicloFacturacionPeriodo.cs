﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CicloFacturacionPeriodo
    {
        public short IdCiclo { get; set; }
        public string Periodo { get; set; }
        public DateTime FechaProceso { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string Observaciones { get; set; }
        public byte Estado { get; set; }
        public DateTime? FechaCalculoInteres { get; set; }

        public virtual Ctf001ciclo IdCicloNavigation { get; set; }
    }
}
