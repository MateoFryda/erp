﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf002
    {
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public string SaArt { get; set; }
        public int? SgId { get; set; }
        public float VbCant { get; set; }
        public decimal VbPcio { get; set; }
        public decimal VbIva1 { get; set; }
        public decimal? VbInterno { get; set; }
        public int? VbAnul { get; set; }
        public decimal? VbCost { get; set; }
        public string VbNrodesp { get; set; }
        public int? VbAduana { get; set; }
        public string VbDetalle { get; set; }
        public double? VbDto { get; set; }
        public decimal? VbPcioLista { get; set; }
        public double VbComision { get; set; }
        public int? VbId { get; set; }
        public int Idd { get; set; }
    }
}
