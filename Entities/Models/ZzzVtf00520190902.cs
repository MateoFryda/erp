﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzVtf00520190902
    {
        public string VeComp { get; set; }
        public byte VeSuc { get; set; }
        public int VeNro { get; set; }
        public string VeClnro { get; set; }
        public DateTime VeFech { get; set; }
        public decimal VeTotal { get; set; }
        public decimal? VePordesc { get; set; }
        public decimal? VeDto { get; set; }
        public double? VeProvis { get; set; }
        public double? VeVen { get; set; }
        public string VeObs { get; set; }
        public bool VeAnul { get; set; }
        public string VeUser { get; set; }
        public byte VeMon { get; set; }
        public decimal VeCotiz { get; set; }
        public decimal VeCotizmon { get; set; }
        public string VeUbicacion { get; set; }
        public DateTime? VeFpagoOriginal { get; set; }
        public byte? VeMonedaOriginal { get; set; }
        public decimal? VeImporteOriginal { get; set; }
        public decimal? VeCotizOriginal { get; set; }
        public string VeImporteenletras { get; set; }
        public byte? VeEstado { get; set; }
        public bool? VeEnviamail { get; set; }
        public DateTime? Fregistro { get; set; }
        public string VeCodigo { get; set; }
        public decimal? PromDiasPago { get; set; }
    }
}
