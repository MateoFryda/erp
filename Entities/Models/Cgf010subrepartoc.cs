﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010subrepartoc
    {
        public string CcCodsubreparto { get; set; }
        public string CcCod { get; set; }
        public string CcPorcentaje { get; set; }
    }
}
