﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DtoVentasCab
    {
        public int IdDto { get; set; }
        public bool Grupal { get; set; }
        public bool Aplicacion { get; set; }
        public string Descrip { get; set; }
        public bool ParamEspecial { get; set; }
        public bool GrupalLimite { get; set; }
        public int? CantidadLimite { get; set; }
    }
}
