﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Umed
    {
        public string SaArt { get; set; }
        public string Codigo { get; set; }
        public decimal Cantidad { get; set; }

        public virtual UnidadMedidum CodigoNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
