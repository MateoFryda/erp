﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProformasImpuesto
    {
        public int CodProforma { get; set; }
        public string ImpuestoCodigo { get; set; }
        public decimal Alicuota { get; set; }
        public decimal VbGravadoLocal { get; set; }
        public decimal VbGravadoExtranjera { get; set; }
        public decimal VbExentoLocal { get; set; }
        public decimal VbExentoExtranjera { get; set; }
        public decimal VbImpuestoLocal { get; set; }
        public decimal VbImpuestoExtranjera { get; set; }

        public virtual ProformasCab CodProformaNavigation { get; set; }
    }
}
