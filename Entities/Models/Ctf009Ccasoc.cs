﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf009Ccasoc
    {
        public string CiNro { get; set; }
        public string CcCod { get; set; }
        public string CiCtaasoc { get; set; }
    }
}
