﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001planesO
    {
        public int CaNroPlan { get; set; }
        public string CaCnro { get; set; }
        public string CaDesc { get; set; }
        public string CaPorcentaje { get; set; }

        public virtual Ctf001 CaCnroNavigation { get; set; }
    }
}
