﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf009
    {
        public Ctf009()
        {
            AgrupContabDets = new HashSet<AgrupContabDet>();
            Cgfcompenshes = new HashSet<Cgfcompensh>();
            Ctf001CaDeudorNavigations = new HashSet<Ctf001>();
            Ctf001CaDeudordolarNavigations = new HashSet<Ctf001>();
            Ctf001CaIcvtasNavigations = new HashSet<Ctf001>();
            ImpuestosCalculoImputacionComprasNavigations = new HashSet<ImpuestosCalculo>();
            ImpuestosCalculoImputacionVentasNavigations = new HashSet<ImpuestosCalculo>();
        }

        public string CiNro { get; set; }
        public string CiDesc { get; set; }
        public byte? CiNivel { get; set; }
        public string CiAcumul { get; set; }
        public string CiMonet { get; set; }
        public string CiClasif { get; set; }
        public string CiCcosto { get; set; }
        public string CiReic { get; set; }
        public byte? CiTipo { get; set; }
        public string CiHab { get; set; }
        public string CiCtaasoc { get; set; }
        public string CiCtaasocconsolidacion { get; set; }
        public string CiUser { get; set; }
        public bool? CiListacc { get; set; }
        public bool? CiAsientoman { get; set; }
        public byte CiMon { get; set; }
        public bool CiReexpresa { get; set; }
        public string CiAjuste { get; set; }
        public bool CiCompensable { get; set; }
        public DateTime? Finhab { get; set; }
        public string CiCtaasocpresup { get; set; }
        public string CiAxi { get; set; }
        public bool? CiTcreporte { get; set; }
        public string CodigoToolsrrhh { get; set; }

        public virtual ICollection<AgrupContabDet> AgrupContabDets { get; set; }
        public virtual ICollection<Cgfcompensh> Cgfcompenshes { get; set; }
        public virtual ICollection<Ctf001> Ctf001CaDeudorNavigations { get; set; }
        public virtual ICollection<Ctf001> Ctf001CaDeudordolarNavigations { get; set; }
        public virtual ICollection<Ctf001> Ctf001CaIcvtasNavigations { get; set; }
        public virtual ICollection<ImpuestosCalculo> ImpuestosCalculoImputacionComprasNavigations { get; set; }
        public virtual ICollection<ImpuestosCalculo> ImpuestosCalculoImputacionVentasNavigations { get; set; }
    }
}
