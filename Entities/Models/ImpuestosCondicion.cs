﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosCondicion
    {
        public ImpuestosCondicion()
        {
            Ctf001s = new HashSet<Ctf001>();
            ImpuestosCondicionDocumentos = new HashSet<ImpuestosCondicionDocumento>();
            ImpuestosPlanCabs = new HashSet<ImpuestosPlanCab>();
        }

        public byte Codigo { get; set; }
        public string Descripcion { get; set; }
        public string CodAfip { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
        public virtual ICollection<ImpuestosCondicionDocumento> ImpuestosCondicionDocumentos { get; set; }
        public virtual ICollection<ImpuestosPlanCab> ImpuestosPlanCabs { get; set; }
    }
}
