﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UpdateDinamicoCampo
    {
        public string Tabla { get; set; }
        public string Campo { get; set; }
        public string Descripcion { get; set; }
        public string SpValidacion { get; set; }
        public bool FcnValidacion { get; set; }
        public bool Clave { get; set; }
        public byte Orden { get; set; }
        public bool PermiteVacio { get; set; }
        public string Agrupacion { get; set; }
        public bool? Obligatorio { get; set; }

        public virtual UpdateDinamicoTabla TablaNavigation { get; set; }
    }
}
