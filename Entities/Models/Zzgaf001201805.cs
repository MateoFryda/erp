﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzgaf001201805
    {
        public double? GaCod { get; set; }
        public double? GaMes { get; set; }
        public double? GaImporte { get; set; }
        public double? GaMinimo { get; set; }
    }
}
