﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RetenciontiposAnteriore
    {
        public string IdRetencion { get; set; }
        public string Descripcion { get; set; }
        public string CbIdent { get; set; }
        public string Parametro { get; set; }
        public int? Nrobanco { get; set; }
        public bool PorFactura { get; set; }
    }
}
