﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XParamConsultaFactura
    {
        public string Descripcion { get; set; }
        public string SpConsultaFacturas { get; set; }
        public string Tipo { get; set; }
    }
}
