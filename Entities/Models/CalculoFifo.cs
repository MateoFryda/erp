﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CalculoFifo
    {
        public string SaArt { get; set; }
        public decimal CostoLocal { get; set; }
        public decimal CostoExtranjera { get; set; }
        public int Cantidad { get; set; }
        public int SgId { get; set; }
        public int SgIdOrigen { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public bool EsLifo { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
        public virtual Stf007h Sg { get; set; }
        public virtual Stf007h SgIdOrigenNavigation { get; set; }
    }
}
