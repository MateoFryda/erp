﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzbtf00220200812
    {
        public DateTime? BbFec { get; set; }
        public DateTime? BbFvto { get; set; }
        public short BbBanco { get; set; }
        public int BbNro { get; set; }
        public decimal BbImporte { get; set; }
        public string BbEntrega { get; set; }
        public string BbTipo { get; set; }
        public int? BbRecibo { get; set; }
        public string BbCcomp { get; set; }
        public string BbFcprov { get; set; }
        public int? BbCnro { get; set; }
        public DateTime? BbFecsal { get; set; }
        public string BbDiferido { get; set; }
        public int BbIdent { get; set; }
        public bool BbCtrlimpresion { get; set; }
        public string CodProvedor { get; set; }
        public string ApellidoYNombre { get; set; }
        public string Mail { get; set; }
        public string TipoDoc { get; set; }
        public double? NroDoc { get; set; }
        public string SocTrabaja { get; set; }
        public double? Legajo { get; set; }
        public string Sa { get; set; }
        public string Sh { get; set; }
        public string Srl { get; set; }
    }
}
