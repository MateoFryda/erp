﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ComandoDia
    {
        public int Itemdia { get; set; }
        public int? Propago { get; set; }
        public int? Procobro { get; set; }
        public int? Antideuda { get; set; }
        public int? Anticredi { get; set; }
    }
}
