﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PuntosDeVentaLock
    {
        public short Punto { get; set; }
        public string Tipo { get; set; }
        public bool ConImpFiscal { get; set; }
        public int UltimoNumeroImpreso { get; set; }

        public virtual PuntosDeVentum PuntosDeVentum { get; set; }
    }
}
