﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf002
    {
        public string SbIdent { get; set; }
        public int SbCod { get; set; }
        public string SbDesc { get; set; }
        public decimal? SbCoef { get; set; }
    }
}
