﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosProvDet
    {
        public int IdLista { get; set; }
        public string ScCodProveedor { get; set; }
        public string SaArt { get; set; }
        public string ScDescArtProv { get; set; }
        public decimal? PrecioLista { get; set; }
        public decimal? Dto1 { get; set; }
        public decimal? Dto2 { get; set; }
        public decimal? Dto3 { get; set; }
        public decimal? Dto4 { get; set; }
        public decimal? Dto5 { get; set; }
        public DateTime? FechaModif { get; set; }

        public virtual PreciosProvCab IdListaNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
