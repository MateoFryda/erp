﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Hoja1
    {
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public double? IdCpto { get; set; }
        public string Descrip { get; set; }
        public string Artículo { get; set; }
        public string Finicio { get; set; }
        public string FechaFinal { get; set; }
        public double? Cuotas { get; set; }
        public string MuestraCuotas { get; set; }
        public string Ccosto { get; set; }
        public string Subcosto { get; set; }
        public string F12 { get; set; }
        public string F13 { get; set; }
        public decimal? Monto { get; set; }
    }
}
