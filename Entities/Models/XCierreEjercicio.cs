﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XCierreEjercicio
    {
        public byte Contabilidad { get; set; }
        public string FechaCierre { get; set; }
        public DateTime? FechaProceso { get; set; }
        public string UsuarioProceso { get; set; }
    }
}
