﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaIngStkCab
    {
        public int IdIngreso { get; set; }
        public int? SgId { get; set; }
        public string IhProveedor { get; set; }
        public string IhFcprovcomp { get; set; }
        public string IhFcprovsuc { get; set; }
        public string IhFcprovFnro { get; set; }
        public string IhObs { get; set; }
        public string IhRemProvcomp { get; set; }
        public string IhRemProvsuc { get; set; }
        public string IhRemProvFnro { get; set; }
        public int? IhNroingmodificado { get; set; }
        public string IhDespacho { get; set; }
        public int? IhAduana { get; set; }
        public DateTime? IhFvtoDespacho { get; set; }
        public int? IhFnro { get; set; }
        public byte IhMon { get; set; }
        public decimal IhCotizmon { get; set; }
        public decimal IhCotizdolar { get; set; }
        public DateTime? IhFecha { get; set; }
        public bool? IhAnul { get; set; }
        public string IhUsuario { get; set; }
        public DateTime? Fregistro { get; set; }
        public string IhOrigenDespacho { get; set; }
        public int? IhEstado { get; set; }
        public decimal? TotalDescuentos { get; set; }
        public decimal? SicrAjustes { get; set; }

        public virtual TrazaIngresoEstado IhEstadoNavigation { get; set; }
    }
}
