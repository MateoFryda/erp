﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Buf001
    {
        public string BaCod { get; set; }
        public string BaCcont { get; set; }
        public string BaCconas { get; set; }
        public string BaDesc { get; set; }
        public DateTime? BaFecbaj { get; set; }
        public DateTime? BaFecalt { get; set; }
        public double? BaCostor { get; set; }
        public double? BaCoef { get; set; }
        public string BaCconam { get; set; }
        public string BaObs { get; set; }
    }
}
