﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosPlanDet
    {
        public string IdPlanImp { get; set; }
        public string ImpuestoCodigo { get; set; }
        public byte OrdenCalculo { get; set; }

        public virtual ImpuestosCodigo ImpuestoCodigoNavigation { get; set; }
    }
}
