﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StMedioIngreso
    {
        public StMedioIngreso()
        {
            StRecepcions = new HashSet<StRecepcion>();
        }

        public string SCodMedioIngreso { get; set; }
        public string SDescMedioIngreso { get; set; }
        public int? NHab { get; set; }

        public virtual ICollection<StRecepcion> StRecepcions { get; set; }
    }
}
