﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TotalizadorCab
    {
        public TotalizadorCab()
        {
            TotalizadorDets = new HashSet<TotalizadorDet>();
            TotalizadorDtdets = new HashSet<TotalizadorDtdet>();
        }

        public int IdTotalizador { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public bool Anulado { get; set; }
        public int? NroAnulacion { get; set; }
        public DateTime? Fecharegistro { get; set; }
        public string Observaciones { get; set; }

        public virtual ICollection<TotalizadorDet> TotalizadorDets { get; set; }
        public virtual ICollection<TotalizadorDtdet> TotalizadorDtdets { get; set; }
    }
}
