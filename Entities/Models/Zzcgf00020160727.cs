﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzcgf00020160727
    {
        public int CgNro { get; set; }
        public string CgIc { get; set; }
        public DateTime CgFecha { get; set; }
        public string CgDesc { get; set; }
        public decimal CgImporte { get; set; }
        public decimal? CgCotiz { get; set; }
        public bool CgExtra { get; set; }
        public int? CgNro1 { get; set; }
        public int? CgNroch { get; set; }
        public string CgTcomp { get; set; }
        public int? CgNcomp { get; set; }
        public string CgTipo { get; set; }
        public string CgCta { get; set; }
        public string CgCc { get; set; }
        public int? CgSuc { get; set; }
        public string CgIt { get; set; }
        public string CgUser { get; set; }
        public DateTime? CgFdebcred { get; set; }
        public int? CgLinea { get; set; }
        public int Lin1 { get; set; }
        public DateTime? FhUmodif { get; set; }
        public DateTime? FhAlta { get; set; }
        public byte CgContab { get; set; }
        public byte CgMon { get; set; }
        public decimal CgCotizmon { get; set; }
        public int CgCompensacion { get; set; }
        public DateTime? CgFechadiario { get; set; }
        public decimal CgNrodiario { get; set; }
        public decimal CgImporteextranjera { get; set; }
        public string CgTipoV { get; set; }
        public string CgTcompV { get; set; }
        public string CgNroserie { get; set; }
    }
}
