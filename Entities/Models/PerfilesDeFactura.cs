﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PerfilesDeFactura
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int? AltaCliente { get; set; }
        public int? FacturaContado { get; set; }
        public int? FacturaCc { get; set; }
        public int? CambiaPrecios { get; set; }
        public int? ValidaNivelDeCredito { get; set; }
        public int? NcctaCte { get; set; }
        public int? NcctaCteDoc { get; set; }
        public string PuntoDeVenta { get; set; }
        public int? CopiasFac { get; set; }
        public int? CopiasRem { get; set; }
        public string Contabilizacion { get; set; }
        public string ImpDeudoraDe { get; set; }
        public string DeudoraPerfil { get; set; }
        public string ImpVentaDe { get; set; }
        public string VentaPerfil { get; set; }
        public string Ccde { get; set; }
        public string Ccperfil { get; set; }
        public int? ValidaStock { get; set; }
        public int? PideComprobanteDeOrigen { get; set; }
        public string ComprobanteDeOrigen { get; set; }
        public string PerfilComOrigen { get; set; }
        public int? ModificaCantidadAlFacturar { get; set; }
        public int? MuestraFecha { get; set; }
        public int? ComFecha { get; set; }
        public int? MuestraCondVta { get; set; }
        public int? AplicaBonificacion { get; set; }
        public int? BonificacionDe { get; set; }
        public int? ComBonificacion { get; set; }
        public decimal? BonificacionManual { get; set; }
        public int? PideObservaciones { get; set; }
        public string Deposito { get; set; }
        public int? ListaDePreciosDe { get; set; }
        public int? ComListaDePrecios { get; set; }
        public int? ListaManual { get; set; }
        public int? PideTransporte { get; set; }
        public int? ComTransporte { get; set; }
        public int? PermiteLeyenda { get; set; }
        public int? Ncsuma { get; set; }
        public int? ComCondVta { get; set; }
        public int? MuestraVendedor { get; set; }
        public int? VendedorDe { get; set; }
        public int? ComVendedor { get; set; }
        public int? VendedorManual { get; set; }
        public int? CampoUser1Pide { get; set; }
        public int? CampoUser1Tipo { get; set; }
        public int? CampoUser1Valida { get; set; }
        public string CampoUser1Titulo { get; set; }
        public int? CampoUser2Pide { get; set; }
        public int? CampoUser2Tipo { get; set; }
        public int? CampoUser2Valida { get; set; }
        public string CampoUser2Titulo { get; set; }
        public int? CampoUser3Pide { get; set; }
        public int? CampoUser3Tipo { get; set; }
        public int? CampoUser3Valida { get; set; }
        public string CampoUser3Titulo { get; set; }
        public int? CampoUser4Pide { get; set; }
        public int? CampoUser4Tipo { get; set; }
        public int? CampoUser4Valida { get; set; }
        public string CampoUser4Titulo { get; set; }
        public int? ComImpDeudora { get; set; }
        public int? ComImpVentas { get; set; }
        public int? ComDeposito { get; set; }
        public string CuerpoDe { get; set; }
        public int? MueveStock { get; set; }
        public int? AgregaArticulosAlFacturar { get; set; }
        public string LeyendaAlpie { get; set; }
        public int? PideLugarDeEntrega { get; set; }
        public int? CopiasFacB { get; set; }
        public int? CantidadDeLineas { get; set; }
        public int? NumeracionManual { get; set; }
        public string Nummansuc { get; set; }
    }
}
