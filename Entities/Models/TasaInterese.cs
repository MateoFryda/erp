﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TasaInterese
    {
        public int TiAnio { get; set; }
        public int TiMes { get; set; }
        public decimal TiTasa { get; set; }
    }
}
