﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class SesVtf001
    {
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public string VaClnro { get; set; }
        public int VaTiva { get; set; }
        public decimal? VaPorex { get; set; }
        public int? VaCpag { get; set; }
        public DateTime VaFfech { get; set; }
        public int? VaDvto { get; set; }
        public int? VaVen { get; set; }
        public int? VaPedido { get; set; }
        public decimal VaTotal { get; set; }
        public decimal? VaDto { get; set; }
        public decimal? VaDto2 { get; set; }
        public decimal? VaPordesc { get; set; }
        public decimal? VaPordes2 { get; set; }
        public decimal? VaIva { get; set; }
        public decimal? VaIvani { get; set; }
        public decimal? VaInterno { get; set; }
        public int? VaMarca { get; set; }
        public string VaObs { get; set; }
        public decimal? VaComi { get; set; }
        public string VaEnt { get; set; }
        public decimal? VaCotiz { get; set; }
        public int? VaAnul { get; set; }
        public DateTime? VaFmanu { get; set; }
        public string VaExpreso { get; set; }
        public string VaComp { get; set; }
        public int? VaRemito { get; set; }
        public DateTime? VaFanul { get; set; }
        public decimal? VaTotex { get; set; }
        public string VaDetalle { get; set; }
        public string VaTrans { get; set; }
        public string VaUser { get; set; }
        public int? VaEtiq { get; set; }
        public string VaDetalle2 { get; set; }
        public string VaUser1alfa { get; set; }
        public string VaUser1Mult { get; set; }
        public DateTime? VaUser1Fecha { get; set; }
        public decimal? VaUser1Moneda { get; set; }
        public float? VaUser1Numerico { get; set; }
        public string VaUser2Alfa { get; set; }
        public string VaUser2Mult { get; set; }
        public DateTime? VaUser2Fecha { get; set; }
        public decimal? VaUser2Moneda { get; set; }
        public float? VaUser2Numerico { get; set; }
        public string VaUser3Alfa { get; set; }
        public string VaUser3Mult { get; set; }
        public DateTime? VaUser3Fecha { get; set; }
        public decimal? VaUser3Moneda { get; set; }
        public float? VaUser3Numerico { get; set; }
        public string VaUser4Alfa { get; set; }
        public string VaUser4Mult { get; set; }
        public DateTime? VaUser4Fecha { get; set; }
        public decimal? VaUser4Moneda { get; set; }
        public float? VaUser4Numerico { get; set; }
        public string VaCodigoDePerfil { get; set; }
        public string VaNombreCampoUsuario { get; set; }
        public int? VaLista { get; set; }
        public string VcTipoComp { get; set; }
        public int? VcSuc { get; set; }
        public int? VcNro { get; set; }
        public string VaPerfil { get; set; }
        public string VcComprobante { get; set; }
        public string VaLentrega { get; set; }
        public int? VaCodLinea { get; set; }
        public int? VaLinea { get; set; }
        public int? VaNro { get; set; }
        public decimal VaIvani27 { get; set; }
        public decimal VaIva27 { get; set; }
        public decimal VaTotal27 { get; set; }
        public DateTime VaFvto { get; set; }
        public byte VaMon { get; set; }
        public decimal VaCotizmon { get; set; }
    }
}
