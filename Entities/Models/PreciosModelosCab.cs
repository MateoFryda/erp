﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosModelosCab
    {
        public int IdModelo { get; set; }
        public string Descripcion { get; set; }
    }
}
