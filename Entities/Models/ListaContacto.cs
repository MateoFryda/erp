﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ListaContacto
    {
        public string Empresa { get; set; }
        public string Estado { get; set; }
        public string Contacto { get; set; }
        public string Mail { get; set; }
        public string MailCae { get; set; }
    }
}
