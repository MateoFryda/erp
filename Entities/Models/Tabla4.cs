﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tabla4
    {
        public int TpRegistro { get; set; }
        public string FechComp { get; set; }
        public string TipoComp { get; set; }
        public string Cfiscal { get; set; }
        public string PtoVta { get; set; }
        public string Compdesde { get; set; }
        public string Comphasta { get; set; }
        public string CantHojas { get; set; }
        public int? TipoDocAfip { get; set; }
        public string CaCuit { get; set; }
        public string CaCnom { get; set; }
        public decimal Total { get; set; }
        public decimal? Exento { get; set; }
        public decimal? Gravado { get; set; }
        public decimal? Implocal { get; set; }
        public int? Imprni { get; set; }
        public int? Campo17 { get; set; }
        public decimal ImpNac { get; set; }
        public decimal Ingbr { get; set; }
        public decimal Impmun { get; set; }
        public decimal? Impint { get; set; }
        public int? Transporte { get; set; }
        public string Tiporesp { get; set; }
        public string Moneda { get; set; }
        public decimal Tipocambio { get; set; }
        public string CodOperacion { get; set; }
        public string CaiNro { get; set; }
        public string CaiVto { get; set; }
        public string Fechaanul { get; set; }
        public int? CantAlicuotas { get; set; }
    }
}
