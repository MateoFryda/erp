﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzXparametrosBk
    {
        public string Item { get; set; }
        public string Valor { get; set; }
        public string Valor2 { get; set; }
        public string Valor3 { get; set; }
        public string Descripcion { get; set; }
        public string Campo { get; set; }
        public string CampoDesc { get; set; }
        public string Tabla { get; set; }
    }
}
