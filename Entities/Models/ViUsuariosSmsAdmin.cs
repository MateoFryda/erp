﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ViUsuariosSmsAdmin
    {
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public short Codigo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int? Legajo { get; set; }
        public int? Sector { get; set; }
        public string Email { get; set; }
        public string Observacion { get; set; }
        public bool EsUserSistema { get; set; }
        public string UsuarioTr { get; set; }
        public DateTime? Fhalta { get; set; }
        public DateTime FhModif { get; set; }
        public DateTime? FhCambioClave { get; set; }
        public bool NoExigeCambioClave { get; set; }
        public DateTime? FhBaja { get; set; }
        public string ClaveAnterior { get; set; }
        public string Pais { get; set; }
        public string EmpresaF { get; set; }
        public int? CodAutorizacion { get; set; }
        public bool? Bloqueado { get; set; }
        public string Smtpuser { get; set; }
        public string Smtppassword { get; set; }
        public string Popuser { get; set; }
        public string Poppassword { get; set; }
        public DateTime? FechaNotificacion { get; set; }
        public int? CodAutorizacion2 { get; set; }
    }
}
