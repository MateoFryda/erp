﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalInterese
    {
        public decimal CdSaldo { get; set; }
        public string CdCnro { get; set; }
        public DateTime? CdFvto { get; set; }
        public string CdFcomp { get; set; }
        public short CdSuc { get; set; }
        public int CdFnro { get; set; }
        public DateTime? Vto1 { get; set; }
        public DateTime? Vto2 { get; set; }
        public DateTime? FechaVto { get; set; }
        public int? Dias { get; set; }
        public decimal? TasaAplic { get; set; }
        public decimal? Interes { get; set; }
        public int? CdMon { get; set; }
        public int Id { get; set; }
    }
}
