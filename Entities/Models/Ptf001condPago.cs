﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001condPago
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool Escontado { get; set; }
    }
}
