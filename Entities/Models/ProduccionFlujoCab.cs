﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProduccionFlujoCab
    {
        public int NroFlujo { get; set; }
        public string Descripcion { get; set; }
    }
}
