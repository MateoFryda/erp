﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class EstadosOrdenesdeCompra
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
