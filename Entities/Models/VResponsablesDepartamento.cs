﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VResponsablesDepartamento
    {
        public string Idresponsable { get; set; }
        public string Descripción { get; set; }
    }
}
