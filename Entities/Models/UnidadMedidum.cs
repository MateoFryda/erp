﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UnidadMedidum
    {
        public UnidadMedidum()
        {
            Despieces = new HashSet<Despiece>();
            OrdenesTrabajoDetTemps = new HashSet<OrdenesTrabajoDetTemp>();
            OrdenesTrabajoDets = new HashSet<OrdenesTrabajoDet>();
            Stf001s = new HashSet<Stf001>();
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string CodigoAfip { get; set; }

        public virtual ICollection<Despiece> Despieces { get; set; }
        public virtual ICollection<OrdenesTrabajoDetTemp> OrdenesTrabajoDetTemps { get; set; }
        public virtual ICollection<OrdenesTrabajoDet> OrdenesTrabajoDets { get; set; }
        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
