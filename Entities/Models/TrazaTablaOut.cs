﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaTablaOut
    {
        public long? IdTransaccion { get; set; }
        public long? IdTransacWs { get; set; }
        public DateTime? FEvento { get; set; }
        public string GlnDestino { get; set; }
        public string CuitDestino { get; set; }
        public string GlnOrigen { get; set; }
        public string NRemito { get; set; }
        public string CuitOrigen { get; set; }
        public string NFactura { get; set; }
        public DateTime? Vencimiento { get; set; }
        public string Gtin { get; set; }
        public string Lote { get; set; }
        public string NumeroSerial { get; set; }
        public int? IdEvento { get; set; }
        public DateTime? FTransaccion { get; set; }
        public long? IdTransacWsAnul { get; set; }
        public DateTime? FAnulacion { get; set; }
        public string HEvento { get; set; }
        public int? IdObraSocial { get; set; }
        public string NroAfiliadoOs { get; set; }
        public int? CantFraccion { get; set; }
        public string DetalleAnulacion { get; set; }
        public int? MAnulacion { get; set; }
        public string Apellido { get; set; }
        public string Nombres { get; set; }
        public int? TipoDocumento { get; set; }
        public string NDocumento { get; set; }
        public string Sexo { get; set; }
        public string Numero { get; set; }
        public string Piso { get; set; }
        public string Dpto { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Direccion { get; set; }
        public string NPostal { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public int? IdEstadoTransaccion { get; set; }
        public int? IdDetalleEvento { get; set; }
        public string DetalleEvento { get; set; }
        public byte? IdEstadoTrazatools { get; set; }
        public string CodigoError { get; set; }
        public string DescripcionError { get; set; }
        public int? IdTransacWsOk { get; set; }
        public int? IdTransacOk { get; set; }
        public string UsuarioModif { get; set; }
        public string DescEstado { get; set; }
        public string DescMotivoDevolucion { get; set; }
        public string DescProducto { get; set; }
        public int? IdMotivoDevolucion { get; set; }
        public string Nombre { get; set; }
        public string OtroMotivoDevolucion { get; set; }
        public string PacienteCalle { get; set; }
        public string PacienteDescObraSocial { get; set; }
        public string PacienteDescTipoDocumento { get; set; }
        public string TransacProvincia { get; set; }
        public string MedicoCuit { get; set; }
        public string MedicoNombres { get; set; }
        public string CodigoDiagnostico { get; set; }
        public string DescDiagnostico { get; set; }
        public string GlnInformador { get; set; }
        public int Id { get; set; }
    }
}
