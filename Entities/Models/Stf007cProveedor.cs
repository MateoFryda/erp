﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf007cProveedor
    {
        public int Id { get; set; }
        public string SaArt { get; set; }
        public string NroSerie { get; set; }
        public string NroSerieProv { get; set; }
        public string Observaciones { get; set; }
    }
}
