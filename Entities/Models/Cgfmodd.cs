﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgfmodd
    {
        public string SCodMod { get; set; }
        public string ScodNivel { get; set; }
        public string CgIc { get; set; }
        public string Flag { get; set; }
    }
}
