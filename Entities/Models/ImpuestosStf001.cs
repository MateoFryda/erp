﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosStf001
    {
        public ImpuestosStf001()
        {
            ImpuestosDetalles = new HashSet<ImpuestosDetalle>();
            Stf001s = new HashSet<Stf001>();
        }

        public string IdImpuestoStf001 { get; set; }
        public string Descripcion { get; set; }
        public byte ParaArticulos { get; set; }

        public virtual ICollection<ImpuestosDetalle> ImpuestosDetalles { get; set; }
        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
