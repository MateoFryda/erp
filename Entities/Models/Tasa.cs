﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tasa
    {
        public DateTime Fecha { get; set; }
        public decimal TasaInteres { get; set; }
        public decimal TasaDescuento { get; set; }
        public string Usuario { get; set; }
        public bool Baja { get; set; }
        public int Id { get; set; }
    }
}
