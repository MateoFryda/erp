﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf003
    {
        public short CcPais { get; set; }
        public int CcCpos { get; set; }
        public string CcLoc { get; set; }
        public string CcProv { get; set; }
        public string CcCprov { get; set; }
        public int IdLocalidad { get; set; }
    }
}
