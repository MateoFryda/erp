﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaIngreso
    {
        public string SaArt { get; set; }
        public int? Cantidad { get; set; }
        public string Despacho { get; set; }
        public DateTime? FecDespacho { get; set; }
        public byte? Aduana { get; set; }
    }
}
