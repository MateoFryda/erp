﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempDatanet
    {
        public string Concepto { get; set; }
        public DateTime? Fecha { get; set; }
        public string NumComprobante { get; set; }
        public string Sucursal { get; set; }
        public decimal? Importe { get; set; }
        public int Identidad { get; set; }
    }
}
