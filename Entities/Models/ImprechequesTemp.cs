﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImprechequesTemp
    {
        public string DiamesEmi { get; set; }
        public string DiaEmi { get; set; }
        public string MesEmi { get; set; }
        public int? AnioEmi { get; set; }
        public string Paguesea { get; set; }
        public decimal? Importe { get; set; }
        public string SImporte { get; set; }
        public string ImporteLetra { get; set; }
        public int? Op { get; set; }
        public string SOp { get; set; }
        public DateTime? Fechaemision { get; set; }
        public string Diavto { get; set; }
        public string Mesvto { get; set; }
        public string Aniovto { get; set; }
        public string Diamesvto { get; set; }
        public string Observaciones { get; set; }
    }
}
