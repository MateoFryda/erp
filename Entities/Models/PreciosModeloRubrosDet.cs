﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosModeloRubrosDet
    {
        public int IDmodelo { get; set; }
        public string Codigo { get; set; }
    }
}
