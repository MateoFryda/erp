﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VPedidosPendiente
    {
        public int Número { get; set; }
        public DateTime Fecha { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Zona { get; set; }
        public string Descripcion { get; set; }
        public int? Cp { get; set; }
        public string Vende { get; set; }
        public string Nombrevendedor { get; set; }
    }
}
