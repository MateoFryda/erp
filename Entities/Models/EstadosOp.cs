﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class EstadosOp
    {
        public byte IdEstado { get; set; }
        public string Descripcion { get; set; }
        public byte Tipoestado { get; set; }
    }
}
