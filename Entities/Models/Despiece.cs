﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Despiece
    {
        public string SaProducto { get; set; }
        public string SaItem { get; set; }
        public decimal SaCant { get; set; }
        public decimal SaCosto { get; set; }
        public string Umed { get; set; }
        public short Item { get; set; }

        public virtual Stf001 SaItemNavigation { get; set; }
        public virtual Stf001 SaProductoNavigation { get; set; }
        public virtual UnidadMedidum UmedNavigation { get; set; }
    }
}
