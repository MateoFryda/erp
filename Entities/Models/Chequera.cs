﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Chequera
    {
        public int IdChequera { get; set; }
        public int CbCod { get; set; }
        public decimal Desdenumero { get; set; }
        public decimal HastaNumero { get; set; }
        public decimal? NumeracionActual { get; set; }
    }
}
