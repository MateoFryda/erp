﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StEstadosArticulo
    {
        public StEstadosArticulo()
        {
            StRecepcions = new HashSet<StRecepcion>();
        }

        public string SCodEstadoArticulo { get; set; }
        public string SDescEstadoArticulo { get; set; }
        public int? NHab { get; set; }

        public virtual ICollection<StRecepcion> StRecepcions { get; set; }
    }
}
