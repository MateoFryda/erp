﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipeCodimp
    {
        public int? CodImpAfip { get; set; }
        public string DesAfipImp { get; set; }
        public int? Iva { get; set; }
    }
}
