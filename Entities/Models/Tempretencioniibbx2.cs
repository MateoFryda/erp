﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempretencioniibbx2
    {
        public string CodJurisdiccion { get; set; }
        public string Cuit { get; set; }
        public string FechaRet { get; set; }
        public string NroComprobante { get; set; }
        public string Comprobante { get; set; }
        public string LetraComp { get; set; }
        public string NroCompr { get; set; }
        public string Importe { get; set; }
    }
}
