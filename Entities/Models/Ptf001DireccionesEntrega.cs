﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001DireccionesEntrega
    {
        public decimal Id { get; set; }
        public string PaCnro { get; set; }
        public string PaCnom { get; set; }
        public short? PaPais { get; set; }
        public string PaProvincia { get; set; }
        public string PaLocalidad { get; set; }
        public int? PaCp { get; set; }
        public string PaDir { get; set; }
        public string PaTel { get; set; }
        public short PaHorario { get; set; }
        public short? PaTransporte { get; set; }
        public string PaDescEntrega { get; set; }
        public string PaGln { get; set; }

        public virtual Transporte PaTransporteNavigation { get; set; }
    }
}
