﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposRegistroSalidum
    {
        public int Codigo { get; set; }
        public string DescRegistro { get; set; }
        public string SpInicio { get; set; }
        public string SpCheck { get; set; }
        public string Tablatemp1 { get; set; }
        public string Ordertemp1 { get; set; }
        public string TablatempSalida { get; set; }
        public string OrdertempSalida { get; set; }
        public int? LongRegistro { get; set; }
        public string SpUnico { get; set; }
        public string ArchivoSalida { get; set; }
    }
}
