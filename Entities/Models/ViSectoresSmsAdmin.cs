﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ViSectoresSmsAdmin
    {
        public int SCodigo { get; set; }
        public string SDescripcion { get; set; }
        public string SCentroCosto { get; set; }
        public int? SParteCircuito { get; set; }
        public bool? EsSectorCompras { get; set; }
        public bool? UsuariosModReqDeSuSector { get; set; }
        public bool? VerReqTodosLosSectores { get; set; }
        public bool? EsSectorAdministracion { get; set; }
    }
}
