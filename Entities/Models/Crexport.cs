﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Crexport
    {
        public string Ti1 { get; set; }
        public int? Ti2 { get; set; }
        public string Ti3 { get; set; }
        public int? Ti4 { get; set; }
        public string Ph1 { get; set; }
        public string Ph2 { get; set; }
        public int? Ph3 { get; set; }
        public string Ph4 { get; set; }
        public int? Ph5 { get; set; }
        public string Ph6 { get; set; }
        public DateTime? Ph7 { get; set; }
        public DateTime? Ph8 { get; set; }
        public string Ph9 { get; set; }
        public string Ph10 { get; set; }
        public string Ph11 { get; set; }
        public string Ph12 { get; set; }
        public string Ph13 { get; set; }
        public string Ph14 { get; set; }
        public string Gh11 { get; set; }
        public string Gh21 { get; set; }
        public string De1 { get; set; }
        public string De2 { get; set; }
        public string De3 { get; set; }
        public double? De4 { get; set; }
        public double? De5 { get; set; }
    }
}
