﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempImpresionRecibo
    {
        public string Dealer { get; set; }
        public string CaCNro { get; set; }
        public string Comp { get; set; }
        public int Suc { get; set; }
        public int Nro { get; set; }
    }
}
