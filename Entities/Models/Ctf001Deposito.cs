﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001Deposito
    {
        public string CaCnro { get; set; }
        public string Deposito { get; set; }
    }
}
