﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RemitosSeries
    {
        public string V7Tipo { get; set; }
        public short V7Suc { get; set; }
        public int V7Nro { get; set; }
        public int V8Linea { get; set; }
        public string SaArt { get; set; }
        public string Series { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
