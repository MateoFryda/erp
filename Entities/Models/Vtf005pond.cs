﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf005pond
    {
        public string PTipo { get; set; }
        public int PSuc { get; set; }
        public int PRecnro { get; set; }
        public decimal PNumf { get; set; }
        public decimal? PNumfp { get; set; }
        public string PFormaPago { get; set; }
        public string PVtoProm { get; set; }
        public string PPago { get; set; }
        public string PDif { get; set; }
        public string PDifff { get; set; }
        public string PDifvt { get; set; }
        public string PPgoctdo { get; set; }
        public string PVen { get; set; }
        public decimal? PNumfx { get; set; }
        public decimal? PNumfpx { get; set; }
        public decimal? PNumfvto { get; set; }
        public decimal? PNumfvtox { get; set; }
        public decimal? PTotimp { get; set; }
        public decimal? PTotimpf { get; set; }
        public int? PActividad { get; set; }
        public decimal? PCodpostal { get; set; }
        public string PFbase { get; set; }
        public string PFbasep { get; set; }
        public decimal? PImportetot { get; set; }
        public bool PAnul { get; set; }
    }
}
