﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VOrdenDeTrabajoV2Cx
    {
        public int NroOt { get; set; }
        public string NroPedido { get; set; }
        public string Artículo { get; set; }
        public string Descripción { get; set; }
        public decimal? Cantidad { get; set; }
        public string Medico { get; set; }
        public string Instrum { get; set; }
        public string NombrePaciente { get; set; }
        public string FechaCirugia { get; set; }
        public string Estado { get; set; }
        public short EstadoCodigo { get; set; }
    }
}
