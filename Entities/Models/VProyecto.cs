﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VProyecto
    {
        public int NroProyecto { get; set; }
        public string Descripción { get; set; }
        public DateTime FechaAlta { get; set; }
        public string NroPedido { get; set; }
    }
}
