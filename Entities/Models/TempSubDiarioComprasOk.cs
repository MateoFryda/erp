﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempSubDiarioComprasOk
    {
        public DateTime? Fecha { get; set; }
        public DateTime? FechaReal { get; set; }
        public string TipoInt { get; set; }
        public int NroInt { get; set; }
        public string Comprobante { get; set; }
        public string Proveedor { get; set; }
        public string CondImpositiva { get; set; }
        public string Tipo { get; set; }
        public string Cuit { get; set; }
        public int? ClasifCodigo { get; set; }
        public string Clasificacion { get; set; }
        public string PcOcproveedor { get; set; }
        public decimal? Gravado { get; set; }
        public decimal? NoGravado { get; set; }
        public decimal? IVA21 { get; set; }
        public decimal? IVA27 { get; set; }
        public decimal? IVAOtrasAlic { get; set; }
        public decimal? PercIva { get; set; }
        public decimal? PercIibbCaba { get; set; }
        public decimal? PercIibbBsAs { get; set; }
        public decimal? RetGan { get; set; }
        public decimal? Total { get; set; }
    }
}
