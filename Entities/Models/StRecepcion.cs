﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StRecepcion
    {
        public StRecepcion()
        {
            StPresupuestoCs = new HashSet<StPresupuestoC>();
        }

        public string SNumeroRecepcion { get; set; }
        public string SClave { get; set; }
        public string DFecha { get; set; }
        public string SHora { get; set; }
        public string CaCnro { get; set; }
        public string SUsuario { get; set; }
        public string SNombreTraeArticulo { get; set; }
        public string SApellidoTraeArticulo { get; set; }
        public string STelefonoTraeArticulo { get; set; }
        public string SDireccionTraeArticulo { get; set; }
        public string SCorreoTraeArticulo { get; set; }
        public string SaArt { get; set; }
        public string SMarca { get; set; }
        public string SModelo { get; set; }
        public string SDescripcion { get; set; }
        public string NroSerie { get; set; }
        public string SAccesorios { get; set; }
        public string SNota { get; set; }
        public string SFalla { get; set; }
        public string SCodEstadoArticulo { get; set; }
        public string SCodMedioIngreso { get; set; }
        public DateTime? DFechaRequeridoParaEntrega { get; set; }
        public string SHoraRequeridoParaEntrega { get; set; }
        public string SCodTipoDeServicio { get; set; }
        public string SCodFormaDeEntrega { get; set; }
        public string SDireccionEntrega { get; set; }
        public DateTime? DFechaIngreso { get; set; }
        public string SNumeroContrato { get; set; }
        public DateTime? DFechaIngresoVirtual { get; set; }
        public bool EsTaxiAereo { get; set; }

        public virtual StEstadosArticulo SCodEstadoArticuloNavigation { get; set; }
        public virtual StFormaDeEntrega SCodFormaDeEntregaNavigation { get; set; }
        public virtual StMedioIngreso SCodMedioIngresoNavigation { get; set; }
        public virtual StTipoDeServicio SCodTipoDeServicioNavigation { get; set; }
        public virtual ICollection<StPresupuestoC> StPresupuestoCs { get; set; }
    }
}
