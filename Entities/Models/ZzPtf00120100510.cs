﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzPtf00120100510
    {
        public string PaCnro { get; set; }
        public string PaCnom { get; set; }
        public string PaDom { get; set; }
        public int? PaCp { get; set; }
        public string PaResp { get; set; }
        public string PaTele { get; set; }
        public string PaFax { get; set; }
        public byte? PaCpag { get; set; }
        public string PaObs { get; set; }
        public byte? PaCact { get; set; }
        public byte? PaCcpto { get; set; }
        public byte? PaIva { get; set; }
        public string PaCuit { get; set; }
        public byte? PaGan { get; set; }
        public double? PaCoefadi { get; set; }
        public double? PaOrigen { get; set; }
        public string PaImpcom { get; set; }
        public byte? PaTipdoc { get; set; }
        public byte? PaRetiv { get; set; }
        public string PaImppro { get; set; }
        public string PaBenef { get; set; }
        public string PaUser { get; set; }
        public string PaEmail { get; set; }
        public string PaProvincia { get; set; }
        public string PaImpprodolar { get; set; }
        public string PaImpproom { get; set; }
        public byte PaRetingbr { get; set; }
        public string PaCbu { get; set; }
        public decimal PaPorex { get; set; }
        public string PaNroIngbr { get; set; }
        public short? PaPais { get; set; }
        public string PaIdAgrupImp { get; set; }
        public string PaTitularCuenta { get; set; }
        public string PaFormapagodef { get; set; }
        public string PaNumeroCuentaBan { get; set; }
        public string PaTipoCuentaBan { get; set; }
        public string PaCodigoBanco { get; set; }
        public byte PaRetsuss { get; set; }
        public byte? PaActec { get; set; }
        public int? PaTae { get; set; }
        public string PaIngbtos { get; set; }
        public bool? PaRetirapago { get; set; }
        public string PaEmailPagos { get; set; }
        public string PaCcosto { get; set; }
        public string PaSubcosto { get; set; }
        public string PaSucbnl { get; set; }
        public bool PaChequecruzado { get; set; }
        public DateTime? PaFulttransf { get; set; }
        public DateTime? PaFultmodif { get; set; }
        public string PaEmailOc { get; set; }
        public string PaNombreFantasia { get; set; }
        public int PaTipopago { get; set; }
    }
}
