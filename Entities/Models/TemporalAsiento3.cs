﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalAsiento3
    {
        public int? Codtransac { get; set; }
        public string CiNro { get; set; }
        public string CiDesc { get; set; }
        public string Ccosto { get; set; }
        public string Subcosto { get; set; }
        public decimal? Importe { get; set; }
        public string Cuentatipo { get; set; }
    }
}
