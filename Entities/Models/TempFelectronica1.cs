﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempFelectronica1
    {
        public int? TpRegistro { get; set; }
        public string FechComp { get; set; }
        public string TipoComp { get; set; }
        public string Cfiscal { get; set; }
        public string PtoVta { get; set; }
        public string Compdesde { get; set; }
        public string Comphasta { get; set; }
        public string CantHojas { get; set; }
        public int? TipodocAfip { get; set; }
        public string CaCuit { get; set; }
        public string CaCnom { get; set; }
        public decimal? Total { get; set; }
        public decimal? Campo12 { get; set; }
        public decimal? Gravado { get; set; }
        public decimal? Implocal { get; set; }
        public int? ImpRni { get; set; }
        public decimal? Exento { get; set; }
        public decimal? ImpNac { get; set; }
        public decimal? IngBr { get; set; }
        public decimal? ImpMun { get; set; }
        public decimal? ImpInt { get; set; }
        public string FechaDesdeServ { get; set; }
        public string FechaHastaServ { get; set; }
        public string FechaVto { get; set; }
    }
}
