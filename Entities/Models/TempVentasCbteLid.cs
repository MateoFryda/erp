﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempVentasCbteLid
    {
        public string C1 { get; set; }
        public string C2 { get; set; }
        public int? C3 { get; set; }
        public int? C4 { get; set; }
        public int? C5 { get; set; }
        public string C6 { get; set; }
        public string C7 { get; set; }
        public string C8 { get; set; }
        public string C9 { get; set; }
        public int? C10 { get; set; }
        public string C11 { get; set; }
        public string C12 { get; set; }
        public decimal? C13 { get; set; }
        public decimal? C14 { get; set; }
        public decimal? C15 { get; set; }
        public decimal? C16 { get; set; }
        public decimal? C17 { get; set; }
    }
}
