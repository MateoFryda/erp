﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DtosClientesArticulo
    {
        public string CaCnro { get; set; }
        public string AtribCliente { get; set; }
        public string AtribArticulo { get; set; }
        public string SaArt { get; set; }
        public int IdDto { get; set; }

        public virtual Ctf001 CaCnroNavigation { get; set; }
        public virtual DtoVentasCab IdDtoNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
