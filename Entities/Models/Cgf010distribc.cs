﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010distribc
    {
        public string CcCoddist { get; set; }
        public string CcCod { get; set; }
        public decimal CcPorcentaje { get; set; }

        public virtual Cgf010 CcCodNavigation { get; set; }
        public virtual Cgf010distribh CcCoddistNavigation { get; set; }
    }
}
