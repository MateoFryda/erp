﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001actividad
    {
        public int CbCod { get; set; }
        public string CbDesc { get; set; }
        public bool CbEsPaciente { get; set; }
        public string CbCodNormaAgip { get; set; }
    }
}
