﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class NovedadesPeriodo
    {
        public int IdNovedad { get; set; }
        public string Periodo { get; set; }
        public decimal Monto { get; set; }
        public byte Moneda { get; set; }
        public int Cant { get; set; }
        public int? IdProforma { get; set; }
        public short? NroCuota { get; set; }

        public virtual NovedadesMaestro IdNovedadNavigation { get; set; }
        public virtual ProformasCab IdProformaNavigation { get; set; }
    }
}
