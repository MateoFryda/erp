﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempseriesDespacho
    {
        public string Comp { get; set; }
        public int? Suc { get; set; }
        public int? Nro { get; set; }
        public string Art { get; set; }
        public string Series { get; set; }
        public string Despachos { get; set; }
    }
}
