﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PresupuestosCab
    {
        public int IdPresupuesto { get; set; }
        public string VcTipo { get; set; }
        public byte? VcSuc { get; set; }
        public int? VcNro { get; set; }
        public string VcClnro { get; set; }
        public DateTime VcFfech { get; set; }
        public string VcVen { get; set; }
        public byte? VcCpag { get; set; }
        public byte? VcTransporte { get; set; }
        public int VcLista { get; set; }
        public string VcObs { get; set; }
        public string VcUsuario { get; set; }
        public string VcLentrega { get; set; }
        public bool VcAnul { get; set; }
        public DateTime VcFcarga { get; set; }
        public int? VcPedObservado { get; set; }
        public string VcPedidoCliente { get; set; }
        public decimal VcCotiz { get; set; }
        public decimal VcCotizmon { get; set; }
        public byte VcMon { get; set; }
        public string VcHcarga { get; set; }
        public DateTime? VcFultModif { get; set; }
        public string VcHultModif { get; set; }
        public bool VcDtosmanuales { get; set; }
        public DateTime? VcFvencimiento { get; set; }
        public string VcPentrega { get; set; }
        public string VcPedidopor { get; set; }
        public string VcExpediente { get; set; }
        public short VcEstado { get; set; }
        public string VcMotivo { get; set; }
        public string VaFcomp { get; set; }
        public int? VaSuc { get; set; }
        public int? VaFnro { get; set; }
        public string VcAgrupImpuesto { get; set; }

        public virtual VentasCab Va { get; set; }
        public virtual Ctf001 VcClnroNavigation { get; set; }
    }
}
