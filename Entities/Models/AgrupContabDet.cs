﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AgrupContabDet
    {
        public short Agrupacion { get; set; }
        public byte TipoImputacionArt { get; set; }
        public string CiNro { get; set; }
        public string Ccosto { get; set; }
        public string Subcosto { get; set; }

        public virtual AgrupContabCab AgrupacionNavigation { get; set; }
        public virtual Ctf009 CiNroNavigation { get; set; }
        public virtual TipoImputacionArt TipoImputacionArtNavigation { get; set; }
    }
}
