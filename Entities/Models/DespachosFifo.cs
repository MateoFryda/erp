﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DespachosFifo
    {
        public int SgId { get; set; }
        public string SaArt { get; set; }
        public decimal SgCant { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public int SgOrigen { get; set; }
        public string IhDespacho { get; set; }
        public int? IhAduana { get; set; }
        public DateTime? IhFvtoDespacho { get; set; }
        public string IhOrigenDespacho { get; set; }

        public virtual Stf007h Sg { get; set; }
        public virtual Stf001Lote Stf001Lote { get; set; }
    }
}
