﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf004TipodeDeudum
    {
        public byte Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Imputacion { get; set; }
    }
}
