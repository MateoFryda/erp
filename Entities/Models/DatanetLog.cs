﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DatanetLog
    {
        public string Banco { get; set; }
        public DateTime FechaExtracto { get; set; }
        public DateTime? FechaImportacion { get; set; }
        public string Usuario { get; set; }
        public string Archivo { get; set; }
    }
}
