﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AgrupContabCab
    {
        public AgrupContabCab()
        {
            AgrupContabDets = new HashSet<AgrupContabDet>();
            Stf001s = new HashSet<Stf001>();
        }

        public short Agrupacion { get; set; }
        public string Descripcion { get; set; }
        public bool? EsBienUso { get; set; }

        public virtual ICollection<AgrupContabDet> AgrupContabDets { get; set; }
        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
