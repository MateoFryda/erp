﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class NovedadesConcepto
    {
        public NovedadesConcepto()
        {
            NovedadesMaestros = new HashSet<NovedadesMaestro>();
        }

        public int IdConcepto { get; set; }
        public string Descripcion { get; set; }
        public bool? MuestraPeriodo { get; set; }
        public byte Periodicidad { get; set; }
        public string SaArt { get; set; }
        public int Tipo { get; set; }
        public bool MesAdelantado { get; set; }
        public bool ProporcionaCargo { get; set; }
        public string DescCorta { get; set; }

        public virtual Periodicidad PeriodicidadNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
        public virtual ConceptosTipo TipoNavigation { get; set; }
        public virtual ICollection<NovedadesMaestro> NovedadesMaestros { get; set; }
    }
}
