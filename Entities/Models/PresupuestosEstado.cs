﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PresupuestosEstado
    {
        public short Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
