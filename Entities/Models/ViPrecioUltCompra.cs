﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ViPrecioUltCompra
    {
        public string SaArt { get; set; }
        public string NroLote { get; set; }
        public string NroSerie { get; set; }
        public decimal? PcioUlt { get; set; }
        public decimal? PcioExtUlt { get; set; }
    }
}
