﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Gaf002
    {
        public double? GbMes { get; set; }
        public double? GbDes1 { get; set; }
        public double? GbImpo1 { get; set; }
        public double? GbPtje1 { get; set; }
        public double? GbDes2 { get; set; }
        public double? GbImpo2 { get; set; }
        public double? GbPtje2 { get; set; }
        public double? GbDes3 { get; set; }
        public double? GbImpo3 { get; set; }
        public double? GbPtje3 { get; set; }
        public double? GbDes4 { get; set; }
        public double? GbImpo4 { get; set; }
        public double? GbPtje4 { get; set; }
        public double? GbDes5 { get; set; }
        public double? GbImpo5 { get; set; }
        public double? GbPtje5 { get; set; }
        public double? GbDes6 { get; set; }
        public double? GbImpo6 { get; set; }
        public double? GbPtje6 { get; set; }
        public double? GbDes7 { get; set; }
        public double? GbImpo7 { get; set; }
        public double? GbPtje7 { get; set; }
        public double? GbDes8 { get; set; }
        public double? GbImpo8 { get; set; }
        public double? GbPtje8 { get; set; }
        public double? GbDes9 { get; set; }
        public double? GbImpo9 { get; set; }
        public double? GbPtje9 { get; set; }
    }
}
