﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzArticulos20210202
    {
        public string Numero { get; set; }
        public string CodArticulo { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string Serie { get; set; }
        public string CodFabricante { get; set; }
        public double? LlevaStock { get; set; }
        public string StockMin { get; set; }
        public string StockMax { get; set; }
        public string PuntoPedido { get; set; }
        public string CódigoRubro { get; set; }
        public string Deposito { get; set; }
        public string CodAgrupContable { get; set; }
        public string LlevaLote { get; set; }
        public string CódigoFamilia { get; set; }
        public string FechaInhabilitación { get; set; }
        public string MonedaPrecioVenta { get; set; }
        public double? MonedaPrecioOrigen { get; set; }
        public string CodImpuesto { get; set; }
        public string AdmiteDecimales { get; set; }
        public string AdmiteDevolución { get; set; }
        public string CodClasificacion { get; set; }
        public string CodDescuentos { get; set; }
        public string NoAptoParaLaVenta { get; set; }
        public string TipoArtículoFe { get; set; }
        public string Peso { get; set; }
        public string Volumen { get; set; }
        public string CodUnidadMedida { get; set; }
        public string DisponibleWeb { get; set; }
        public double? LlevaSerie { get; set; }
    }
}
