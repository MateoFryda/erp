﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Lineascheque
    {
        public int Banco { get; set; }
        public double? Largocheque { get; set; }
        public double? Largopagina { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public string Item { get; set; }
        public byte Linea { get; set; }
        public string Printer { get; set; }
        public string Comentario { get; set; }
        public string Reportes { get; set; }
    }
}
