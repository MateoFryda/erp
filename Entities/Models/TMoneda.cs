﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TMoneda
    {
        public int CMoneda { get; set; }
        public string Descripcion { get; set; }
        public string Simbolo { get; set; }
        public string Codigo { get; set; }
    }
}
