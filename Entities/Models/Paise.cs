﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Paise
    {
        public short PaCodigo { get; set; }
        public string PaDescripcion { get; set; }
        public string PaCodafipj { get; set; }
        public string PaCodafipf { get; set; }
        public string CodigoAfip { get; set; }
    }
}
