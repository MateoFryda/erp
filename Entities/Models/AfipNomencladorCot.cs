﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipNomencladorCot
    {
        public string Codigo { get; set; }
        public string Unidad { get; set; }
        public string Descripcion { get; set; }
    }
}
