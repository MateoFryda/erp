﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TipoRequerimiento
    {
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public bool? EsAnticipo { get; set; }
        public string Subcentro { get; set; }
    }
}
