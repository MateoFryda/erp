﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Bancoscodigo
    {
        public string Banco { get; set; }
        public string Nombre { get; set; }
    }
}
