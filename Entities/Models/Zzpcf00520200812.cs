﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzpcf00520200812
    {
        public string CeFcomp { get; set; }
        public short CeSuc { get; set; }
        public int CeFnro { get; set; }
        public string CeCnro { get; set; }
        public string CeFcprov { get; set; }
        public DateTime CeFfech { get; set; }
        public decimal CeTotal { get; set; }
        public DateTime? CeFvto { get; set; }
        public DateTime? CeFcanc { get; set; }
        public string CeCcanc { get; set; }
        public int? CeNcanc { get; set; }
        public string CeComp { get; set; }
        public byte CeMon { get; set; }
        public decimal CeCotizmon { get; set; }
        public bool CeDifcam { get; set; }
        public decimal CeCotiz { get; set; }
        public string CeCta { get; set; }
        public string CodProvedor { get; set; }
        public string ApellidoYNombre { get; set; }
        public string Mail { get; set; }
        public string TipoDoc { get; set; }
        public double? NroDoc { get; set; }
        public string SocTrabaja { get; set; }
        public double? Legajo { get; set; }
        public string Sa { get; set; }
        public string Sh { get; set; }
        public string Srl { get; set; }
    }
}
