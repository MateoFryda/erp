﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Banco
    {
        public int BaBanco { get; set; }
        public string BaCustodia { get; set; }
        public string BaGarantia { get; set; }
        public string BaCedidos { get; set; }
        public string BaBancoreal { get; set; }
        public string BaClavebanco { get; set; }
        public string BaNumcuenta { get; set; }
        public bool? BaTiponumeracion { get; set; }
        public string BaCuit { get; set; }
        public string BaClearing { get; set; }
        public byte? BaTipo { get; set; }
        public string BaDescripcion { get; set; }
    }
}
