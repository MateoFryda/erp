﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XMotivosNcdevol
    {
        public byte IdMotivo { get; set; }
        public string Descrip { get; set; }
    }
}
