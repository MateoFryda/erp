﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Codigosfact
    {
        public double? SaArt { get; set; }
        public string SaDesc { get; set; }
    }
}
