﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempSaldosEnviado
    {
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string TipoDeuda { get; set; }
        public double? Saldo { get; set; }
        public string Correo { get; set; }
        public string Enviado { get; set; }
    }
}
