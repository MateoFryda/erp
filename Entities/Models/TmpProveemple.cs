﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TmpProveemple
    {
        public string CodProvedor { get; set; }
        public string ApellidoYNombre { get; set; }
        public string Mail { get; set; }
        public string TipoDoc { get; set; }
        public double? NroDoc { get; set; }
        public string SocTrabaja { get; set; }
        public double? Legajo { get; set; }
        public string Sa { get; set; }
        public string Sh { get; set; }
        public string Srl { get; set; }
    }
}
