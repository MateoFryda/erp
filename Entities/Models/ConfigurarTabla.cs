﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ConfigurarTabla
    {
        public string Tabla { get; set; }
        public string Campo { get; set; }
        public string Descripcion { get; set; }
        public string Leyenda { get; set; }
        public string Tipo { get; set; }
        public int Longitud { get; set; }
        public bool? Invisible { get; set; }
        public bool? Permitenulo { get; set; }
    }
}
