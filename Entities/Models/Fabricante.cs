﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Fabricante
    {
        public Fabricante()
        {
            Stf001s = new HashSet<Stf001>();
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
