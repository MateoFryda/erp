﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UpdDinDtosClientesArticulo
    {
        public DateTime FechaUpdate { get; set; }
        public string CaCnro { get; set; }
        public string AtribCliente { get; set; }
        public string AtribArticulo { get; set; }
        public string SaArt { get; set; }
        public int IdDto { get; set; }
    }
}
