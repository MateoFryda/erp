﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Lote
    {
        public Stf001Lote()
        {
            Stf007cs = new HashSet<Stf007c>();
        }

        public string SaArt { get; set; }
        public string NroLote { get; set; }
        public DateTime Fvto { get; set; }
        public DateTime? Fing { get; set; }
        public string Tipolote { get; set; }
        public DateTime? FechaFabricacion { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
        public virtual ICollection<Stf007c> Stf007cs { get; set; }
    }
}
