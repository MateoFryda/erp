﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Prioridade
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
