﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PuntosDeVentaUbicacion
    {
        public string UbicacionFisica { get; set; }
        public string TipoComprobante { get; set; }
        public short PuntoDeVenta { get; set; }
        public string Deposito { get; set; }

        public virtual DePosito DepositoNavigation { get; set; }
        public virtual PuntosDeVentum PuntosDeVentum { get; set; }
    }
}
