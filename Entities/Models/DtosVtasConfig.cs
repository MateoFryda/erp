﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DtosVtasConfig
    {
        public bool Cliente { get; set; }
        public bool ClienteAtribArt { get; set; }
        public bool AtribCliente { get; set; }
        public bool AtribClienteAtribArt { get; set; }
    }
}
