﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CodigosQr
    {
        public int Id { get; set; }
        public byte[] Imagen { get; set; }
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }
        public string Valor3 { get; set; }
        public string Valor4 { get; set; }
        public string Valor5 { get; set; }
        public string Valor6 { get; set; }
        public string Valor7 { get; set; }
        public string Valor8 { get; set; }
        public string Valor9 { get; set; }
        public string Valor10 { get; set; }
    }
}
