﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasIvaxAlicuotas1
    {
        public string Fcomp { get; set; }
        public short Fsuc { get; set; }
        public int Fnro { get; set; }
        public decimal Iva1050 { get; set; }
        public decimal Grav1050 { get; set; }
        public decimal Iva1900 { get; set; }
        public decimal Grav1900 { get; set; }
        public decimal Iva2100 { get; set; }
        public decimal Grav2100 { get; set; }
        public decimal Iva2700 { get; set; }
        public decimal Grav2700 { get; set; }
    }
}
