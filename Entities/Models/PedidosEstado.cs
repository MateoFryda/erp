﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosEstado
    {
        public PedidosEstado()
        {
            PedidosCabs = new HashSet<PedidosCab>();
        }

        public short Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<PedidosCab> PedidosCabs { get; set; }
    }
}
