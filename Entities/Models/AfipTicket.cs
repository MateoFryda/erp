﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipTicket
    {
        public string ServicioAfip { get; set; }
        public decimal? UniqueId { get; set; }
        public DateTime? GenerationTime { get; set; }
        public DateTime? ExpirationTime { get; set; }
        public string Token { get; set; }
        public string Sign { get; set; }
        public string TipoOperacion { get; set; }
    }
}
