﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TmpStf001
    {
        public string SaArt { get; set; }
        public string SaModelo { get; set; }
        public string SaDesc { get; set; }
        public string SaDescmed { get; set; }
        public string SaCodFabricante { get; set; }
        public string SaUmed { get; set; }
        public byte SaIva { get; set; }
        public bool SaLlevaStock { get; set; }
        public int SaStkMin { get; set; }
        public int SaStkMax { get; set; }
        public int SaPuntoPed { get; set; }
        public bool SaSerie { get; set; }
        public byte SaPartida { get; set; }
        public decimal? SaCostoStd { get; set; }
        public string SaRubro { get; set; }
        public string SaN0 { get; set; }
        public string SaN1 { get; set; }
        public string SaN2 { get; set; }
        public string SaN3 { get; set; }
        public string SaN4 { get; set; }
        public short SaAgrupacion { get; set; }
        public bool SaLlevaLote { get; set; }
        public short SaCodDescOrigen { get; set; }
        public short SaCodTipoCambio { get; set; }
        public short SaCodCostoExt { get; set; }
        public short SaCodGastoInt { get; set; }
        public short SaCodGastoFinanc { get; set; }
        public decimal SaCostoOrigen { get; set; }
        public short SaCodUtilidad { get; set; }
        public decimal SaPrecioLista { get; set; }
        public decimal SaCostofob { get; set; }
        public string SaDocumento { get; set; }
        public string SaFamilia { get; set; }
        public string SaProvVig { get; set; }
        public string SaListaPcioProvVig { get; set; }
        public DateTime? Finhabilitacion { get; set; }
        public byte SaMon { get; set; }
        public decimal SaCoeflanding { get; set; }
        public byte SaMonOrigen { get; set; }
        public string SaImpuesto { get; set; }
        public bool SaPermitedecimales { get; set; }
        public bool SaAdmitedevolucion { get; set; }
        public short SaClasif { get; set; }
        public bool SaSinDescuento { get; set; }
        public bool SaNoAptoVenta { get; set; }
        public byte[] SaFoto { get; set; }
        public decimal? SaPeso { get; set; }
        public decimal? SaVolumen { get; set; }
        public byte? SaNivel { get; set; }
        public int? SaDiasVtoLote { get; set; }
        public bool? SaNoRemitirFact { get; set; }
        public bool? SaNodisponibleWeb { get; set; }
        public bool? SaEsTrazable { get; set; }
        public byte? SaTipoArticuloFe { get; set; }
        public bool? SaSerieAuto { get; set; }
        public bool SaLlevaDespacho { get; set; }
        public string SaNomencladorCot { get; set; }
        public string SaUsuarioUltModif { get; set; }
    }
}
