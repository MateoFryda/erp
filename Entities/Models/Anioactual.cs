﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Anioactual
    {
        public int? Anioactual1 { get; set; }
        public int? Hastaanioactual { get; set; }
    }
}
