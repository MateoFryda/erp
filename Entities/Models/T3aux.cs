﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class T3aux
    {
        public string Tipocomp { get; set; }
        public string Ptovta { get; set; }
        public string Comphasta { get; set; }
        public decimal? Alicuota { get; set; }
    }
}
