﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DepositosTiposMovimientoStock
    {
        public string Codigo { get; set; }
        public string Deposito { get; set; }
        public string Tipo { get; set; }
        public int Id { get; set; }

        public virtual TiposMovimientoStock CodigoNavigation { get; set; }
        public virtual DePosito DepositoNavigation { get; set; }
    }
}
