﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class GruposFp
    {
        public string IdGrupo { get; set; }
        public string Descripcion { get; set; }
        public string BcPathTb { get; set; }
        public string BcPrefijoAtb { get; set; }
        public string BcSptransfB { get; set; }
        public string BcSpvalTb { get; set; }
        public string BcCopiatb { get; set; }
        public string BcSpmailtb { get; set; }
        public byte? PideFechaPago { get; set; }
        public byte? SpGenerarchivo { get; set; }
    }
}
