﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TmpDespiecesCalculo
    {
        public string SaProducto { get; set; }
        public int? SaNivel { get; set; }
    }
}
