﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Reemplazo
    {
        public string SaArt1 { get; set; }
        public string SaArt2 { get; set; }
        public int? CantEmpaque { get; set; }
        public int? Prioridad { get; set; }

        public virtual Stf001 SaArt1Navigation { get; set; }
        public virtual Stf001 SaArt2Navigation { get; set; }
    }
}
