﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001LotesHistorico
    {
        public string SaArt { get; set; }
        public string NroLote { get; set; }
        public DateTime FvtoAnterior { get; set; }
        public DateTime FvtoNueva { get; set; }
        public DateTime FfabricacionAnterior { get; set; }
        public DateTime FfabricacionNueva { get; set; }
        public string Usuario { get; set; }
        public DateTime? Fecha { get; set; }
        public string LoteAnterior { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
