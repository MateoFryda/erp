﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosDetCantHistico
    {
        public int? VcNro { get; set; }
        public string SaArt { get; set; }
        public string VfDetalle { get; set; }
        public decimal? VfCantHistorico { get; set; }
        public decimal? VfCantHistorico2 { get; set; }
        public decimal? VfCantNueva { get; set; }
        public decimal? VfCantNueva2 { get; set; }
        public string V7Tipo { get; set; }
        public int? V7Suc { get; set; }
        public int? V7Nro { get; set; }
    }
}
