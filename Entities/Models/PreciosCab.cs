﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosCab
    {
        public PreciosCab()
        {
            Ctf001s = new HashSet<Ctf001>();
            PreciosDets = new HashSet<PreciosDet>();
        }

        public short IdLista { get; set; }
        public string Descripcion { get; set; }
        public decimal Coef1 { get; set; }
        public decimal Coef2 { get; set; }
        public decimal Coef3 { get; set; }
        public decimal Coef4 { get; set; }
        public decimal Coef5 { get; set; }
        public decimal? IdListaorigen { get; set; }
        public DateTime? FechaVig { get; set; }
        public byte TipoLista { get; set; }
        public bool IvaIncluido { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
        public virtual ICollection<PreciosDet> PreciosDets { get; set; }
    }
}
