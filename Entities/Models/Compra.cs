﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Compra
    {
        public int CoFila { get; set; }
        public string CoDesc { get; set; }
        public string CoCampo { get; set; }
        public string CoImputa { get; set; }
        public string CoSubdi { get; set; }
        public string CoImputaProv { get; set; }
        public bool? CoModificable { get; set; }
        public string CoTipoValor { get; set; }
        public decimal? CoAlicIva { get; set; }
        public bool? CoRetencionAlComprar { get; set; }
        public string Idcalculo { get; set; }
        public decimal? Alicuota { get; set; }
        public int? TipoImpuesto { get; set; }

        public virtual ImpuestosCalculo IdcalculoNavigation { get; set; }
    }
}
