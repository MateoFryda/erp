﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001exencion
    {
        public string PaCnro { get; set; }
        public string ImpuestoCodigo { get; set; }
        public decimal Porcentaje { get; set; }
        public DateTime? Fechahasta { get; set; }

        public virtual Ptf001 PaCnroNavigation { get; set; }
    }
}
