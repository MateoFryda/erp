﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempSubdiarioAsiento
    {
        public string Tipo { get; set; }
        public int Suc { get; set; }
        public int Nro { get; set; }
        public string Comp { get; set; }
        public string Fecha { get; set; }
        public string Comprobante { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Cuit { get; set; }
        public string Anulado { get; set; }
        public string CondImp { get; set; }
        public string Codprov { get; set; }
        public string Provincia { get; set; }
        public int Ivani { get; set; }
        public int Grav1050 { get; set; }
        public int Iva1050 { get; set; }
        public int Grav1900 { get; set; }
        public int Iva1900 { get; set; }
        public int Grav2100 { get; set; }
        public int Iva2100 { get; set; }
        public int Grav2700 { get; set; }
        public int Iva2700 { get; set; }
        public decimal? Exento { get; set; }
        public int Total { get; set; }
    }
}
