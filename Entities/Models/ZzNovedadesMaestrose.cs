﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzNovedadesMaestrose
    {
        public int IdNovedad { get; set; }
        public string CodCliente { get; set; }
        public int IdConcepto { get; set; }
        public decimal Importe { get; set; }
        public short Moneda { get; set; }
        public int Cantidad { get; set; }
        public DateTime? Finicio { get; set; }
        public DateTime? Ffinal { get; set; }
        public string Descripcion { get; set; }
        public short CantCuotas { get; set; }
        public bool MuestraCuotas { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public short GrupoProforma { get; set; }
        public bool Anulado { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }
        public string Campo10 { get; set; }
        public string RazonSocial { get; set; }
        public string Dir { get; set; }
        public string Pais { get; set; }
        public string CodPostal { get; set; }
        public string CondImp { get; set; }
        public string TipoDoc { get; set; }
        public string Cuit { get; set; }
    }
}
