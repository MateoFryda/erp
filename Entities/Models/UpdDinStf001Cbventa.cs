﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UpdDinStf001Cbventa
    {
        public string SaArt { get; set; }
        public string SaCodbarra { get; set; }
    }
}
