﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf001ret
    {
        public string PaCnro { get; set; }
        public string PaTiporet { get; set; }
        public int PaCpto { get; set; }
    }
}
