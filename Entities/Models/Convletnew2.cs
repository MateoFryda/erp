﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Convletnew2
    {
        public string Tiporecibo { get; set; }
        public short Sucrecibo { get; set; }
        public int Numerrecibo { get; set; }
        public byte VeMon { get; set; }
        public decimal VeCotiz { get; set; }
        public decimal VeCotizmon { get; set; }
        public decimal? Importe { get; set; }
        public byte? MonedaImputa { get; set; }
        public string Letras { get; set; }
    }
}
