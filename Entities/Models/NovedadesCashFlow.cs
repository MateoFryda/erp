﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class NovedadesCashFlow
    {
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }
        public decimal? Importe { get; set; }
    }
}
