﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Buf002
    {
        public string BbCod { get; set; }
        public string BbProy { get; set; }
        public DateTime? BbFecdes { get; set; }
        public DateTime? BbFechas { get; set; }
    }
}
