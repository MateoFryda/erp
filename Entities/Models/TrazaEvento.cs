﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaEvento
    {
        public int? IdEvento { get; set; }
        public string DetalleEvento { get; set; }
        public int? IdDetalleEvento { get; set; }
        public int? IdTipoDetalleEvento { get; set; }
        public string DTipoDetalleEvento { get; set; }
        public int? IdTipoAgenteNotificador { get; set; }
        public string DTipoAgenteNotificador { get; set; }
        public int? IdTipoAgenteOrigen { get; set; }
        public string DTipoAgenteOrigen { get; set; }
        public int? IdTipoAgenteDestino { get; set; }
        public string DTipoAgenteDestino { get; set; }
        public int? MNotificadorOrigen { get; set; }
        public int? MInmoviliza { get; set; }
        public string IdAccionStock { get; set; }
        public string DAccionStock { get; set; }
        public string MRequiereSustancias { get; set; }
    }
}
