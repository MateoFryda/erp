﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Distribuidore
    {
        public string IdDistribuidor { get; set; }
        public string NombreDistribuidor { get; set; }
        public string IdProveedor { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdDistribuidorInterno { get; set; }
        public DateTime? Finhabilitacion { get; set; }
        public string NroDocumento { get; set; }
        public string Correo { get; set; }
    }
}
