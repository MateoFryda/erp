﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001ciclo
    {
        public Ctf001ciclo()
        {
            CicloFacturacionPeriodos = new HashSet<CicloFacturacionPeriodo>();
            Ctf001s = new HashSet<Ctf001>();
            ProformasCabs = new HashSet<ProformasCab>();
        }

        public short CodCiclo { get; set; }
        public string Descrip { get; set; }
        public DateTime? FInhabilitacion { get; set; }

        public virtual ICollection<CicloFacturacionPeriodo> CicloFacturacionPeriodos { get; set; }
        public virtual ICollection<Ctf001> Ctf001s { get; set; }
        public virtual ICollection<ProformasCab> ProformasCabs { get; set; }
    }
}
