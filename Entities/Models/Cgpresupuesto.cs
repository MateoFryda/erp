﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgpresupuesto
    {
        public string CiNro { get; set; }
        public string CiCc { get; set; }
        public string CiIt { get; set; }
        public int CiAno { get; set; }
        public int CiMes { get; set; }
        public decimal CiImporte { get; set; }
    }
}
