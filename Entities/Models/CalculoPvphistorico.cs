﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CalculoPvphistorico
    {
        public int Id { get; set; }
        public string SaArt { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Pvp { get; set; }
    }
}
