﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsFactura
    {
        public int IdOperacion { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public short? CondPago { get; set; }
        public string CodImpuesto { get; set; }
        public byte? TipoIva { get; set; }
        public byte? Tipodoc { get; set; }
        public string Cuit { get; set; }
        public string Direccion { get; set; }
        public int? CodigoPostal { get; set; }
        public short? Pais { get; set; }
        public string Provincia { get; set; }
        public string DescPcia { get; set; }
        public string CodigoPostalDesc { get; set; }
        public string Zona { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime? Fechavto { get; set; }
        public int? ListaPrecio { get; set; }
        public string Observacion { get; set; }
        public int? Transporte { get; set; }
        public string Vendedor { get; set; }
        public string LugarEntrega { get; set; }
        public string FacturaCliente { get; set; }
        public int? PtoVta { get; set; }
        public int? NroComprob { get; set; }
        public string Tcomp { get; set; }
        public byte? Moneda { get; set; }
        public decimal? Cotizacion { get; set; }
        public decimal? Cotizdolar { get; set; }
        public string TipoFacturacion { get; set; }
        public bool? Cond { get; set; }
        public bool? FacturacionAutomatica { get; set; }
        public bool? ConRemito { get; set; }
        public string FactTipo { get; set; }
        public short? FactSuc { get; set; }
        public int? FactNro { get; set; }
        public string RemTipo { get; set; }
        public short? RemSuc { get; set; }
        public int? RemNro { get; set; }
        public bool? SeGeneroIngreso { get; set; }
        public int? Ingreso { get; set; }
        public DateTime? Gdthoy { get; set; }
        public string Usuario { get; set; }
        public string TipoRem { get; set; }
        public int? NroPedido { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public bool? Facturable { get; set; }
        public bool? EsRemProv { get; set; }
        public string DepositoD { get; set; }
        public bool? GrabaStock { get; set; }
        public bool? Grabactacte { get; set; }
        public int? NroAsiento { get; set; }
        public bool? DtosManuales { get; set; }
        public int NroCompCf { get; set; }
        public string SClavePresupuesto { get; set; }
        public string SNumeroPresupuesto { get; set; }
    }
}
