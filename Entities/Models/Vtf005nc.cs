﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf005nc
    {
        public string VeComp { get; set; }
        public byte VeSuc { get; set; }
        public int VeNro { get; set; }
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
    }
}
