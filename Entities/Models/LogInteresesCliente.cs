﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class LogInteresesCliente
    {
        public int? IdEvento { get; set; }
        public DateTime? FechaEvento { get; set; }
        public int? DiasdeGracia { get; set; }
        public string TipoNovAsociado { get; set; }
        public int? NroNovAsociado { get; set; }
        public string TipoDocOriginal { get; set; }
        public int? SucDocOriginal { get; set; }
        public int? NroDocOriginal { get; set; }
        public string TipoCalculo { get; set; }
        public string TipoComprobanteCalculo { get; set; }
        public int? NroComprobanteCalculo { get; set; }
        public decimal? MontoInteres { get; set; }
        public decimal? TasaAplicada { get; set; }
        public string Usuario { get; set; }
        public decimal? MontoBase { get; set; }
        public DateTime? FechaVtoBase { get; set; }
        public byte? SucComprobanteCalculo { get; set; }
        public string Cliente { get; set; }
        public byte? Idciclo { get; set; }
        public int? Moneda { get; set; }
        public string Periodo { get; set; }
    }
}
