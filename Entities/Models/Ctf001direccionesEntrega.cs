﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001direccionesEntrega
    {
        public decimal Id { get; set; }
        public string CaCnro { get; set; }
        public string CaCnom { get; set; }
        public short? CaPais { get; set; }
        public string CaProvincia { get; set; }
        public string CaLocalidad { get; set; }
        public int? CaCp { get; set; }
        public string CaDir { get; set; }
        public string CaTel { get; set; }
        public short CaHorario { get; set; }
        public short? CaTransporte { get; set; }
        public string CaDescEntrega { get; set; }
        public string CaGln { get; set; }
        public string CaNroAfiliado { get; set; }
        public string CaDniafiliado { get; set; }
        public string CaDestinatario { get; set; }
        public string CaZona { get; set; }
        public string CaCoddom { get; set; }
        public string Cuit { get; set; }
        public int? CaId { get; set; }

        public virtual Transporte CaTransporteNavigation { get; set; }
    }
}
