﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Definicione
    {
        public string Item { get; set; }
        public string Valor { get; set; }
        public string Programa { get; set; }
        public string Valor2 { get; set; }
        public int? CantDefault { get; set; }
        public string Obs { get; set; }
    }
}
