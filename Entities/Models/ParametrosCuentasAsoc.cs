﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParametrosCuentasAsoc
    {
        public string IdConcepto { get; set; }
        public string Nombre { get; set; }
        public byte? Desde { get; set; }
        public byte? Longitud { get; set; }
    }
}
