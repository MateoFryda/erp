﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasImpxArt1
    {
        public string Fcomp { get; set; }
        public short Fsuc { get; set; }
        public int Fnro { get; set; }
        public decimal Internos { get; set; }
        public decimal Ivani { get; set; }
    }
}
