﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosCompra
    {
        public int CoFila { get; set; }
        public string ImpuestoCodigo { get; set; }
        public decimal? Alicuota { get; set; }
    }
}
