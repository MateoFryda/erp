﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DePosito
    {
        public DePosito()
        {
            DepositosTiposMovimientoStocks = new HashSet<DepositosTiposMovimientoStock>();
            DepositosUbicacions = new HashSet<DepositosUbicacion>();
            Stf007hSgDestNavigations = new HashSet<Stf007h>();
            Stf007hSgOriNavigations = new HashSet<Stf007h>();
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public byte Orden { get; set; }
        public byte Disponible { get; set; }
        public string Domicilio { get; set; }
        public string Cp { get; set; }
        public string Loc { get; set; }
        public string Pcia { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Mail { get; set; }
        public string Gln { get; set; }
        public byte? DisponibleProd { get; set; }
        public string Nombre { get; set; }
        public string Cuit { get; set; }
        public bool Convenio { get; set; }
        public bool ZonaFranca { get; set; }
        public bool? Propio { get; set; }
        public short? Actividad { get; set; }
        public string Pais { get; set; }
        public DateTime? Finhabilitacion { get; set; }
        public string CcCod { get; set; }
        public string Cliente { get; set; }

        public virtual Cgf010 CcCodNavigation { get; set; }
        public virtual ICollection<DepositosTiposMovimientoStock> DepositosTiposMovimientoStocks { get; set; }
        public virtual ICollection<DepositosUbicacion> DepositosUbicacions { get; set; }
        public virtual ICollection<Stf007h> Stf007hSgDestNavigations { get; set; }
        public virtual ICollection<Stf007h> Stf007hSgOriNavigations { get; set; }
    }
}
