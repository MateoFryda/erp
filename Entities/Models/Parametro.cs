﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Parametro
    {
        public string Clave { get; set; }
        public string Valor { get; set; }
    }
}
