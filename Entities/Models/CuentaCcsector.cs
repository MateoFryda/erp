﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CuentaCcsector
    {
        public string Cuenta { get; set; }
        public string Centro { get; set; }
        public int? Sector { get; set; }

        public virtual Cgf010 CentroNavigation { get; set; }
    }
}
