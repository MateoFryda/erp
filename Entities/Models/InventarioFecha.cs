﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class InventarioFecha
    {
        public DateTime? FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }
    }
}
