﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempIva2
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public string Impuesto { get; set; }
        public decimal Importe { get; set; }
    }
}
