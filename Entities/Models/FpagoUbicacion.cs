﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class FpagoUbicacion
    {
        public string UbicacionFisica { get; set; }
        public string FormadePago { get; set; }
        public bool EsEfectivoVuelto { get; set; }

        public virtual Btf003 FormadePagoNavigation { get; set; }
    }
}
