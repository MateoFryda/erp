﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cff002
    {
        public double? CfCod { get; set; }
        public string CfDesc { get; set; }
        public string CfMemo { get; set; }
    }
}
