﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipfeWsfexLog
    {
        public int Id { get; set; }
        public string TipoComprobante { get; set; }
        public int SucursalComprobante { get; set; }
        public int NumeroComprobante { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? Estado { get; set; }
        public int? ErrorCodigo { get; set; }
        public string ErrorDescripcion { get; set; }
    }
}
