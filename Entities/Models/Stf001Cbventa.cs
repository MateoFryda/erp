﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Cbventa
    {
        public string SaArt { get; set; }
        public string SaCodbarra { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
