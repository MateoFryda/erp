﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosImportadosDet
    {
        public int Id { get; set; }
        public int Registro { get; set; }
        public string Sku { get; set; }
        public decimal? Cantidad { get; set; }
        public decimal? Precio { get; set; }
        public int? Item { get; set; }
        public string ItemDesc { get; set; }

        public virtual PedidosImportadosCab IdNavigation { get; set; }
    }
}
