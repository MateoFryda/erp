﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposMovimientoStockUsuario
    {
        public string Codigo { get; set; }
        public string Usuario { get; set; }

        public virtual TiposMovimientoStock CodigoNavigation { get; set; }
    }
}
