﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stocksxdep1
    {
        public string SaArt { get; set; }
        public decimal? Stkmin { get; set; }
        public decimal? Stkmax { get; set; }
        public decimal? Puntoped { get; set; }
        public decimal? Stock01 { get; set; }
        public string Ubicacion01 { get; set; }
        public decimal? StkTotal { get; set; }
    }
}
