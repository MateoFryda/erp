﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosImportadosCab
    {
        public PedidosImportadosCab()
        {
            PedidosImportadosDets = new HashSet<PedidosImportadosDet>();
        }

        public int Id { get; set; }
        public string V7Tipo { get; set; }
        public int? V7Suc { get; set; }
        public int? V7Nro { get; set; }
        public string VaFcomp { get; set; }
        public int? VaSuc { get; set; }
        public int? VaFnro { get; set; }
        public int? NroPedido { get; set; }
        public string NombreFac { get; set; }
        public string ApellidosFac { get; set; }
        public string DocumentoFac { get; set; }
        public string DireccionFac { get; set; }
        public string CiudadFac { get; set; }
        public string CodProviciaFac { get; set; }
        public string Cpfac { get; set; }
        public string CodPaisFac { get; set; }
        public string MailFac { get; set; }
        public string TelefonoFac { get; set; }
        public string NombreEnv { get; set; }
        public string ApellidosEnv { get; set; }
        public string DireccionEnv { get; set; }
        public string CiudadEnv { get; set; }
        public string CodProviciaEnv { get; set; }
        public string Cpenv { get; set; }
        public string CodPaisEnv { get; set; }
        public string MetodoEnv { get; set; }
        public decimal? ImporteEnv { get; set; }
        public string FormaCobro { get; set; }
        public string Deposito { get; set; }

        public virtual ICollection<PedidosImportadosDet> PedidosImportadosDets { get; set; }
    }
}
