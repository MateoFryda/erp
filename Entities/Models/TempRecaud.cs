﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempRecaud
    {
        public string CodJuris { get; set; }
        public string Cuit { get; set; }
        public string PeriodoRet { get; set; }
        public string Cbu { get; set; }
        public string TipoCuenta { get; set; }
        public string Moneda { get; set; }
        public string Importe { get; set; }
    }
}
