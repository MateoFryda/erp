﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PresupuestosComprasDet
    {
        public int PdNro { get; set; }
        public string PdPnro { get; set; }
        public string PdArt { get; set; }
        public string PdDesc { get; set; }
        public decimal PdCant { get; set; }
        public decimal PdPcio { get; set; }
        public string PdUmed { get; set; }

        public virtual Stf001 PdArtNavigation { get; set; }
    }
}
