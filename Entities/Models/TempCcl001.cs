﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempCcl001
    {
        public string CdCnro { get; set; }
        public string CdFcomp { get; set; }
        public int? CdSuc { get; set; }
        public int? CdFnro { get; set; }
        public string CdComp { get; set; }
        public DateTime? CdFfech { get; set; }
        public DateTime? CdFvto { get; set; }
        public decimal? CdSaldo { get; set; }
        public decimal? CdTotal { get; set; }
        public byte? CdMon { get; set; }
        public decimal? CdCotizmon { get; set; }
        public decimal? CdCotiz { get; set; }
        public string Concepto { get; set; }
        public string Nombre { get; set; }
        public string CaContacto { get; set; }
        public byte? CaCpag { get; set; }
    }
}
