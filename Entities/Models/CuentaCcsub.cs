﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CuentaCcsub
    {
        public string Cuenta { get; set; }
        public string Centro { get; set; }
        public string SubCc { get; set; }

        public virtual Cgf010 CentroNavigation { get; set; }
        public virtual Cgf011 SubCcNavigation { get; set; }
    }
}
