﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosCodigo
    {
        public ImpuestosCodigo()
        {
            ImpuestosDetalles = new HashSet<ImpuestosDetalle>();
            ImpuestosPlanDets = new HashSet<ImpuestosPlanDet>();
        }

        public string IdImpuesto { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public byte? DiscriminaEnFactura { get; set; }
        public decimal PorExenCia { get; set; }
        public DateTime? FechaVigenciaExencionD { get; set; }
        public DateTime? FechaVigenciaExencionH { get; set; }

        public virtual ImpuestosTipo IdImpuestoNavigation { get; set; }
        public virtual ICollection<ImpuestosDetalle> ImpuestosDetalles { get; set; }
        public virtual ICollection<ImpuestosPlanDet> ImpuestosPlanDets { get; set; }
    }
}
