﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsImpuestosxArt
    {
        public int IdOperacion { get; set; }
        public string Art { get; set; }
        public string Remitotipo { get; set; }
        public int? Remitosuc { get; set; }
        public int? Remitonro { get; set; }
        public string Idimpuesto { get; set; }
        public string IdCalculo { get; set; }
        public int? CodTransac { get; set; }
        public string Descripcion { get; set; }
        public string ImputacionVentas { get; set; }
        public string ImputacionCompras { get; set; }
        public decimal? Impuesto { get; set; }
        public decimal? Alicuota { get; set; }
        public decimal? Base { get; set; }
        public decimal? Exento { get; set; }
        public decimal? Impuestoext { get; set; }
        public decimal? BaseExt { get; set; }
        public decimal? ExentoExt { get; set; }
        public byte? Orden { get; set; }
    }
}
