﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Presentacion
    {
        public Stf001Presentacion()
        {
            Stf001s = new HashSet<Stf001>();
        }

        public int Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Stf001> Stf001s { get; set; }
    }
}
