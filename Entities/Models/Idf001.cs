﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Idf001
    {
        public double? IdAn { get; set; }
        public double? IdMes { get; set; }
        public double? IdIndmay { get; set; }
        public double? IdIndmin { get; set; }
        public double? IdIndIpc { get; set; }
    }
}
