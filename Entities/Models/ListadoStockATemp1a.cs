﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ListadoStockATemp1a
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public string Comprobante { get; set; }
        public DateTime Fecha { get; set; }
        public string Origen { get; set; }
        public string DescOri { get; set; }
        public string Destino { get; set; }
        public string DescDest { get; set; }
        public decimal? Cantidad { get; set; }
    }
}
