﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vendedore
    {
        public Vendedore()
        {
            Ctf001s = new HashSet<Ctf001>();
        }

        public string IdVendedor { get; set; }
        public string NombreVendedor { get; set; }
        public string IdProveedor { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdVendedorInterno { get; set; }
        public DateTime? Finhabilitacion { get; set; }
        public string NroDocumento { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
    }
}
