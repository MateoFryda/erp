﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Audit
    {
        public string TipoComprobante { get; set; }
        public string Numero { get; set; }
        public DateTime? Fecha { get; set; }
        public string UserName { get; set; }
        public string Tabla { get; set; }
        public string Hora { get; set; }
        public string Debug { get; set; }
    }
}
