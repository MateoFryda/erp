﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class MovCajaMovimiento
    {
        public DateTime FechayHora { get; set; }
        public string Ubicacion { get; set; }
        public string Usuario { get; set; }
        public int CodMov { get; set; }
        public decimal ImporteLoc { get; set; }
        public decimal ImporteExt { get; set; }
        public decimal Cotiz { get; set; }
        public string Fpago { get; set; }
        public string Descrip { get; set; }

        public virtual MovCajaCodigo CodMovNavigation { get; set; }
    }
}
