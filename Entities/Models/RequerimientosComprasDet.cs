﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RequerimientosComprasDet
    {
        public string RcTipo { get; set; }
        public int RcNumero { get; set; }
        public string SaArt { get; set; }
        public short RcIdlinea { get; set; }
        public DateTime? RcFechadisponibilidad { get; set; }
        public string RcDescripcion { get; set; }
        public string RcDescripcionaux { get; set; }
        public decimal? RcCantidad { get; set; }
        public decimal? RcCantidadcomprar { get; set; }
        public string RcUnidadmedida { get; set; }
        public decimal? RcPrecioestimado { get; set; }
        public decimal? RcPreciocomprar { get; set; }
        public string RcCentrocosto { get; set; }
        public string RcSubcentrocosto { get; set; }
        public string RcImputacion { get; set; }
        public string RcEsptec { get; set; }
        public DateTime? RcFecharechazo { get; set; }
        public string RcMotivo { get; set; }
        public string RcUsuarioaviso { get; set; }
        public string RcUsuario { get; set; }
        public bool RcReqCerrado { get; set; }
        public int? RcNroOt { get; set; }
        public short? RcNroLinOt { get; set; }
        public string RcProveedorItem { get; set; }
        public string RcObsItem { get; set; }

        public virtual RequerimientosComprasCab Rc { get; set; }
    }
}
