﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposDocxBanco
    {
        public string IdBanco { get; set; }
        public string IdTipoDoc { get; set; }
        public string Descripcion { get; set; }
    }
}
