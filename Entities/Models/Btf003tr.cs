﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Btf003tr
    {
        public int Tipo { get; set; }
        public string Descripcion { get; set; }
    }
}
