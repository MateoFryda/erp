﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Transporte
    {
        public Transporte()
        {
            Ctf001s = new HashSet<Ctf001>();
            RemitosCabs = new HashSet<RemitosCab>();
        }

        public short TaCodigo { get; set; }
        public string TaDescripcion { get; set; }
        public string TaDomicilio { get; set; }
        public string TaCp { get; set; }
        public string TaLoc { get; set; }
        public string TaPcia { get; set; }
        public string TaTel { get; set; }
        public string TaFax { get; set; }
        public string TaMail { get; set; }
        public string TaJefe { get; set; }
        public string TaFilcar { get; set; }
        public string TaZona { get; set; }
        public string TaCuit { get; set; }
        public string TaPcianueva { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
        public virtual ICollection<RemitosCab> RemitosCabs { get; set; }
    }
}
