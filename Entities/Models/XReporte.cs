﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XReporte
    {
        public string IdReporte { get; set; }
        public string NombreReporte { get; set; }
        public string SpReporte { get; set; }
    }
}
