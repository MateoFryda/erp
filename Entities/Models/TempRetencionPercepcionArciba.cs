﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempRetencionPercepcionArciba
    {
        public string TipoOperacion { get; set; }
        public string CodigoNorma { get; set; }
        public string FechaRet { get; set; }
        public string TipoComprobanteOrigen { get; set; }
        public string Letra { get; set; }
        public string NroComp { get; set; }
        public string FechaComp { get; set; }
        public string MontoComprobante { get; set; }
        public string NroCertificado { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string SituacionIb { get; set; }
        public string NroInscripcionIb { get; set; }
        public string SituacionIva { get; set; }
        public string RazonSocial { get; set; }
        public string OtrosConceptos { get; set; }
        public string ImporteIva { get; set; }
        public string MontoSujetoRetencion { get; set; }
        public string Alicuota { get; set; }
        public string MontoRetencion { get; set; }
        public string MontoTotalRetenido { get; set; }
    }
}
