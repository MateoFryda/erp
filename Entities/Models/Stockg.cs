﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stockg
    {
        public string SaModelo { get; set; }
        public string SaDesc { get; set; }
        public string SaDescmed { get; set; }
        public double? SaCodBarra { get; set; }
        public string SaCodFabricante { get; set; }
        public string SaUmed { get; set; }
        public double? SaIva { get; set; }
        public double? SaCodImpInt { get; set; }
        public bool? SaLlevaStock { get; set; }
        public double? SaStkMin { get; set; }
        public double? SaStkMax { get; set; }
        public double? SaPuntoPed { get; set; }
        public double? SaListaPcio { get; set; }
        public double? SaComision { get; set; }
        public bool? SaHabilitado { get; set; }
        public bool? SaSerie { get; set; }
        public double? SaPartida { get; set; }
        public double? SaCostoStd { get; set; }
        public double? SaPuc { get; set; }
        public double? SaFuc { get; set; }
        public double? SaPpp { get; set; }
        public double? SaRubro { get; set; }
        public double? SaN0 { get; set; }
        public string SaN1 { get; set; }
        public string SaN2 { get; set; }
        public string SaN3 { get; set; }
        public string SaN4 { get; set; }
        public double? SaAgrupacion { get; set; }
        public bool? SaLlevaLote { get; set; }
        public double? SaCodDescOrigen { get; set; }
        public string SaCodTipoCambio { get; set; }
        public double? SaCodCostoExt { get; set; }
        public double? SaCodGastoInt { get; set; }
        public double? SaCodGastoFinanc { get; set; }
        public double? SaCodUtilidad { get; set; }
        public double? SaPrecioLista { get; set; }
        public string SaArt { get; set; }
        public float? SaCoefgi { get; set; }
        public float? SaCoefcf { get; set; }
        public decimal? SaCostofob { get; set; }
        public decimal? SaCostoorigen { get; set; }
    }
}
