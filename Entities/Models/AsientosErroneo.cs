﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AsientosErroneo
    {
        public int CgNro { get; set; }
        public DateTime CgFecha { get; set; }
        public decimal? Diferencia { get; set; }
        public string CgTipo { get; set; }
        public string CgTcomp { get; set; }
        public int? CgSuc { get; set; }
        public int? CgNcomp { get; set; }
        public byte CgContab { get; set; }
    }
}
