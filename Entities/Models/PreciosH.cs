﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosH
    {
        public int PhCodigo { get; set; }
        public string PhDescripcion { get; set; }
        public DateTime PhFalta { get; set; }
        public DateTime? PhFfinVig { get; set; }
        public int PhMoneda { get; set; }
    }
}
