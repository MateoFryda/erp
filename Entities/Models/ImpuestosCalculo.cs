﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosCalculo
    {
        public ImpuestosCalculo()
        {
            Compras = new HashSet<Compra>();
            ImpuestosAlicuota = new HashSet<ImpuestosAlicuota>();
            ImpuestosDetalles = new HashSet<ImpuestosDetalle>();
        }

        public string IdCalculo { get; set; }
        public short? Provincia { get; set; }
        public DateTime? FechaVigDesde { get; set; }
        public DateTime? FechaVigHasta { get; set; }
        public decimal MontoDesde { get; set; }
        public decimal? MontoHasta { get; set; }
        public decimal MontoArestar { get; set; }
        public decimal Alicuota { get; set; }
        public decimal? ImpuestoMaximo { get; set; }
        public bool CalculaImpSobreImp { get; set; }
        public string ImputacionVentas { get; set; }
        public string ImputacionCompras { get; set; }
        public string ImputacionVentasCob { get; set; }
        public string ImputacionComprasCob { get; set; }
        public bool? EsDefault { get; set; }
        public int? CodImpAfip { get; set; }
        public string DesAfipImp { get; set; }
        public string Ccosto { get; set; }

        public virtual Ctf009 ImputacionComprasNavigation { get; set; }
        public virtual Ctf009 ImputacionVentasNavigation { get; set; }
        public virtual Provincia ProvinciaNavigation { get; set; }
        public virtual ICollection<Compra> Compras { get; set; }
        public virtual ICollection<ImpuestosAlicuota> ImpuestosAlicuota { get; set; }
        public virtual ICollection<ImpuestosDetalle> ImpuestosDetalles { get; set; }
    }
}
