﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class EmailComprobantesEnviado
    {
        public string TipoComp { get; set; }
        public int Suc { get; set; }
        public int Nro { get; set; }
        public int? IdEmail { get; set; }
        public DateTime? FEnviado { get; set; }
    }
}
