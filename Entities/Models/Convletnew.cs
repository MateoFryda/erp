﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Convletnew
    {
        public string Tiporecibo { get; set; }
        public short Sucrecibo { get; set; }
        public int Numerrecibo { get; set; }
        public string Letras { get; set; }
    }
}
