﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParametrosCashflowEspeciale
    {
        public string Tabla { get; set; }
        public string Parametrocampo { get; set; }
        public string Valor { get; set; }
        public short Id { get; set; }
        public string Criteriopositivo { get; set; }
        public string Criterionegativo { get; set; }
        public byte Orden { get; set; }
    }
}
