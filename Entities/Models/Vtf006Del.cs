﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf006Del
    {
        public string TUsuario { get; set; }
        public DateTime? TFechayhora { get; set; }
        public string VfComp { get; set; }
        public int VfNro { get; set; }
        public short VfSuc { get; set; }
        public string VfCcomp { get; set; }
        public short? VfCsuc { get; set; }
        public int VfCnro { get; set; }
        public decimal VfImporte { get; set; }
        public double? VfIva { get; set; }
        public double? VfTotuss { get; set; }
        public string VfImputa { get; set; }
        public string VfCcosto { get; set; }
        public string VfItemc { get; set; }
        public string VfCta { get; set; }
        public DateTime? VfFdep { get; set; }
        public DateTime? VfFvto { get; set; }
        public int VfId { get; set; }
    }
}
