﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VendedoresInterno
    {
        public string IdVendedorInterno { get; set; }
        public string NombreVendedorInterno { get; set; }
    }
}
