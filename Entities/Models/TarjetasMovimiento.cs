﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TarjetasMovimiento
    {
        public int IdMov { get; set; }
        public string FormaDePago { get; set; }
        public string NroTarjeta { get; set; }
        public string Lote { get; set; }
        public string Autorizacion { get; set; }
        public decimal Importe { get; set; }
        public decimal ImporteExt { get; set; }
        public byte? Cuotas { get; set; }
        public string Fvtotarj { get; set; }
        public string Dni { get; set; }
        public string CodigoAutorizacion { get; set; }
        public string VeComp { get; set; }
        public byte VeSuc { get; set; }
        public int VeNro { get; set; }
        public decimal Cotizacion { get; set; }

        public virtual Btf003 FormaDePagoNavigation { get; set; }
    }
}
