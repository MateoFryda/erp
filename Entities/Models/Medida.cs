﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Medida
    {
        public int? Idmedida { get; set; }
        public string Descripcion { get; set; }
    }
}
