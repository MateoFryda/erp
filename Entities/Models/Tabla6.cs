﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tabla6
    {
        public string FechComp { get; set; }
        public string TipoComp { get; set; }
        public string PtoVta { get; set; }
        public string Comprobante { get; set; }
        public int? JurisIibb { get; set; }
        public decimal IngBr { get; set; }
        public decimal ImpMun { get; set; }
        public string JurisMun { get; set; }
    }
}
