﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Provincia
    {
        public Provincia()
        {
            ImpuestosCalculos = new HashSet<ImpuestosCalculo>();
            VentasCabProformas = new HashSet<VentasCabProforma>();
            VentasCabs = new HashSet<VentasCab>();
        }

        public short PvCodigoInterno { get; set; }
        public string PvDescripcion { get; set; }
        public string PvCodigo { get; set; }
        public string Jurisdiccion { get; set; }
        public int? PvCodafip { get; set; }
        public int? Codigoiibb { get; set; }

        public virtual ICollection<ImpuestosCalculo> ImpuestosCalculos { get; set; }
        public virtual ICollection<VentasCabProforma> VentasCabProformas { get; set; }
        public virtual ICollection<VentasCab> VentasCabs { get; set; }
    }
}
