﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf002
    {
        public string CbIdent { get; set; }
        public int CbCod { get; set; }
        public string CbDesc { get; set; }
        public double CbCoef { get; set; }
        public double CbCoef1 { get; set; }
        public double CbCoef2 { get; set; }
        public double CbCoef3 { get; set; }
        public double CbCoef4 { get; set; }
        public double CbCoef5 { get; set; }
        public double CbCoef6 { get; set; }
        public double CbCoef7 { get; set; }
        public string CbDesc2 { get; set; }
        public string CodRegimen { get; set; }
    }
}
