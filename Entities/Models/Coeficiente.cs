﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Coeficiente
    {
        public string CoTipo { get; set; }
        public int CoCodigo { get; set; }
        public decimal CoCoef { get; set; }
        public string CoDescrip { get; set; }
    }
}
