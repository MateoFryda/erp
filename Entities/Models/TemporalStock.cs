﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalStock
    {
        public string Deposito { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public string Nrolote { get; set; }
        public DateTime Fvto { get; set; }
        public string Nroserie { get; set; }
        public decimal Cantidad { get; set; }
        public string DescDeposito { get; set; }
        public string Cuit { get; set; }
    }
}
