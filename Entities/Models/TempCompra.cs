﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempCompra
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public decimal? TotalCompras { get; set; }
        public decimal PercepIva { get; set; }
        public int PercepNac { get; set; }
        public decimal? PercepIibb { get; set; }
        public int PercepMuni { get; set; }
        public int PercepInt { get; set; }
        public int OtrosTributos { get; set; }
    }
}
