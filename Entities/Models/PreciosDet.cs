﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosDet
    {
        public short IdLista { get; set; }
        public string SaArt { get; set; }
        public decimal Precio { get; set; }

        public virtual PreciosCab IdListaNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
