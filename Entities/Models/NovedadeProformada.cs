﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class NovedadeProformada
    {
        public string VaPeriodo { get; set; }
        public string VaClnro { get; set; }
        public string CaCnom { get; set; }
        public string IdProforma { get; set; }
        public string CaCnro { get; set; }
    }
}
