﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class NormasAgipretPerc
    {
        public short CbCodOperacion { get; set; }
        public string CbCodNorma { get; set; }
        public string CbDescNorma { get; set; }
    }
}
