﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TipoRequerimientosUsuario
    {
        public string Usuario { get; set; }
        public string Tipo { get; set; }
    }
}
