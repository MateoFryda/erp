﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001ainhabilitar
    {
        public string SaArt { get; set; }
        public DateTime? FechaBaja { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
