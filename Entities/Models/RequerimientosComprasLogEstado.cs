﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RequerimientosComprasLogEstado
    {
        public int RcNumero { get; set; }
        public string RcTipo { get; set; }
        public DateTime RcFecha { get; set; }
        public string RcUsuario { get; set; }
        public short RcEstadoanterior { get; set; }
        public short RcEstadoactual { get; set; }
        public int Ident { get; set; }
        public string RcComentarios { get; set; }
    }
}
