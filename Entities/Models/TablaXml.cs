﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaXml
    {
        public decimal Id { get; set; }
        public short Indent { get; set; }
        public string NodeName { get; set; }
        public string NodeValue { get; set; }
    }
}
