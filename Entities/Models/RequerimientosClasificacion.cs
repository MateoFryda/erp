﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RequerimientosClasificacion
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
