﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tabla1
    {
        public int TpRegistro { get; set; }
        public string FechComp { get; set; }
        public string TipoComp { get; set; }
        public string Cfiscal { get; set; }
        public string PtoVta { get; set; }
        public string Compdesde { get; set; }
        public string Comphasta { get; set; }
        public int? TipoDocAfip { get; set; }
        public string CaCuit { get; set; }
        public string CaCnom { get; set; }
        public decimal? Total { get; set; }
        public int Campo12 { get; set; }
        public decimal? Gravado { get; set; }
        public decimal Alicuota { get; set; }
        public decimal? Implocal { get; set; }
        public int ImpRni { get; set; }
        public decimal? Exento { get; set; }
        public decimal ImpNac { get; set; }
        public decimal IngBr { get; set; }
        public decimal ImpMun { get; set; }
        public decimal ImpInt { get; set; }
        public string TipoResp { get; set; }
        public string Moneda { get; set; }
        public decimal TipoCambio { get; set; }
        public string CodOperacion { get; set; }
        public string CaiNro { get; set; }
        public string CaiVto { get; set; }
        public string FechaAnul { get; set; }
    }
}
