﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf206
    {
        public int PfNro { get; set; }
        public string PfCcomp { get; set; }
        public int PfCnro { get; set; }
        public decimal PfImporte { get; set; }
        public decimal? PfTotuss { get; set; }
        public int? PfBco { get; set; }
        public string PfImputa { get; set; }
        public string PfCcosto { get; set; }
        public string PfItemc { get; set; }
        public decimal PfIdent { get; set; }
        public string PfObservaciones { get; set; }
        public int? PfNrolote { get; set; }
        public string SaArt { get; set; }
    }
}
