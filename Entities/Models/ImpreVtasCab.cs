﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpreVtasCab
    {
        public string Cnro { get; set; }
        public string Cnom { get; set; }
        public string Dir { get; set; }
        public string Cp { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Tiva { get; set; }
        public string Cuit { get; set; }
        public string Nroingb { get; set; }
        public string Fecha { get; set; }
        public string Fechavto { get; set; }
        public string Cpag { get; set; }
        public float? Totex { get; set; }
        public float? Total { get; set; }
        public float? Total27 { get; set; }
        public float? Iva { get; set; }
        public float? Iva27 { get; set; }
        public float? Ivani { get; set; }
        public float? Ivani27 { get; set; }
        public string Fcomp { get; set; }
        public short? Fsuc { get; set; }
        public short? Fnro { get; set; }
        public float? Interno { get; set; }
        public string Obs { get; set; }
        public string Detalle { get; set; }
        public string Lentrega { get; set; }
        public float? Dto { get; set; }
        public string PedidoNro { get; set; }
        public string Vendedor { get; set; }
        public string Operador { get; set; }
        public string RemitoNro { get; set; }
    }
}
