﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipWebService
    {
        public int Id { get; set; }
        public string Servicio { get; set; }
        public string Descripcion { get; set; }
        public string UrlPrueba { get; set; }
        public string UrlProduccion { get; set; }
        public string UrlLoginPrueba { get; set; }
        public string UrlLoginProduccion { get; set; }
        public int ClavePrueba { get; set; }
        public int ClaveProduccion { get; set; }
        public string CuitPrueba { get; set; }
        public string CuitProduccion { get; set; }
        public int WsTipo { get; set; }
    }
}
