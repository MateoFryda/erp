﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CreditosYdebitosArecibir
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public string PcPnro { get; set; }
        public DateTime? PcFfech { get; set; }
        public decimal? PcTotal { get; set; }
        public decimal? PcDto { get; set; }
        public decimal? PcIva { get; set; }
        public decimal? PcIva27 { get; set; }
        public decimal? PcIvaad { get; set; }
        public DateTime? PcFvto { get; set; }
        public decimal PcCotiz { get; set; }
        public byte PcMon { get; set; }
        public string PcFcprov { get; set; }
        public decimal? PcTotex { get; set; }
        public string PcObs { get; set; }
        public double? PcCpag { get; set; }
        public DateTime? PcFreal { get; set; }
        public decimal? PcRg3431 { get; set; }
        public decimal? PcRg3543 { get; set; }
        public decimal? PcIngbr { get; set; }
        public bool PcSubdi { get; set; }
        public double? PcLitros { get; set; }
        public string PcMemo { get; set; }
        public double PcSector { get; set; }
        public string PcMarca { get; set; }
        public DateTime? PcFrecep { get; set; }
        public DateTime? PcFenv { get; set; }
        public DateTime PcFaprob { get; set; }
        public DateTime PcFateso { get; set; }
        public string PcTipo { get; set; }
        public decimal? PcRetiva { get; set; }
        public int? PcClasificacion { get; set; }
        public string PcUser1 { get; set; }
        public string PcUser2 { get; set; }
        public string PcUser3 { get; set; }
        public string PcUser4 { get; set; }
        public double? PcUsern1 { get; set; }
        public double? PcUsern2 { get; set; }
        public double? PcUsern3 { get; set; }
        public double? PcUsern4 { get; set; }
        public string PcUser { get; set; }
        public string PcComori { get; set; }
        public decimal? PcSucori { get; set; }
        public decimal? PcNroori { get; set; }
        public string PcProvincia { get; set; }
        public int? PcEstado { get; set; }
        public string PcTipoOrdenCompra { get; set; }
        public int? PcNroOrdenCompra { get; set; }
        public string PcTipoFacturaAsociada { get; set; }
        public double? PcNroFacturaAsociada { get; set; }
        public DateTime? PcFpago { get; set; }
        public decimal PcCotizmon { get; set; }
    }
}
