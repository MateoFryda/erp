﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosCondicionDocumento
    {
        public byte Codigo { get; set; }
        public short IdTipoDoc { get; set; }

        public virtual ImpuestosCondicion CodigoNavigation { get; set; }
        public virtual TiposDocumento IdTipoDocNavigation { get; set; }
    }
}
