﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempVentasCbteExcelX
    {
        public string Tipo { get; set; }
        public string Suc { get; set; }
        public string Nro { get; set; }
        public string Fecha { get; set; }
        public string Nombre { get; set; }
        public string TipoDoc { get; set; }
        public string Documento { get; set; }
        public string Provincia { get; set; }
        public string Pais { get; set; }
        public string Fvto { get; set; }
        public string Moneda { get; set; }
        public string Cotiz { get; set; }
        public string Exento { get; set; }
        public string Grav250 { get; set; }
        public string Iva250 { get; set; }
        public string Grav500 { get; set; }
        public string Iva500 { get; set; }
        public string Grav1050 { get; set; }
        public string Iva1050 { get; set; }
        public string Grav2100 { get; set; }
        public string Iva2100 { get; set; }
        public string Grav2700 { get; set; }
        public string Iva2700 { get; set; }
        public string ImpuestosNacionales { get; set; }
        public string ImpuestosIibb { get; set; }
        public string ImpuestosMunicipales { get; set; }
        public string ImpuestosInternos { get; set; }
        public string OtrosTributos { get; set; }
        public string Total { get; set; }
    }
}
