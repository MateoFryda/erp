﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ConceptosTipo
    {
        public ConceptosTipo()
        {
            NovedadesConceptos = new HashSet<NovedadesConcepto>();
        }

        public int Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<NovedadesConcepto> NovedadesConceptos { get; set; }
    }
}
