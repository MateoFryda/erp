﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Temppercepiibbsuf
    {
        public string CodJuris { get; set; }
        public string Cuit { get; set; }
        public string FechaRet { get; set; }
        public string SucComprobante { get; set; }
        public string Nrocomprob { get; set; }
        public string Tipo { get; set; }
        public string Letra { get; set; }
        public string Importe { get; set; }
    }
}
