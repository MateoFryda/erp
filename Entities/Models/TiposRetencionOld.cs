﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposRetencionOld
    {
        public short Codigo { get; set; }
        public short? Codopafip { get; set; }
        public string Descopafip { get; set; }
        public short? Codimpafip { get; set; }
        public string Descimpafip { get; set; }
    }
}
