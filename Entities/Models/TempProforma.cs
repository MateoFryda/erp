﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempProforma
    {
        public int? CodTransaccion { get; set; }
        public int? IdProforma { get; set; }
        public byte? TipoRegistro { get; set; }
        public string Item1 { get; set; }
        public string Item2 { get; set; }
        public string Item3 { get; set; }
        public string Item4 { get; set; }
        public string Item5 { get; set; }
        public string Item6 { get; set; }
        public string Item7 { get; set; }
        public bool? ErrorCarga { get; set; }
    }
}
