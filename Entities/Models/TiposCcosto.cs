﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposCcosto
    {
        public byte IdTipo { get; set; }
        public string Descripcion { get; set; }
    }
}
