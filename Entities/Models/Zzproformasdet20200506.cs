﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzproformasdet20200506
    {
        public int CodProforma { get; set; }
        public int Linea { get; set; }
        public string Articulo { get; set; }
        public decimal Cant { get; set; }
        public decimal Precio { get; set; }
        public string Descripcion { get; set; }
        public string CodImpuesto { get; set; }
        public int? CodAgrupCont { get; set; }
        public string Ccosto { get; set; }
        public string Scosto { get; set; }
        public string VaFcomp { get; set; }
        public int? VaSuc { get; set; }
        public int? VaFnro { get; set; }
    }
}
