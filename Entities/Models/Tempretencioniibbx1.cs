﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempretencioniibbx1
    {
        public string Cuit { get; set; }
        public string NroAgente { get; set; }
        public string DigitoV { get; set; }
        public string FechaRet { get; set; }
        public string NroComp { get; set; }
        public string Importe { get; set; }
        public string MontoBase { get; set; }
    }
}
