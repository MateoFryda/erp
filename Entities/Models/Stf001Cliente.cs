﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001Cliente
    {
        public string SaArt { get; set; }
        public string CaCnro { get; set; }
        public string CodArtCli { get; set; }

        public virtual Ctf001 CaCnroNavigation { get; set; }
        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
