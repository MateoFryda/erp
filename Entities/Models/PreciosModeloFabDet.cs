﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosModeloFabDet
    {
        public int IdModelo { get; set; }
        public string Codigo { get; set; }
    }
}
