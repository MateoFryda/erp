﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasDetProforma
    {
        public VentasDetProforma()
        {
            VentasDetImpuestosProformas = new HashSet<VentasDetImpuestosProforma>();
        }

        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public string SaArt { get; set; }
        public int VbId { get; set; }
        public decimal VbCant { get; set; }
        public decimal VbPciolocal { get; set; }
        public decimal VbPcioextranjera { get; set; }
        public decimal VbPciofinalLocal { get; set; }
        public decimal VbPciofinalExtranjera { get; set; }
        public string VbDetalle { get; set; }
        public decimal? VbPorcDto1 { get; set; }
        public decimal? VbPorcDto2 { get; set; }
        public decimal? VbPorcDto3 { get; set; }
        public decimal? VbPorcDto4 { get; set; }
        public decimal VbPcioLista { get; set; }
        public string V7Tipo { get; set; }
        public short? V7Suc { get; set; }
        public int? V7Nro { get; set; }
        public string VcTipo { get; set; }
        public short? VcSuc { get; set; }
        public string VcNro { get; set; }
        public string VaFcompdev { get; set; }
        public int? VaSucdev { get; set; }
        public int? VaFnrodev { get; set; }
        public string VbTxtDto1 { get; set; }
        public string VbTxtDto2 { get; set; }
        public string VbTxtDto3 { get; set; }
        public string VbTxtDto4 { get; set; }
        public decimal? VbCostoStdlocal { get; set; }
        public decimal? VbCostoStdext { get; set; }
        public decimal? VbCostoUclocal { get; set; }
        public decimal? VbCostoUcext { get; set; }
        public decimal? VbCostoPpplocal { get; set; }
        public decimal? VbCostoPppext { get; set; }
        public decimal? VbCostoFifolocal { get; set; }
        public decimal? VbCostoFifoext { get; set; }
        public string VaCc { get; set; }
        public string VaIc { get; set; }
        public decimal VbTotalLocal { get; set; }
        public decimal VbTotalextranjera { get; set; }
        public short VbOrden { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
        public virtual VentasCab Va { get; set; }
        public virtual ICollection<VentasDetImpuestosProforma> VentasDetImpuestosProformas { get; set; }
    }
}
