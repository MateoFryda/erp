﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasCabProforma
    {
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public string VaClnro { get; set; }
        public int? VaCpag { get; set; }
        public DateTime VaFfech { get; set; }
        public DateTime? VaFvto { get; set; }
        public string VaVen { get; set; }
        public string VaObs { get; set; }
        public string VaEnt { get; set; }
        public decimal VaCotiz { get; set; }
        public byte VaAnul { get; set; }
        public string VaComp { get; set; }
        public DateTime? VaFanul { get; set; }
        public int? VaTrans { get; set; }
        public string VaUser { get; set; }
        public int? VaLista { get; set; }
        public string VaPerfil { get; set; }
        public string VaLentrega { get; set; }
        public byte VaMon { get; set; }
        public decimal VaCotizmon { get; set; }
        public short? VaPciaCodigo { get; set; }
        public DateTime? VaFpresentacion { get; set; }
        public bool? VaEsFacturaContado { get; set; }
        public bool? VaEsFacturaManual { get; set; }
        public string CaDir { get; set; }
        public string CaCp { get; set; }
        public string CaZona { get; set; }
        public string CaLocalidad { get; set; }
        public string CaCuit { get; set; }
        public byte? CaTipodoc { get; set; }
        public string CaIdAgrupImp { get; set; }
        public byte? VaTiva { get; set; }
        public string CaCnom { get; set; }
        public DateTime? Fregistro { get; set; }
        public short? CaPais { get; set; }
        public bool? DtosManuales { get; set; }
        public string VaPeriodo { get; set; }
        public string VaCae { get; set; }
        public string VaObsCae { get; set; }
        public DateTime? VaFvtoCae { get; set; }
        public string VcExpediente { get; set; }
        public string VcPedidoCliente { get; set; }
        public string VaEstado { get; set; }
        public int? VaSucfactura { get; set; }
        public int? VaFnrofactura { get; set; }

        public virtual Ctf001 VaClnroNavigation { get; set; }
        public virtual Provincia VaPciaCodigoNavigation { get; set; }
    }
}
