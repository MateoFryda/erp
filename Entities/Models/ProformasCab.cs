﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProformasCab
    {
        public ProformasCab()
        {
            NovedadesPeriodos = new HashSet<NovedadesPeriodo>();
            ProformasDets = new HashSet<ProformasDet>();
            ProformasImpuestos = new HashSet<ProformasImpuesto>();
        }

        public int CodProforma { get; set; }
        public string CaCnro { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime? Fvto { get; set; }
        public int? Mon { get; set; }
        public decimal? CotizMon { get; set; }
        public decimal? Cotiz { get; set; }
        public string Periodo { get; set; }
        public string Observaciones { get; set; }
        public bool Facturado { get; set; }
        public bool Anulado { get; set; }
        public string Tcomp { get; set; }
        public short Ciclo { get; set; }
        public int? GrupoProforma { get; set; }

        public virtual Ctf001 CaCnroNavigation { get; set; }
        public virtual Ctf001ciclo CicloNavigation { get; set; }
        public virtual ProformasCabExtra ProformasCabExtra { get; set; }
        public virtual ICollection<NovedadesPeriodo> NovedadesPeriodos { get; set; }
        public virtual ICollection<ProformasDet> ProformasDets { get; set; }
        public virtual ICollection<ProformasImpuesto> ProformasImpuestos { get; set; }
    }
}
