﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RemitosCab
    {
        public RemitosCab()
        {
            RemitosDets = new HashSet<RemitosDet>();
        }

        public string V7Tipo { get; set; }
        public short V7Suc { get; set; }
        public int V7Nro { get; set; }
        public DateTime V7Ffech { get; set; }
        public DateTime? V7Fvto { get; set; }
        public string V7Clnro { get; set; }
        public string V7Prov { get; set; }
        public string V7Ven { get; set; }
        public byte? V7Cpag { get; set; }
        public short V7Transporte { get; set; }
        public int? V7Lista { get; set; }
        public string V7Obs { get; set; }
        public string V7Usuario { get; set; }
        public string V7CodigoDeFacturacion { get; set; }
        public string V7Lentrega { get; set; }
        public string V7Clasificacion { get; set; }
        public DateTime? V7Fdespacho { get; set; }
        public int? V7NroEntrega { get; set; }
        public int? V7Anul { get; set; }
        public DateTime? V7FechaConfirmacion { get; set; }
        public bool? V7Facturable { get; set; }
        public string V7Tipocomp { get; set; }
        public DateTime? Fregistro { get; set; }
        public string V7Cc { get; set; }
        public string V7Ic { get; set; }
        public int? CantBultos { get; set; }
        public decimal? PesoEstimado { get; set; }
        public byte? V7Mon { get; set; }
        public bool V7Flete { get; set; }
        public decimal? V7Volumen { get; set; }
        public string VcExpediente { get; set; }
        public string VcPedidoCliente { get; set; }

        public virtual Ctf001 V7ClnroNavigation { get; set; }
        public virtual Transporte V7TransporteNavigation { get; set; }
        public virtual RemitosCabExtra RemitosCabExtra { get; set; }
        public virtual RemitosEntrega RemitosEntrega { get; set; }
        public virtual ICollection<RemitosDet> RemitosDets { get; set; }
    }
}
