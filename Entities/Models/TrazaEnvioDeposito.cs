﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaEnvioDeposito
    {
        public int Id { get; set; }
        public int? IdTraza { get; set; }
        public string NroLote { get; set; }
        public string NroSerie { get; set; }
        public bool? Procesado { get; set; }
    }
}
