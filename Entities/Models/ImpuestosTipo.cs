﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosTipo
    {
        public ImpuestosTipo()
        {
            ImpuestosCodigos = new HashSet<ImpuestosCodigo>();
            VentasDetImpuestos = new HashSet<VentasDetImpuesto>();
            VentasDetImpuestosProformas = new HashSet<VentasDetImpuestosProforma>();
            VentasImpuestos = new HashSet<VentasImpuesto>();
            VentasImpuestosProformas = new HashSet<VentasImpuestosProforma>();
        }

        public string IdImpuesto { get; set; }
        public string Descripcion { get; set; }
        public byte DetallaPorArticulo { get; set; }
        public int IdTipoImpuesto { get; set; }

        public virtual XTiposImpuesto IdTipoImpuestoNavigation { get; set; }
        public virtual ICollection<ImpuestosCodigo> ImpuestosCodigos { get; set; }
        public virtual ICollection<VentasDetImpuesto> VentasDetImpuestos { get; set; }
        public virtual ICollection<VentasDetImpuestosProforma> VentasDetImpuestosProformas { get; set; }
        public virtual ICollection<VentasImpuesto> VentasImpuestos { get; set; }
        public virtual ICollection<VentasImpuestosProforma> VentasImpuestosProformas { get; set; }
    }
}
