﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AnticiposPreciosCab
    {
        public AnticiposPreciosCab()
        {
            AnticiposPreciosDets = new HashSet<AnticiposPreciosDet>();
        }

        public int Id { get; set; }
        public short IdLista { get; set; }
        public string Descripcion { get; set; }
        public decimal Coef1 { get; set; }
        public decimal Coef2 { get; set; }
        public decimal Coef3 { get; set; }
        public decimal Coef4 { get; set; }
        public decimal Coef5 { get; set; }
        public decimal? IdListaorigen { get; set; }
        public DateTime? FechaVig { get; set; }
        public byte TipoLista { get; set; }
        public bool IvaIncluido { get; set; }
        public int NroPedido { get; set; }
        public decimal Cotiz { get; set; }
        public decimal? Dto1 { get; set; }
        public decimal? Dto2 { get; set; }
        public decimal? Dto3 { get; set; }
        public decimal? Dto4 { get; set; }
        public int? CondPago { get; set; }

        public virtual ICollection<AnticiposPreciosDet> AnticiposPreciosDets { get; set; }
    }
}
