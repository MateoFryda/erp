﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PuntosDeVentaRecibosUbicacion
    {
        public string UbicacionFisica { get; set; }
        public short PuntoDeVenta { get; set; }
    }
}
