﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzIngStkDet20201118
    {
        public int IdIngreso { get; set; }
        public string SaArt { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal CostoLocal { get; set; }
        public decimal CostoExtranjera { get; set; }
        public decimal CostoLocalLand { get; set; }
        public decimal CostoExtranjeraLand { get; set; }
        public decimal CantFact { get; set; }
        public int? IhOrdendeCompra { get; set; }
        public int Id { get; set; }
        public int? OcdetId { get; set; }
        public decimal? CantUmedStd { get; set; }
        public string RcTipo { get; set; }
        public int? RcNumero { get; set; }
        public DateTime? IhFecEntregaEst { get; set; }
        public DateTime? IhFecsolreq { get; set; }
        public string Umed { get; set; }
        public decimal? CostoUmlocal { get; set; }
        public decimal? CostoUmextranjera { get; set; }
        public decimal? CostoUmlocalLand { get; set; }
        public decimal? CostoUmextranjeraLand { get; set; }
        public string IhCcosto { get; set; }
        public string IhScosto { get; set; }
        public decimal? Cantidad2 { get; set; }
    }
}
