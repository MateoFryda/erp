﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zona
    {
        public Zona()
        {
            Ctf001s = new HashSet<Ctf001>();
        }

        public string IdZona { get; set; }
        public string ZoDescripcion { get; set; }
        public string Z0Letra { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
    }
}
