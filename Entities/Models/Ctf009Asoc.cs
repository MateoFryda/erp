﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf009Asoc
    {
        public string CiNro { get; set; }
        public string CiDesc { get; set; }
        public byte? CiNivel { get; set; }
        public string CiAcumul { get; set; }
        public string CiMonet { get; set; }
        public string CiClasif { get; set; }
        public string CiCcosto { get; set; }
        public string CiReic { get; set; }
        public double? CiTipo { get; set; }
        public string CiHab { get; set; }
        public string CiAgrup { get; set; }
        public byte? CiMon { get; set; }
        public DateTime? Finhab { get; set; }
    }
}
