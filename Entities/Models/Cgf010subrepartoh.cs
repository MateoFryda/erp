﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010subrepartoh
    {
        public string CcCodsubreparto { get; set; }
        public string CcDescsubreparto { get; set; }
        public string CcCod { get; set; }
        public string CiNro { get; set; }
    }
}
