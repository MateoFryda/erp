﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Provinciasbnl
    {
        public string Codigo { get; set; }
        public string CodigoBnl { get; set; }
        public string Descripcion { get; set; }
        public string CodBanco { get; set; }
    }
}
