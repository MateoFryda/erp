﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DtoVentasDet
    {
        public int IdDto { get; set; }
        public decimal Rango { get; set; }
        public decimal Coef1 { get; set; }
        public decimal Coef2 { get; set; }
        public decimal Coef3 { get; set; }
        public decimal Coef4 { get; set; }
        public decimal Coef5 { get; set; }

        public virtual DtoVentasCab IdDtoNavigation { get; set; }
    }
}
