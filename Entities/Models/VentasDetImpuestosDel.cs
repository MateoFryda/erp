﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasDetImpuestosDel
    {
        public string TUsuario { get; set; }
        public DateTime? TFechayhora { get; set; }
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public int VbId { get; set; }
        public string SaArt { get; set; }
        public string ImpuestoCodigo { get; set; }
        public decimal Alicuota { get; set; }
        public decimal VbGravadoLocal { get; set; }
        public decimal VbGravadoExtranjera { get; set; }
        public decimal VbExentoLocal { get; set; }
        public decimal VbExentoExtranjera { get; set; }
        public decimal VbImpuestoLocal { get; set; }
        public decimal VbImpuestoExtranjera { get; set; }
        public byte VbOrden { get; set; }
    }
}
