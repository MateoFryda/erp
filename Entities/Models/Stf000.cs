﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf000
    {
        public string S0Codi { get; set; }
        public string S0Item { get; set; }
        public string S0Desc { get; set; }
        public double? S0Nro1 { get; set; }
        public double? S0Nro2 { get; set; }
        public double? S0Nro3 { get; set; }
    }
}
