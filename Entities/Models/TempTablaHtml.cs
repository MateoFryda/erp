﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempTablaHtml
    {
        public decimal Id { get; set; }
        public string Textohtml { get; set; }
    }
}
