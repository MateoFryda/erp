﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class LandingCost
    {
        public int CodLandingCost { get; set; }
        public decimal CoefLanding { get; set; }
        public string DescLanding { get; set; }
    }
}
