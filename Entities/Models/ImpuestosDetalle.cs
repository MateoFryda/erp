﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosDetalle
    {
        public string IdImpuestosStf001 { get; set; }
        public string ImpuestosCodigo { get; set; }
        public string IdCalculo { get; set; }

        public virtual ImpuestosCalculo IdCalculoNavigation { get; set; }
        public virtual ImpuestosStf001 IdImpuestosStf001Navigation { get; set; }
        public virtual ImpuestosCodigo ImpuestosCodigoNavigation { get; set; }
    }
}
