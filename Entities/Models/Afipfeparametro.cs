﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Afipfeparametro
    {
        public string Item { get; set; }
        public string Valor { get; set; }
        public string Descripcion { get; set; }
    }
}
