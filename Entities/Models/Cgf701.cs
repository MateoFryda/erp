﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf701
    {
        public string CiCta { get; set; }
        public double? CiAnio { get; set; }
        public double? CiMes { get; set; }
        public decimal? CiSaldo { get; set; }
        public decimal? CiDebe { get; set; }
        public decimal? CiHaber { get; set; }
        public string CiAniomes { get; set; }
    }
}
