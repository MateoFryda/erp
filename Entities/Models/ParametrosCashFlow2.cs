﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParametrosCashFlow2
    {
        public string Tipo { get; set; }
        public string TipoDesc { get; set; }
    }
}
