﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class FormaPagoTipoCuentum
    {
        public string FormaPago { get; set; }
        public string CodTipoCuenta { get; set; }
        public string DescTipoCuenta { get; set; }
    }
}
