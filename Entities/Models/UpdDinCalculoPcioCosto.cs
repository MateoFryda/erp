﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class UpdDinCalculoPcioCosto
    {
        public DateTime FechaUpdate { get; set; }
        public string SaArt { get; set; }
        public string IdConcepto { get; set; }
        public string Codigo { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Resultado { get; set; }
        public string Orden { get; set; }
    }
}
