﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CondPagoVenc
    {
        public short CCod { get; set; }
        public int CDias { get; set; }
        public decimal CPorc { get; set; }

        public virtual Ctf001condPago CCodNavigation { get; set; }
    }
}
