﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ConfigurarParametro
    {
        public string Label { get; set; }
        public string Condicion1 { get; set; }
        public string Condicion2 { get; set; }
        public string Condicion3 { get; set; }
        public string Recuperar { get; set; }
        public string Grabar { get; set; }
        public string Tabla { get; set; }
        public string Nombrecampo { get; set; }
        public string Validar { get; set; }
        public string Descripcion { get; set; }
        public string Codigodeabm { get; set; }
        public int Orden { get; set; }
        public int Id { get; set; }
    }
}
