﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosDetHistorico
    {
        public string VcTipo { get; set; }
        public byte VcSuc { get; set; }
        public int VcNro { get; set; }
        public string SaArt { get; set; }
        public int VfLinea { get; set; }
        public decimal VfCant { get; set; }
        public decimal VfPcio { get; set; }
        public decimal? VfPcioConIva { get; set; }
        public string VfDeposito { get; set; }
        public string VfPedObs { get; set; }
        public string VfDetalle { get; set; }
        public decimal VfDto1 { get; set; }
        public decimal VfDto2 { get; set; }
        public decimal VfDto3 { get; set; }
        public decimal VdDto4 { get; set; }
        public decimal? VfPcioLista { get; set; }
        public string VfTxtDto1 { get; set; }
        public string VfTxtDto2 { get; set; }
        public string VfTxtDto3 { get; set; }
        public string VfTxtDto4 { get; set; }
        public decimal VfPcioNeto { get; set; }
        public decimal VfAlicuotaIva { get; set; }
        public decimal VfPcioNetoConIva { get; set; }
    }
}
