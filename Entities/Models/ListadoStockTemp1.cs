﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ListadoStockTemp1
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public DateTime? Fvto { get; set; }
        public string Comprobante { get; set; }
        public string TipoMov { get; set; }
        public DateTime Fecha { get; set; }
        public string Origen { get; set; }
        public string DescOri { get; set; }
        public string Destino { get; set; }
        public string DescDest { get; set; }
        public decimal? Cantidad { get; set; }
    }
}
