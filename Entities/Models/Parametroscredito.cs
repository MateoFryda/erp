﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Parametroscredito
    {
        public string Campo { get; set; }
        public string Tabla { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string Sqlbrowse { get; set; }
    }
}
