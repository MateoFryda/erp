﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposDeComprobanteIvaCompra
    {
        public byte CondicionImpositiva { get; set; }
        public string TipoComprobante { get; set; }
        public string TipoFactura { get; set; }
        public int Id { get; set; }
    }
}
