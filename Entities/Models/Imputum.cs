﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Imputum
    {
        public string IcCuenta { get; set; }
        public string IcNombre { get; set; }
        public int IcId { get; set; }
    }
}
