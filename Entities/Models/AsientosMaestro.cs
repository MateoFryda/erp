﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AsientosMaestro
    {
        public string CgClave { get; set; }
        public string CgIc { get; set; }
        public string CgDesc { get; set; }
        public double? CgImporte { get; set; }
        public string CgCc { get; set; }
        public string CgIt { get; set; }
    }
}
