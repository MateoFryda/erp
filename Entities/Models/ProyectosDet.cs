﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProyectosDet
    {
        public int PrNro { get; set; }
        public int PrLinea { get; set; }
        public int? PrNroot { get; set; }
        public string SaArt { get; set; }
        public string PrDetalle { get; set; }
        public decimal PrCant { get; set; }
        public decimal PrPcio { get; set; }
        public DateTime? PrFechEst { get; set; }
        public string PrPedidoCliente { get; set; }
        public bool PrLineaAnul { get; set; }
        public bool? PrLlevalote { get; set; }

        public virtual ProyectosCab PrNroNavigation { get; set; }
    }
}
