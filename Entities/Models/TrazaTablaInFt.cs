﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaTablaInFt
    {
        public int Id { get; set; }
        public DateTime? Fecha { get; set; }
        public string Articulo { get; set; }
        public string NroSerie { get; set; }
        public string NroLote { get; set; }
        public bool? Procesado { get; set; }
    }
}
