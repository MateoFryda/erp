﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001Cobradore
    {
        public string IdCobrador { get; set; }
        public string Descripcion { get; set; }
    }
}
