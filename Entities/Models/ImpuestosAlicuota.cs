﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ImpuestosAlicuota
    {
        public DateTime FechaVdesde { get; set; }
        public DateTime FechaVhasta { get; set; }
        public string Cuit { get; set; }
        public decimal AlicuotaReten { get; set; }
        public decimal AlicuotaPercep { get; set; }
        public string IdCalculo { get; set; }

        public virtual ImpuestosCalculo IdCalculoNavigation { get; set; }
    }
}
