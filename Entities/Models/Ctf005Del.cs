﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf005Del
    {
        public string TUsuario { get; set; }
        public DateTime? TFechayhora { get; set; }
        public string CeFcomp { get; set; }
        public int CeSuc { get; set; }
        public decimal CeFnro { get; set; }
        public string CeCnro { get; set; }
        public DateTime? CeFfech { get; set; }
        public decimal CeTotal { get; set; }
        public DateTime? CeFvto { get; set; }
        public DateTime? CeFcanc { get; set; }
        public string CeCcanc { get; set; }
        public int? CeCsuc { get; set; }
        public int? CeNcanc { get; set; }
        public decimal CeCotiz { get; set; }
        public double? CePordesc { get; set; }
        public double? CeComi { get; set; }
        public string CeComp { get; set; }
        public int? CeZona { get; set; }
        public decimal? CeIva { get; set; }
        public string CeCodigo { get; set; }
        public byte CeMon { get; set; }
        public decimal CeCotizmon { get; set; }
        public bool CeDifcam { get; set; }
    }
}
