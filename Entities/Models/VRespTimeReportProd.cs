﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VRespTimeReportProd
    {
        public string Código { get; set; }
        public string Descripción { get; set; }
    }
}
