﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AutorizacionesEstados2H
    {
        public int CodAutorizacion { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
        public byte Moneda { get; set; }
    }
}
