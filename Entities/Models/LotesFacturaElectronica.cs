﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class LotesFacturaElectronica
    {
        public string FileName { get; set; }
        public string Tipo { get; set; }
        public short Suc { get; set; }
        public int Nro { get; set; }
        public int NroH { get; set; }
        public string NroCae { get; set; }
        public string Estado { get; set; }
        public string Motivo { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime? FvtoCae { get; set; }
    }
}
