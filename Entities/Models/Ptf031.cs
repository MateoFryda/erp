﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf031
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public string PcImputa { get; set; }
        public decimal PcImporte { get; set; }
        public string PcCcosto { get; set; }
        public string PcIt { get; set; }
        public int PcIdent { get; set; }
    }
}
