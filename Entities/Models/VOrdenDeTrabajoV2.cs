﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VOrdenDeTrabajoV2
    {
        public int NroOt { get; set; }
        public string Artículo { get; set; }
        public string Descripción { get; set; }
        public decimal? Cantidad { get; set; }
        public string Estado { get; set; }
        public short EstadoCodigo { get; set; }
    }
}
