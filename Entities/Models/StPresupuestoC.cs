﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StPresupuestoC
    {
        public StPresupuestoC()
        {
            StPresupuestoConsumos = new HashSet<StPresupuestoConsumo>();
        }

        public string SClavePresupuesto { get; set; }
        public string SNumeroPresupuesto { get; set; }
        public DateTime? DFecha { get; set; }
        public string SHa { get; set; }
        public string SNumeroRecepcion { get; set; }
        public string SClave { get; set; }
        public string SFalla { get; set; }
        public string SObservaciones { get; set; }
        public decimal? NTotalPresupuesto { get; set; }
        public string SCodEstado { get; set; }
        public string SFallaTecnico { get; set; }
        public decimal? NPorDtoTareas { get; set; }
        public decimal? NPorDtoRepuestos { get; set; }
        public string SDtoObservaciones { get; set; }
        public decimal? NDtoTareas { get; set; }
        public decimal? NDtoRepuestos { get; set; }
        public decimal? NTotalFinal { get; set; }
        public decimal? NTotalTareas { get; set; }
        public decimal? NTotalRepuestos { get; set; }
        public decimal? NTotalFinalP { get; set; }
        public decimal? NTotalTareasFinal { get; set; }
        public decimal? NTotalRepuestosFinal { get; set; }
        public string SCodMotivo { get; set; }
        public DateTime? DFechaEntrega { get; set; }
        public string SHoraEntrega { get; set; }
        public string VaFcomp { get; set; }
        public int? VaSuc { get; set; }
        public int? VaFnro { get; set; }
        public string V7Tipo { get; set; }
        public int? V7Suc { get; set; }
        public int? V7Nro { get; set; }
        public decimal? NCotiz { get; set; }
        public decimal? CoefTareas { get; set; }
        public decimal? CoefRepuestos { get; set; }

        public virtual StRecepcion S { get; set; }
        public virtual ICollection<StPresupuestoConsumo> StPresupuestoConsumos { get; set; }
    }
}
