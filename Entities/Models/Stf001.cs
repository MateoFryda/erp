﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001
    {
        public Stf001()
        {
            AnticiposPreciosDets = new HashSet<AnticiposPreciosDet>();
            CalculoPcioCostos = new HashSet<CalculoPcioCosto>();
            CalculoPpps = new HashSet<CalculoPpp>();
            DespieceSaItemNavigations = new HashSet<Despiece>();
            DespieceSaProductoNavigations = new HashSet<Despiece>();
            IngStkDets = new HashSet<IngStkDet>();
            NovedadesConceptos = new HashSet<NovedadesConcepto>();
            OrdenesTrabajoDetTemps = new HashSet<OrdenesTrabajoDetTemp>();
            OrdenesTrabajoDets = new HashSet<OrdenesTrabajoDet>();
            Otf002s = new HashSet<Otf002>();
            PedidosDets = new HashSet<PedidosDet>();
            PreciosDets = new HashSet<PreciosDet>();
            PreciosProvDets = new HashSet<PreciosProvDet>();
            PresupuestosComprasDets = new HashSet<PresupuestosComprasDet>();
            PresupuestosDets = new HashSet<PresupuestosDet>();
            ProformasDets = new HashSet<ProformasDet>();
            RemitosDets = new HashSet<RemitosDet>();
            RemitosSeries = new HashSet<RemitosSeries>();
            Stf001Bs = new HashSet<Stf001B>();
            Stf001Cbventa = new HashSet<Stf001Cbventa>();
            Stf001Clientes = new HashSet<Stf001Cliente>();
            Stf001Cs = new HashSet<Stf001C>();
            Stf001Lotes = new HashSet<Stf001Lote>();
            Stf004SaItemNavigations = new HashSet<Stf004>();
            Stf004SaProductoNavigations = new HashSet<Stf004>();
            VentasDetProformas = new HashSet<VentasDetProforma>();
            VentasDets = new HashSet<VentasDet>();
        }

        public string SaArt { get; set; }
        public string SaModelo { get; set; }
        public string SaDesc { get; set; }
        public string SaDescmed { get; set; }
        public string SaCodFabricante { get; set; }
        public string SaUmed { get; set; }
        public byte SaIva { get; set; }
        public bool? SaLlevaStock { get; set; }
        public int SaStkMin { get; set; }
        public int SaStkMax { get; set; }
        public int SaPuntoPed { get; set; }
        public decimal? SaComision { get; set; }
        public bool SaSerie { get; set; }
        public byte SaPartida { get; set; }
        public decimal? SaCostoStd { get; set; }
        public decimal? SaPuc { get; set; }
        public DateTime? SaFuc { get; set; }
        public decimal? SaPpp { get; set; }
        public string SaRubro { get; set; }
        public string SaN0 { get; set; }
        public string SaN1 { get; set; }
        public string SaN2 { get; set; }
        public string SaN3 { get; set; }
        public string SaN4 { get; set; }
        public short SaAgrupacion { get; set; }
        public bool SaLlevaLote { get; set; }
        public short SaCodDescOrigen { get; set; }
        public short SaCodTipoCambio { get; set; }
        public short SaCodCostoExt { get; set; }
        public short SaCodGastoInt { get; set; }
        public short SaCodGastoFinanc { get; set; }
        public decimal SaCostoOrigen { get; set; }
        public short SaCodUtilidad { get; set; }
        public decimal SaPrecioLista { get; set; }
        public decimal SaCostofob { get; set; }
        public string SaDocumento { get; set; }
        public string SaFamilia { get; set; }
        public string SaProvVig { get; set; }
        public string SaListaPcioProvVig { get; set; }
        public DateTime? Finhabilitacion { get; set; }
        public byte SaMon { get; set; }
        public decimal SaCoeflanding { get; set; }
        public byte SaMonOrigen { get; set; }
        public string SaImpuesto { get; set; }
        public bool SaPermitedecimales { get; set; }
        public bool? SaAdmitedevolucion { get; set; }
        public short SaClasif { get; set; }
        public bool SaSinDescuento { get; set; }
        public bool SaNoAptoVenta { get; set; }
        public byte[] SaFoto { get; set; }
        public decimal? SaPeso { get; set; }
        public decimal? SaVolumen { get; set; }
        public byte? SaNivel { get; set; }
        public bool? SaNoDisponibleWeb { get; set; }
        public int? SaDiasVtoLote { get; set; }
        public bool? SaNoRemitirFact { get; set; }
        public bool? SaEsTrazable { get; set; }
        public byte? SaTipoArticuloFe { get; set; }
        public bool? SaSerieAuto { get; set; }
        public int? SaPresentacion { get; set; }
        public int? SaCantxenv { get; set; }
        public string SaUmed2 { get; set; }
        public decimal? SaEquivalenciaUm { get; set; }
        public bool? SaLlevaDespacho { get; set; }
        public string SaNomencladorCot { get; set; }
        public string SaUsuarioUltModif { get; set; }
        public string SaUmedAlfatebta { get; set; }
        public string SaUmedAlfabeta { get; set; }

        public virtual AgrupContabCab SaAgrupacionNavigation { get; set; }
        public virtual Stf001clasif SaClasifNavigation { get; set; }
        public virtual Fabricante SaCodFabricanteNavigation { get; set; }
        public virtual Stf003 SaFamiliaNavigation { get; set; }
        public virtual ImpuestosStf001 SaImpuestoNavigation { get; set; }
        public virtual Stf001Presentacion SaPresentacionNavigation { get; set; }
        public virtual RubroArt SaRubroNavigation { get; set; }
        public virtual UnidadMedidum SaUmedNavigation { get; set; }
        public virtual Stf001Extra Stf001Extra { get; set; }
        public virtual ICollection<AnticiposPreciosDet> AnticiposPreciosDets { get; set; }
        public virtual ICollection<CalculoPcioCosto> CalculoPcioCostos { get; set; }
        public virtual ICollection<CalculoPpp> CalculoPpps { get; set; }
        public virtual ICollection<Despiece> DespieceSaItemNavigations { get; set; }
        public virtual ICollection<Despiece> DespieceSaProductoNavigations { get; set; }
        public virtual ICollection<IngStkDet> IngStkDets { get; set; }
        public virtual ICollection<NovedadesConcepto> NovedadesConceptos { get; set; }
        public virtual ICollection<OrdenesTrabajoDetTemp> OrdenesTrabajoDetTemps { get; set; }
        public virtual ICollection<OrdenesTrabajoDet> OrdenesTrabajoDets { get; set; }
        public virtual ICollection<Otf002> Otf002s { get; set; }
        public virtual ICollection<PedidosDet> PedidosDets { get; set; }
        public virtual ICollection<PreciosDet> PreciosDets { get; set; }
        public virtual ICollection<PreciosProvDet> PreciosProvDets { get; set; }
        public virtual ICollection<PresupuestosComprasDet> PresupuestosComprasDets { get; set; }
        public virtual ICollection<PresupuestosDet> PresupuestosDets { get; set; }
        public virtual ICollection<ProformasDet> ProformasDets { get; set; }
        public virtual ICollection<RemitosDet> RemitosDets { get; set; }
        public virtual ICollection<RemitosSeries> RemitosSeries { get; set; }
        public virtual ICollection<Stf001B> Stf001Bs { get; set; }
        public virtual ICollection<Stf001Cbventa> Stf001Cbventa { get; set; }
        public virtual ICollection<Stf001Cliente> Stf001Clientes { get; set; }
        public virtual ICollection<Stf001C> Stf001Cs { get; set; }
        public virtual ICollection<Stf001Lote> Stf001Lotes { get; set; }
        public virtual ICollection<Stf004> Stf004SaItemNavigations { get; set; }
        public virtual ICollection<Stf004> Stf004SaProductoNavigations { get; set; }
        public virtual ICollection<VentasDetProforma> VentasDetProformas { get; set; }
        public virtual ICollection<VentasDet> VentasDets { get; set; }
    }
}
