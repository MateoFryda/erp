﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsFpago
    {
        public int IdOperacion { get; set; }
        public string CodFp { get; set; }
        public string DescFp { get; set; }
        public string DetalleFp { get; set; }
        public decimal ImporteFp { get; set; }
        public string SpGrabacion { get; set; }
        public string TipoFp { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
    }
}
