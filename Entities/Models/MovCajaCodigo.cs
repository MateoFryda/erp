﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class MovCajaCodigo
    {
        public MovCajaCodigo()
        {
            MovCajaMovimientos = new HashSet<MovCajaMovimiento>();
        }

        public int CodMov { get; set; }
        public string Descripción { get; set; }
        public byte Tipo { get; set; }

        public virtual MovCajaTipo TipoNavigation { get; set; }
        public virtual ICollection<MovCajaMovimiento> MovCajaMovimientos { get; set; }
    }
}
