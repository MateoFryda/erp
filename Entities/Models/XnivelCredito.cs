﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XnivelCredito
    {
        public XnivelCredito()
        {
            Ctf001CaNc1Navigations = new HashSet<Ctf001>();
            Ctf001CaNc2Navigations = new HashSet<Ctf001>();
        }

        public byte IdNivelCredito { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
        public byte Moneda { get; set; }

        public virtual ICollection<Ctf001> Ctf001CaNc1Navigations { get; set; }
        public virtual ICollection<Ctf001> Ctf001CaNc2Navigations { get; set; }
    }
}
