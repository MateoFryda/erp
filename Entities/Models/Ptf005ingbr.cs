﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf005ingbr
    {
        public string PeCop { get; set; }
        public int PeNro { get; set; }
        public byte PeCcpto { get; set; }
        public decimal PeImpsujret { get; set; }
        public decimal PeReten { get; set; }
        public int PeRnro { get; set; }
    }
}
