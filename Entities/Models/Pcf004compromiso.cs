﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Pcf004compromiso
    {
        public string CdFcomp { get; set; }
        public byte? CdSuc { get; set; }
        public int? CdFnro { get; set; }
        public string CdCnro { get; set; }
        public DateTime? CdFfech { get; set; }
        public decimal? CdTotal { get; set; }
        public DateTime? CdFvto { get; set; }
        public DateTime? CdFcanc { get; set; }
        public decimal? CdSaldo { get; set; }
        public string CdCcanc { get; set; }
        public byte? CdCsuc { get; set; }
        public int? CdNcanc { get; set; }
        public decimal CdCotiz { get; set; }
        public double? CdPordesc { get; set; }
        public double? CdIva { get; set; }
        public double? CdComi { get; set; }
        public string CdComp { get; set; }
        public string CdFcprov { get; set; }
        public int? CdClasificacion { get; set; }
        public bool CdSeleccion { get; set; }
        public DateTime? CdFpago { get; set; }
        public byte CdMon { get; set; }
        public decimal CdCotizmon { get; set; }
    }
}
