﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class DespieceMo
    {
        public string SaProducto { get; set; }
        public short Secuencia { get; set; }
        public string Proveedor { get; set; }
        public string Sector { get; set; }
        public string Tarea { get; set; }
        public decimal? Horas { get; set; }
        public decimal? Costo { get; set; }
        public bool UsaHojaRuta { get; set; }
        public string SaArt { get; set; }
    }
}
