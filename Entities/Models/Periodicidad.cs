﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Periodicidad
    {
        public Periodicidad()
        {
            NovedadesConceptos = new HashSet<NovedadesConcepto>();
        }

        public byte IdPeriodicidad { get; set; }
        public string Periodicidad1 { get; set; }
        public short Cantdias { get; set; }

        public virtual ICollection<NovedadesConcepto> NovedadesConceptos { get; set; }
    }
}
