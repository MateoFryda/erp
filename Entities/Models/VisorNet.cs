﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VisorNet
    {
        public int Id { get; set; }
        public string Reporte { get; set; }
        public bool? Impresora { get; set; }
        public string Parametros { get; set; }
        public string Formulas { get; set; }
    }
}
