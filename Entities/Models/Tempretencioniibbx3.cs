﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Tempretencioniibbx3
    {
        public string TipoOperacion { get; set; }
        public string CodigoNorma { get; set; }
        public string FechaRet { get; set; }
        public string TipoComprobante { get; set; }
        public string LetraComp { get; set; }
        public string NroComprobante { get; set; }
        public string Fechacomprobante { get; set; }
        public string Monto { get; set; }
        public string Numerocertificadopropio { get; set; }
        public string Tipodocumento { get; set; }
        public string Cuit { get; set; }
        public string SitIb { get; set; }
        public string NroIb { get; set; }
        public string SituacionIva { get; set; }
        public string Razonsocial { get; set; }
        public string Otrosconceptos { get; set; }
        public string Iva { get; set; }
        public string MontoBaseRet { get; set; }
        public string Alicuota { get; set; }
        public string RetPracticadas { get; set; }
        public string TotalRetenido { get; set; }
    }
}
