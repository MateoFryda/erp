﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StPresupuestoConsumo
    {
        public string SClavePresupuesto { get; set; }
        public string SNumeroPresupuesto { get; set; }
        public string SgComp { get; set; }
        public decimal SgSuc { get; set; }
        public decimal SgNro { get; set; }

        public virtual StPresupuestoC S { get; set; }
    }
}
