﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010
    {
        public Cgf010()
        {
            Cgf010distribcs = new HashSet<Cgf010distribc>();
            DePositos = new HashSet<DePosito>();
            NovedadesMaestros = new HashSet<NovedadesMaestro>();
            PuntosDeVenta = new HashSet<PuntosDeVentum>();
        }

        public string CcCod { get; set; }
        public string CcDesc { get; set; }
        public string CcResponsable { get; set; }
        public string CcEmail { get; set; }
        public DateTime? Finhab { get; set; }
        public int? CcSector { get; set; }
        public int? CcTipo { get; set; }

        public virtual ICollection<Cgf010distribc> Cgf010distribcs { get; set; }
        public virtual ICollection<DePosito> DePositos { get; set; }
        public virtual ICollection<NovedadesMaestro> NovedadesMaestros { get; set; }
        public virtual ICollection<PuntosDeVentum> PuntosDeVenta { get; set; }
    }
}
