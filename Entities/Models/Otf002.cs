﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Otf002
    {
        public int ObNro { get; set; }
        public string ObArt { get; set; }
        public string ObDesc { get; set; }
        public DateTime ObFrec { get; set; }
        public decimal ObCant { get; set; }
        public decimal? ObCantIng { get; set; }
        public decimal? ObPcio { get; set; }
        public int ObId { get; set; }
        public decimal? ObPeso { get; set; }
        public decimal? ObVolumen { get; set; }
        public string ObLote { get; set; }
        public string ObUmed { get; set; }
        public DateTime? ObFecsolreq { get; set; }
        public bool? ObCerrada { get; set; }
        public string ObCcosto { get; set; }
        public string ObScosto { get; set; }
        public string ObEspTec { get; set; }
        public string ObTiporeq { get; set; }
        public int? ObNroReq { get; set; }
        public string ObTipolote { get; set; }
        public string ObImputacion { get; set; }
        public decimal? ObPciobruto { get; set; }
        public decimal? ObPorcdto { get; set; }

        public virtual Stf001 ObArtNavigation { get; set; }
        public virtual Otf001 ObNroNavigation { get; set; }
    }
}
