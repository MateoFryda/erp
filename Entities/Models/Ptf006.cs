﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf006
    {
        public int PfNro { get; set; }
        public string PfCcomp { get; set; }
        public int? PfCnro { get; set; }
        public decimal PfImporte { get; set; }
        public decimal? PfTotuss { get; set; }
        public short? PfBco { get; set; }
        public string PfImputa { get; set; }
        public string PfCcosto { get; set; }
        public string PfItemc { get; set; }
        public string PfCop { get; set; }
        public int PfIdent { get; set; }
        public DateTime? PfFvto { get; set; }
        public int PfId { get; set; }
        public int? PfNrolote { get; set; }
        public string PfCta { get; set; }
        public int? PfCnroelectronico { get; set; }
        public byte[] Pago { get; set; }
    }
}
