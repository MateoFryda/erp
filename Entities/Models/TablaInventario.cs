﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaInventario
    {
        public string Deposito { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        public string Ubicacion1 { get; set; }
        public string Ubicacion2 { get; set; }
        public string Ubicacion3 { get; set; }
        public string Ubicacion4 { get; set; }
        public DateTime Fecha { get; set; }
        public bool Consolidado { get; set; }
    }
}
