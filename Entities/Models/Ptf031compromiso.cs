﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf031compromiso
    {
        public string PcComp { get; set; }
        public int PcNro { get; set; }
        public string PcImputa { get; set; }
        public decimal PcImporte { get; set; }
        public string PcCcosto { get; set; }
        public string PcIt { get; set; }
    }
}
