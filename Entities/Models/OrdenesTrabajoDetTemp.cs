﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class OrdenesTrabajoDetTemp
    {
        public int OtNroOt { get; set; }
        public short OtLinea { get; set; }
        public string OtArt { get; set; }
        public string OtDetalle { get; set; }
        public decimal OtCant { get; set; }
        public string OtUmed { get; set; }
        public string OtEspTec { get; set; }

        public virtual Stf001 OtArtNavigation { get; set; }
        public virtual OrdenesTrabajoCabTemp OtNroOtNavigation { get; set; }
        public virtual UnidadMedidum OtUmedNavigation { get; set; }
    }
}
