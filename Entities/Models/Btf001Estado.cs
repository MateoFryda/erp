﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Btf001Estado
    {
        public int BaNro { get; set; }
        public int BaId { get; set; }
        public string BaEstadoactual { get; set; }
        public string BaEstadoanterior { get; set; }
        public string BaCcan { get; set; }
        public int? BaNrocan { get; set; }
        public int BaBanco { get; set; }
    }
}
