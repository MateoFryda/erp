﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ProformasError
    {
        public int IdTransaccion { get; set; }
        public int? Proforma { get; set; }
        public string DescripciónError { get; set; }
    }
}
