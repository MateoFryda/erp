﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vdgrpadron
    {
        public string FechaPublicacion { get; set; }
        public string FechaVdesde { get; set; }
        public string FechaVhasta { get; set; }
        public string Cuit { get; set; }
        public string TipoContrInsc { get; set; }
        public string MarcaAltaBaja { get; set; }
        public string MarcaCbioAlicuota { get; set; }
        public string AlicuotaPercep { get; set; }
        public string AlicuotaReten { get; set; }
        public string GrupoPercep { get; set; }
        public string GrupoReten { get; set; }
        public string Tipopadron { get; set; }
    }
}
