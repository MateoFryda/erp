﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010subrepartod
    {
        public string CcCodsubreparto { get; set; }
        public string CiNro { get; set; }
    }
}
