﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CgflistaReportesContable
    {
        public string CiNro { get; set; }
        public string CiDescripcioN { get; set; }
        public short? CiTiporeporte { get; set; }
        public short? CiIdlista { get; set; }
        public DateTime? CiFechaAlta { get; set; }
        public string CiUsrAlta { get; set; }
    }
}
