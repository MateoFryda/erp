﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StocksxdepLotesSeriesCliente
    {
        public string SaArt { get; set; }
        public string NroLote { get; set; }
        public string NroSerie { get; set; }
        public string Cliente { get; set; }
        public decimal? SaStkmin { get; set; }
        public decimal? SaStkmax { get; set; }
        public decimal? SaPuntoped { get; set; }
        public decimal Stock { get; set; }
        public decimal Stockumed { get; set; }
        public string Dep { get; set; }
    }
}
