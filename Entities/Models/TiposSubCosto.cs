﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposSubCosto
    {
        public byte IdTipo { get; set; }
        public string Descripcion { get; set; }
    }
}
