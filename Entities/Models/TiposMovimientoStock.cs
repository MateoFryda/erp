﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposMovimientoStock
    {
        public TiposMovimientoStock()
        {
            DepositosTiposMovimientoStocks = new HashSet<DepositosTiposMovimientoStock>();
            Stf007hs = new HashSet<Stf007h>();
            TiposMovimientoStockUsuarios = new HashSet<TiposMovimientoStockUsuario>();
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool MValidaStock { get; set; }
        public string Cuenta { get; set; }
        public byte? TipoImputacionArt { get; set; }
        public string TipoCuenta { get; set; }
        public bool? Manual { get; set; }
        public byte TipoMov { get; set; }
        public string TipoComprobanteAsoc { get; set; }
        public bool Despacha { get; set; }
        public string CuentaStock { get; set; }
        public bool? PasajeVencidos { get; set; }
        public bool? RequiereAprobacion { get; set; }
        public decimal? FactorCosto { get; set; }
        public string TipoCosto { get; set; }
        public bool? Cirugia { get; set; }
        public bool? EsNacionalizacion { get; set; }
        public bool? PasajeVtoProximo { get; set; }
        public bool? Trazabilidad { get; set; }
        public bool? PideImputacion { get; set; }

        public virtual ICollection<DepositosTiposMovimientoStock> DepositosTiposMovimientoStocks { get; set; }
        public virtual ICollection<Stf007h> Stf007hs { get; set; }
        public virtual ICollection<TiposMovimientoStockUsuario> TiposMovimientoStockUsuarios { get; set; }
    }
}
