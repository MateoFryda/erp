﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgfcompensc
    {
        public int GpNro { get; set; }
        public int CgNro { get; set; }
        public int? CgLinea { get; set; }
        public DateTime CgFecha { get; set; }
        public decimal CgImporte { get; set; }
        public string CgCc { get; set; }
        public string CgIt { get; set; }
        public string CgTipo { get; set; }
        public string CgTcomp { get; set; }
        public int? CgNcomp { get; set; }
        public short? CgNsuc { get; set; }
        public string CgDesc { get; set; }
    }
}
