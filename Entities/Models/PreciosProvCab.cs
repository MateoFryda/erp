﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PreciosProvCab
    {
        public PreciosProvCab()
        {
            PreciosProvDets = new HashSet<PreciosProvDet>();
        }

        public int Idlista { get; set; }
        public string PaCnro { get; set; }
        public string ListaProv { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fechavig { get; set; }
        public byte Moneda { get; set; }

        public virtual Ptf001 PaCnroNavigation { get; set; }
        public virtual ICollection<PreciosProvDet> PreciosProvDets { get; set; }
    }
}
