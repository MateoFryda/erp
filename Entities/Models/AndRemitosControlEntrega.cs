﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AndRemitosControlEntrega
    {
        public string V7Tipo { get; set; }
        public short V7Suc { get; set; }
        public int V7Nro { get; set; }
        public DateTime? Fecha { get; set; }
        public string Foto { get; set; }
        public string Usuario { get; set; }
        public string Ubicacion { get; set; }
        public string Observacion { get; set; }
        public string Qr { get; set; }

        public virtual RemitosCab V7 { get; set; }
    }
}
