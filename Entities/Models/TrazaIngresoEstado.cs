﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TrazaIngresoEstado
    {
        public TrazaIngresoEstado()
        {
            TrazaIngStkCabs = new HashSet<TrazaIngStkCab>();
        }

        public int Idestado { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<TrazaIngStkCab> TrazaIngStkCabs { get; set; }
    }
}
