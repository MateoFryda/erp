﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf001Linea
    {
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public decimal? VaLineas { get; set; }
    }
}
