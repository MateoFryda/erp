﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001ExtrasParametro
    {
        public int Tipo { get; set; }
        public byte Orden { get; set; }
        public string Label { get; set; }
        public string TipoControl { get; set; }
        public int? Tamanio { get; set; }
        public short? LongitudMax { get; set; }
        public bool? Obligatorio { get; set; }
        public string SpBrowse { get; set; }
        public string TitBrowse { get; set; }
        public string SpValidacion { get; set; }
    }
}
