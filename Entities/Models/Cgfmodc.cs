﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgfmodc
    {
        public string ScodMod { get; set; }
        public string SCodNivel { get; set; }
        public string SdescNivel { get; set; }
    }
}
