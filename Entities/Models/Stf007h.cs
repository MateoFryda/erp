﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf007h
    {
        public Stf007h()
        {
            CalculoPpps = new HashSet<CalculoPpp>();
            IngStkCabs = new HashSet<IngStkCab>();
            Stf007cs = new HashSet<Stf007c>();
        }

        public int SgId { get; set; }
        public DateTime SgFecha { get; set; }
        public string SgComp { get; set; }
        public int SgSuc { get; set; }
        public int SgNro { get; set; }
        public string SgOri { get; set; }
        public string SgDest { get; set; }
        public string SgCta { get; set; }
        public string SgObs { get; set; }
        public string TipoMovimientoStock { get; set; }
        public int? NroMovStock { get; set; }
        public string SgUser { get; set; }
        public string SgDeporiginal { get; set; }
        public byte? SgIdMovitoDevol { get; set; }
        public byte? SgIdMotivoDevol { get; set; }

        public virtual DePosito SgDestNavigation { get; set; }
        public virtual DePosito SgOriNavigation { get; set; }
        public virtual TiposMovimientoStock TipoMovimientoStockNavigation { get; set; }
        public virtual ICollection<CalculoPpp> CalculoPpps { get; set; }
        public virtual ICollection<IngStkCab> IngStkCabs { get; set; }
        public virtual ICollection<Stf007c> Stf007cs { get; set; }
    }
}
