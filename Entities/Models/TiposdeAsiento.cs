﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposdeAsiento
    {
        public string Tipo { get; set; }
        public string Comprobante { get; set; }
        public string Descripcion { get; set; }
        public string Descripcion2 { get; set; }
        public string Modificable { get; set; }
        public byte Contab { get; set; }
        public string Desistema { get; set; }
        public string Concomprobante { get; set; }
    }
}
