﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CuentasConMovimientoSinccAsoc
    {
        public string CgIc { get; set; }
        public double? CiTipo { get; set; }
        public DateTime? CgFecha { get; set; }
        public decimal? Importe { get; set; }
        public string CiDesc { get; set; }
        public string CgTipo { get; set; }
        public string CgTcomp { get; set; }
        public string CuentaOrigen { get; set; }
        public string DescOrigen { get; set; }
    }
}
