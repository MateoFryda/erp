﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AfipfeWsfex
    {
        public string TipoComprobante { get; set; }
        public int SucursalComprobante { get; set; }
        public int NumeroComprobante { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Cae { get; set; }
        public string ObservacionCae { get; set; }
        public string FechaVencimientoCae { get; set; }
        public int? TransaccionNumero { get; set; }
        public int? Estado { get; set; }
        public int? ErrorCodigo { get; set; }
        public string ErrorDescripcion { get; set; }
        public int Orden { get; set; }
        public int IdOp { get; set; }
        public int NumeroComprobanteD { get; set; }
        public int NumeroComprobanteH { get; set; }
    }
}
