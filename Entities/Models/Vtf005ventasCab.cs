﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Vtf005ventasCab
    {
        public Vtf005ventasCab()
        {
            Vtf005ventasDets = new HashSet<Vtf005ventasDet>();
        }

        public string VeComp { get; set; }
        public byte VeSuc { get; set; }
        public int VeNro { get; set; }
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public int VaFnro { get; set; }
        public DateTime? VeFechaPondPago { get; set; }

        public virtual ICollection<Vtf005ventasDet> Vtf005ventasDets { get; set; }
    }
}
