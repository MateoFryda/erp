﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalCompRecibo
    {
        public int? Codtransac { get; set; }
        public string Comp { get; set; }
        public int? Suc { get; set; }
        public int? Nro { get; set; }
        public string Imputacion { get; set; }
        public decimal? Importe { get; set; }
        public int? Moneda { get; set; }
        public decimal? Cotizmon { get; set; }
        public DateTime? Fvto { get; set; }
        public string Cc { get; set; }
        public string Subcc { get; set; }
        public byte? Difcam { get; set; }
    }
}
