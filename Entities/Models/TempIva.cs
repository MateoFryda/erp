﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempIva
    {
        public string PcFcomp { get; set; }
        public int PcFnro { get; set; }
        public decimal PcIva { get; set; }
        public decimal PcIva27 { get; set; }
        public decimal PcIvaad { get; set; }
    }
}
