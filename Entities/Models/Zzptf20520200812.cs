﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Zzptf20520200812
    {
        public int PeNro { get; set; }
        public string PePnro { get; set; }
        public string PeCop { get; set; }
        public DateTime? PeFech { get; set; }
        public decimal? PeTotal { get; set; }
        public decimal? PePordesc { get; set; }
        public decimal? PeDto { get; set; }
        public string PeObs { get; set; }
        public bool PeAnul { get; set; }
        public decimal? PeReten { get; set; }
        public decimal? PeInterno { get; set; }
        public decimal? PeComis { get; set; }
        public decimal? PeIvacomi { get; set; }
        public decimal? PeTotgrav { get; set; }
        public decimal? PeTotnogr { get; set; }
        public string PeUser { get; set; }
        public byte PeMon { get; set; }
        public decimal PeCotiz { get; set; }
        public decimal PeCotizmon { get; set; }
        public int? RcNumero { get; set; }
        public string CodProvedor { get; set; }
        public string ApellidoYNombre { get; set; }
        public string Mail { get; set; }
        public string TipoDoc { get; set; }
        public double? NroDoc { get; set; }
        public string SocTrabaja { get; set; }
        public double? Legajo { get; set; }
        public string Sa { get; set; }
        public string Sh { get; set; }
        public string Srl { get; set; }
    }
}
