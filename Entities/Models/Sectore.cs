﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Sectore
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string PaCnro { get; set; }
        public decimal? Costo { get; set; }
    }
}
