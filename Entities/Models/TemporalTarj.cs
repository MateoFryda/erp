﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalTarj
    {
        public int? Codtransac { get; set; }
        public string Resultado { get; set; }
    }
}
