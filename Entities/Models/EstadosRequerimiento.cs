﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class EstadosRequerimiento
    {
        public short Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool PermitecompraSector { get; set; }
        public bool? PermiteModificarReq { get; set; }
        public bool? NoPermiteTotal0 { get; set; }
    }
}
