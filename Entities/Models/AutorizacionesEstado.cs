﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class AutorizacionesEstado
    {
        public int CodAutorizacion { get; set; }
        public int EstadoAct { get; set; }
        public int EstadoFin { get; set; }
        public decimal MontoDesde { get; set; }
        public decimal? MontoHasta { get; set; }
        public int? Sector { get; set; }
        public string TipoOperacion { get; set; }
    }
}
