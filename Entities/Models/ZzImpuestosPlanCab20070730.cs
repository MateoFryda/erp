﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzImpuestosPlanCab20070730
    {
        public string IdPlanImp { get; set; }
        public byte CondicionImpositiva { get; set; }
        public string Descripcion { get; set; }
        public byte Clientes { get; set; }
        public bool GeneraCompDc { get; set; }
    }
}
