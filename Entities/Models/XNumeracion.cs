﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class XNumeracion
    {
        public string Codigo { get; set; }
        public int Numero { get; set; }
        public string Descripcion { get; set; }
    }
}
