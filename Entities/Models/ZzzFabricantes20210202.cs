﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzzFabricantes20210202
    {
        public string CodFabricante { get; set; }
        public string DescFabricante { get; set; }
        public string F3 { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public double? Versión { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
