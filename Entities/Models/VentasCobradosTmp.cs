﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class VentasCobradosTmp
    {
        public string VaFcomp { get; set; }
        public int VaSuc { get; set; }
        public decimal VaFnro { get; set; }
        public DateTime? FechaFactura { get; set; }
        public decimal? ImporteCobrado { get; set; }
        public decimal Cotizdolarx { get; set; }
    }
}
