﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TiposDocumento
    {
        public TiposDocumento()
        {
            ImpuestosCondicionDocumentos = new HashSet<ImpuestosCondicionDocumento>();
        }

        public short IdTipoDoc { get; set; }
        public string Descripcion { get; set; }
        public string SpValidacion { get; set; }
        public short? TipoDocAfip { get; set; }

        public virtual ICollection<ImpuestosCondicionDocumento> ImpuestosCondicionDocumentos { get; set; }
    }
}
