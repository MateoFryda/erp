﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CuentasConMovDolaresAsoc
    {
        public string CgIc { get; set; }
        public string CgCc { get; set; }
        public string CgIt { get; set; }
        public byte? CiTipo { get; set; }
        public DateTime? CgFecha { get; set; }
        public decimal? Importe { get; set; }
        public string CiDesc { get; set; }
        public string CgTipo { get; set; }
        public string CgTcomp { get; set; }
    }
}
