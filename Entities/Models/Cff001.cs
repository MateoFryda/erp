﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cff001
    {
        public string CfCod { get; set; }
        public string CfDesc { get; set; }
        public double? CfIva1 { get; set; }
        public double? CfPcio { get; set; }
        public string CfImputa { get; set; }
        public double? CfTip { get; set; }
    }
}
