﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ViewVentascabListadoCliente
    {
        public string VaClnro { get; set; }
        public DateTime VaFfech { get; set; }
    }
}
