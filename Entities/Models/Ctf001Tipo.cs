﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ctf001Tipo
    {
        public Ctf001Tipo()
        {
            Ctf001s = new HashSet<Ctf001>();
        }

        public int Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Ctf001> Ctf001s { get; set; }
    }
}
