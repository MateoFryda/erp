﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Ptf005transferencia
    {
        public string PeCop { get; set; }
        public int? PeNro { get; set; }
        public DateTime? Fecha { get; set; }
        public string Regh { get; set; }
        public string Regc { get; set; }
        public string Data { get; set; }
    }
}
