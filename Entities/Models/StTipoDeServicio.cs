﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class StTipoDeServicio
    {
        public StTipoDeServicio()
        {
            StRecepcions = new HashSet<StRecepcion>();
        }

        public string SCodTipoDeServicio { get; set; }
        public string SDescTipoDeServicio { get; set; }
        public int? NHab { get; set; }
        public int? NVirtual { get; set; }

        public virtual ICollection<StRecepcion> StRecepcions { get; set; }
    }
}
