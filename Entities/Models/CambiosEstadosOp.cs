﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class CambiosEstadosOp
    {
        public string Tipo { get; set; }
        public int Nro { get; set; }
        public string Usuario { get; set; }
        public short EstadoNuevo { get; set; }
        public short EstadoAnterior { get; set; }
        public DateTime Fecha { get; set; }
    }
}
