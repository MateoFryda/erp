﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TempRetencionX
    {
        public string Regimen { get; set; }
        public string Cuit { get; set; }
        public DateTime? Fecha { get; set; }
        public string Espacio { get; set; }
        public string Comprob { get; set; }
        public string MontoRet { get; set; }
    }
}
