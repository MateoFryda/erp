﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TemporalFpRecibo
    {
        public int? Codtransac { get; set; }
        public string Codfp { get; set; }
        public decimal? Importefp { get; set; }
        public string Spgrabacion { get; set; }
        public string Tipofp { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
    }
}
