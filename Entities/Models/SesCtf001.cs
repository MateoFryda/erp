﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class SesCtf001
    {
        public string CaCnro { get; set; }
        public string CaCnom { get; set; }
        public string CaDir { get; set; }
        public int? CaCp { get; set; }
        public string CaResp { get; set; }
        public string CaZona { get; set; }
        public string CaVen { get; set; }
        public byte CaIva { get; set; }
        public decimal CaPorex { get; set; }
        public string CaTel { get; set; }
        public string CaFax { get; set; }
        public string CaCuit { get; set; }
        public string CaIngb { get; set; }
        public short? CaCnc { get; set; }
        public short? CaCact { get; set; }
        public short? CaCpag { get; set; }
        public DateTime? CaFalta { get; set; }
        public string CaPcia { get; set; }
        public short CaTransporte { get; set; }
        public short CaLista { get; set; }
        public string CaLent { get; set; }
        public bool CaExterio { get; set; }
        public byte? CaIbcf { get; set; }
        public byte? CaTasaingb { get; set; }
        public string CaEntrega { get; set; }
        public byte? CaTipdoc { get; set; }
        public string CaLocalidad { get; set; }
        public string CaDeudor { get; set; }
        public string CaDeudordolar { get; set; }
        public string CaIcvtas { get; set; }
        public string CaProvincia { get; set; }
        public string CaContacto { get; set; }
        public string CaCodigoDeFacturacion { get; set; }
        public string CaEmail { get; set; }
        public byte? CaNc1 { get; set; }
        public byte? CaNc2 { get; set; }
        public byte? CaNc3 { get; set; }
        public string CaCtacte { get; set; }
        public string CaFacturarA { get; set; }
        public byte? CaCodDto { get; set; }
        public string CaObservaciones { get; set; }
        public int? CaAgrup { get; set; }
        public string CaCodigo { get; set; }
        public DateTime? CaInhabilitacion { get; set; }
        public byte CaClasif { get; set; }
        public string CaIdAgrupImp { get; set; }
        public short? CaPais { get; set; }
        public string CaSubZona { get; set; }
        public string CaNombreFantasia { get; set; }
        public int CaTipocliente { get; set; }
        public byte CaHorario { get; set; }
        public short CaCiclo { get; set; }
        public string CaDealer { get; set; }
        public bool CaEventualCtaCte { get; set; }
        public string CaEmailCobranza { get; set; }
        public string CaCodproveedor { get; set; }
        public byte? CaCobranzas { get; set; }
    }
}
