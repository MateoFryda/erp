﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Compradore
    {
        public string CodComprador { get; set; }
        public string Descripcion { get; set; }
    }
}
