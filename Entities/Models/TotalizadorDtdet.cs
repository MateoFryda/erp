﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TotalizadorDtdet
    {
        public int IdTotalizadorDt { get; set; }
        public int IdTotalizador { get; set; }

        public virtual TotalizadorDtcab IdTotalizadorDtNavigation { get; set; }
        public virtual TotalizadorCab IdTotalizadorNavigation { get; set; }
    }
}
