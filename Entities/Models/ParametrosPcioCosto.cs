﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ParametrosPcioCosto
    {
        public ParametrosPcioCosto()
        {
            CalculoPcioCostos = new HashSet<CalculoPcioCosto>();
        }

        public string IdConcepto { get; set; }
        public string Nombre { get; set; }
        public string LabelResultado { get; set; }
        public string SpBrowse { get; set; }
        public string SpValidacion { get; set; }
        public string Calculo1 { get; set; }
        public string Calculo2 { get; set; }
        public string Dtonropcioxprov { get; set; }

        public virtual ParametrosArtPcioCosto ParametrosArtPcioCosto { get; set; }
        public virtual ICollection<CalculoPcioCosto> CalculoPcioCostos { get; set; }
    }
}
