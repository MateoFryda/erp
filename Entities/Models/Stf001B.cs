﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Stf001B
    {
        public string SaArt { get; set; }
        public string SbDeposito { get; set; }
        public decimal SbStkMin { get; set; }
        public decimal SbStkmax { get; set; }
        public decimal SbPuntoPed { get; set; }
        public string SbUbi1 { get; set; }
        public string SbUbi2 { get; set; }
        public string SbUbi3 { get; set; }
        public string SbUbi4 { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
    }
}
