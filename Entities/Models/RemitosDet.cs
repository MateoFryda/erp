﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class RemitosDet
    {
        public string V7Tipo { get; set; }
        public short V7Suc { get; set; }
        public int V7Nro { get; set; }
        public int V8Linea { get; set; }
        public string SaArt { get; set; }
        public int SgId { get; set; }
        public decimal V8Cant { get; set; }
        public decimal V8Pcio { get; set; }
        public string V8Detalle { get; set; }
        public decimal? V8PcioLista { get; set; }
        public decimal V8Dto1 { get; set; }
        public decimal V8Dto2 { get; set; }
        public decimal V8Dto3 { get; set; }
        public decimal V8Dto4 { get; set; }
        public int? NroPedido { get; set; }
        public decimal? V8PcioNeto { get; set; }
        public string V8TxtDto1 { get; set; }
        public string V8TxtDto2 { get; set; }
        public string V8TxtDto3 { get; set; }
        public string V8TxtDto4 { get; set; }
        public decimal? V8CostoStdlocal { get; set; }
        public decimal? V8CostoStdext { get; set; }
        public decimal? V8CostoUclocal { get; set; }
        public decimal? V8CostoUcext { get; set; }
        public decimal? V8CostoPpplocal { get; set; }
        public decimal? V8CostoPppext { get; set; }
        public decimal? V8CostoFifolocal { get; set; }
        public decimal? V8CostoFifoext { get; set; }
        public string V8Fcomp { get; set; }
        public int? V8Fnro { get; set; }
        public byte V8Orden { get; set; }
        public string NroPresupSt { get; set; }
        public decimal? V8PcioConiva { get; set; }
        public decimal? V8PcioNetoConiva { get; set; }
        public int? NroOt { get; set; }
        public decimal? V8Cant2 { get; set; }

        public virtual Stf001 SaArtNavigation { get; set; }
        public virtual RemitosCab V7 { get; set; }
    }
}
