﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Otf001Entrega
    {
        public int OaNro { get; set; }
        public string DepCodigo { get; set; }
        public string DepDescripcion { get; set; }
        public string Observacion { get; set; }
        public string Direccion { get; set; }
        public short? PaisCodigo { get; set; }
        public string PaisDescripcion { get; set; }
        public string Codpostal { get; set; }
        public string ProvCodigo { get; set; }
        public string ProvDescripcion { get; set; }
        public string Localidad { get; set; }

        public virtual Otf001 OaNroNavigation { get; set; }
    }
}
