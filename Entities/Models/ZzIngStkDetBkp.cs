﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class ZzIngStkDetBkp
    {
        public int IdIngreso { get; set; }
        public string SaArt { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal CostoLocal { get; set; }
        public decimal CostoExtranjera { get; set; }
        public decimal CostoLocalLand { get; set; }
        public decimal CostoExtranjeraLand { get; set; }
        public decimal CantFact { get; set; }
        public int? IhOrdendeCompra { get; set; }
        public int Id { get; set; }
        public int? OcdetId { get; set; }
    }
}
