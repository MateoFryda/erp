﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Btf003
    {
        public Btf003()
        {
            FpagoUbicacions = new HashSet<FpagoUbicacion>();
            TarjetasMovimientos = new HashSet<TarjetasMovimiento>();
        }

        public string BcCod { get; set; }
        public int BcTipo { get; set; }
        public string BcIc { get; set; }
        public string BcIcchdiferido { get; set; }
        public byte BcMon { get; set; }
        public string BcDescripcion { get; set; }
        public DateTime? BcFinhab { get; set; }
        public string BcSpvalidacion { get; set; }
        public string BcTipocuenta { get; set; }
        public string BcNumerocuenta { get; set; }
        public bool? BcValidanivelcred { get; set; }
        public string BcGrupofp { get; set; }
        public int BcTiporetencion { get; set; }
        public decimal BcAlicuota { get; set; }
        public bool BcValidasaldo { get; set; }
        public string BcJurisdiccion { get; set; }
        public bool? BcPondFechaMenor { get; set; }
        public bool? BcPondFechaMayor { get; set; }
        public string BcTipofp { get; set; }
        public string BcCcosto { get; set; }

        public virtual ICollection<FpagoUbicacion> FpagoUbicacions { get; set; }
        public virtual ICollection<TarjetasMovimiento> TarjetasMovimientos { get; set; }
    }
}
