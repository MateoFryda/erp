﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class Cgf010distribh
    {
        public Cgf010distribh()
        {
            Cgf010distribcs = new HashSet<Cgf010distribc>();
        }

        public string CcCoddist { get; set; }
        public string CcDescdist { get; set; }
        public string CcCod { get; set; }

        public virtual ICollection<Cgf010distribc> Cgf010distribcs { get; set; }
    }
}
