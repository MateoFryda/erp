﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class PedidosCab
    {
        public PedidosCab()
        {
            PedidosDets = new HashSet<PedidosDet>();
        }

        public string VcTipo { get; set; }
        public byte VcSuc { get; set; }
        public int VcNro { get; set; }
        public string VcClnro { get; set; }
        public DateTime VcFfech { get; set; }
        public string VcVen { get; set; }
        public byte? VcCpag { get; set; }
        public short? VcTransporte { get; set; }
        public int VcLista { get; set; }
        public string VcObs { get; set; }
        public string VcUsuario { get; set; }
        public string VcLentrega { get; set; }
        public bool VcAnul { get; set; }
        public DateTime VcFcarga { get; set; }
        public int? VcPedObservado { get; set; }
        public string VcPedidoCliente { get; set; }
        public decimal VcCotiz { get; set; }
        public decimal VcCotizmon { get; set; }
        public byte VcMon { get; set; }
        public string VcHcarga { get; set; }
        public DateTime? VcFultModif { get; set; }
        public string VcHultModif { get; set; }
        public bool VcDtosmanuales { get; set; }
        public DateTime? VcFVencimiento { get; set; }
        public DateTime? Fregistro { get; set; }
        public string VcCc { get; set; }
        public string VcIc { get; set; }
        public string VcTipoComp { get; set; }
        public string VcPentrega { get; set; }
        public short VcEstado { get; set; }
        public byte VcCfact { get; set; }
        public bool VcFlete { get; set; }
        public string VcPedidopor { get; set; }
        public string VcExpediente { get; set; }
        public int? VcNroPlanOs { get; set; }
        public short? VcPtoVta { get; set; }
        public bool? VcEsProduccion { get; set; }
        public string VcAgrupImpuesto { get; set; }
        public bool? EsAnticipo { get; set; }
        public bool? EsDeAnticipo { get; set; }

        public virtual Ctf001 VcClnroNavigation { get; set; }
        public virtual PedidosEstado VcEstadoNavigation { get; set; }
        public virtual PedidosCabExtra PedidosCabExtra { get; set; }
        public virtual ICollection<PedidosDet> PedidosDets { get; set; }
    }
}
