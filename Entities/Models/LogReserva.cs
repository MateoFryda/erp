﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class LogReserva
    {
        public int Id { get; set; }
        public int? NroPedido { get; set; }
        public string Articulo { get; set; }
        public string NroLote { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
