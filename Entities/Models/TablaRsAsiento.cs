﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.ERP
{
    public partial class TablaRsAsiento
    {
        public int IdOperacion { get; set; }
        public int CodTransac { get; set; }
        public string CiNro { get; set; }
        public string CiDesc { get; set; }
        public string Ccosto { get; set; }
        public string Subcosto { get; set; }
        public decimal Importe { get; set; }
        public string CuentaTipo { get; set; }
        public decimal? ImporteExt { get; set; }
    }
}
